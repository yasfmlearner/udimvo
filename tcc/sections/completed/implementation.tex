\section{Implementation}\label{sec:implementation}

For the initial months of research, we stuck with the plan outlined in the
proposal in which we set out to implement an unsupervised learning framework
for visual odometry based on mutual information. One of the challenges with
this framework that, to the best of our knowledge, hasn't been explored yet, is
the generation of positive and negative training samples. Since we wanted to
combine the mutual information framework with the geometric consistency
self-supervisory signal proposed by Iyer
\etal~\cite{Iyer2018GeometricConsistency}, we needed a strategy that would use
sequences of consecutive frames to leverage the forward pass computations
needed to compute the composite transformation constraints (CTC) loss.

Let $\{I_i\;|\;1 \le i \le N\}$ be a sequence of $N$ consecutive frames and
$\{T_{j \rightarrow j+1}\;|\;1 \le j \le N - 1\}$ a sequence of consecutive
transformations where $T_{j \rightarrow j+1}$ is the relative transformation
between frames $I_{j}$ and $I_{j+1}$. For a minimum temporal distance $2 \le
t_{\min} \le N-1$ between frames, we can formulate a total of $n_{\mathrm{CTC}}
= \frac{(N - t_{\min})(N - t_{\min} + 1)}{2}$ composite transformation
contraints. Given a sequence of $N$ frames and a minimum temporal distance
$t_{\min}$, we have $n_\mathrm{CTC}$ non-consecutive transformations $\{T_{j
\rightarrow j + t}\;|\; 1 \le j \le N-t;\; t_{\min} \le t \le N-1\}$, with
which we can define a constraint between the directly predicted pose $T_{j
\rightarrow j + t}$ and the composite transformation $\widehat{T}_{j
\rightarrow j + t}$ obtained from composing the consecutive transformations
$T_{j + t - 1 \rightarrow j + t}\cdots T_{j \rightarrow j+1}$.  Let $p_{j
\rightarrow j+1}$ denote the 6-DoF pose obtained from the log map of $T_{j
\rightarrow j+1}$, we can define the CTC loss of the model as

\begin{equation}
  \mathcal{L}_\mathrm{CTC} = \sum_{t=t_{\min}}^{N-1} \sum_{j=1}^{N-t} \|p_{j
  \rightarrow j + t} - \hat{p}_{j \rightarrow j + t} \|_2.
\end{equation}

To formulate the mutual information objective, we need to define a way to
generate positive ($x$) and negative ($\hat{x}$) samples drawn from the input
space $\mathcal{X}$. A sample can be defined as a pair of frames from a
trajectory with enough overlap to estimate the relative pose. We
wish to learn an encoder network $E_\psi : \mathcal{X} \rightarrow \mathcal{Y}$
that maps input samples to a pose $y$ in the feature space $\mathcal{Y}$. Using
the notation from the CTC objective, we have that $x_{ij} = \{I_i, I_j\}$ and
$y_{ij} = p_{i \rightarrow j}$. First, we sample $L+1$ augmentations $a_l \sim
\mathcal{A}$ and apply them to both images in the pair to obtain new positive
samples $x_{ij}^{l} = \{a_l(I_i), a_l(I_j)\}$, where $a_0$ is the identity
function.  Then, the negative samples are created by applying $K$ random
augmentations $b_k \sim \mathcal{A}$ to one of the images in the pair
$x_{ij}^l$, such that $\hat{x}_{ij}^{lk} = \{a_l(I_i), b_k(a_l(I_j))\}$.  We use
$E_\psi$ to encode all positive samples and pair the result with the respective
negative samples to form positive $(x_{ij}^l, y_{ij})$ and negative
$(\hat{x}_{ij}^{lk}, y_{ij})$ pairs that are fed to a discriminator $T_\omega :
\mathcal{X} \times \mathcal{Y} \rightarrow \mathbb{R}$.  The real value output
by the discriminator is then used to compute our estimate of the mutual
information between each pair using the
Donsker-Varadhan~\cite{Donsker1976KLDual} estimator, which we wish to maximize.
The loss term we wish to minimize can be formulated as

\begin{equation}
  \mathcal{L}_{\mathrm{MI}} = -\sum_{l=1}^{L+1} \sum_{t=t_{\min}}^{N-1}
  \sum_{i=1}^{N-t}  T_\omega (x_{ii+t}^l, y_{ii+t}) + \log \sum_{k=1}^K
  \sum_{l=1}^{L+1} \sum_{t=t_{\min}}^{N-1} \sum_{i=1}^{N-t} e^{T_\omega
  (\hat{x}_{ii+t}^{lk}, y_{ii+t})}.
\end{equation}

Intuitively, we want the discriminator to output high activations for the
positive samples and low activations to the negative samples, and by doing so we
expect the discriminator to learn how to maximize the mutual information between
$x$ and $y$.

Even though we want to maximize the mutual information between the inputs $x$
and their respective pose $y$, in practice, the dimensionality of the inputs is
too large, and we use a convolutional encoder $C_\psi$ to extract meaningful
features from the inputs with which we can pair the pose with before feeding
the discriminator. Figure~\ref{fig:sample-framework} shows an outline of the
mutual information framework that was used.

\input{sections/completed/sample-fwk}

The augmentation function space $\mathcal{A}$ is defined by the set of all
possible affine transformations created from the composition of a rotation
$R_\theta$, translation $T_{xy}$, and a scale transformation $S_{xy}$, in this
order. The transformations are given by Equation~\ref{eq:affine}, where each
each parameter was sampled uniformly from the intervals $\theta \in [-\pi/8,
\pi/8]$, $t_x \in [-0.05, 0.05]W$, $t_y \in [-0.05, 0.05]H$, and $s \in [0.9,
1.1]$.

\begin{equation}\label{eq:affine}
  R_\theta =
\begin{pmatrix}
  \cos(\theta) & -\sin(\theta) & 0 \\
  \sin(\theta) & \cos(\theta) & 0 \\
  0 & 0 & 1
\end{pmatrix},
  T_{xy} =
  \begin{pmatrix}
    1 & 0 & t_x \\
    0 & 1 & t_y \\
    0 & 0 & 1
  \end{pmatrix},
  S_{xy} =
  \begin{pmatrix}
    s_x & 0 & 0 \\
    0 & s_y & 0 \\
    0 & 0 & 1
  \end{pmatrix}
\end{equation}

The final transformation function used to augment the images is given by the
composition of the three transformations as $M = S_{xy} T_{xy} R_\theta$.
Figure~\ref{fig:aug-batch} shows a positve and negative input with and without
augmentation. Note how for Figure~\ref{fig:neg-img-aug} we apply two
augmentation functions sampled from $\mathcal{A}$ to the second image in the
pair. This means we have to be somewhat conservative with the abruptness of the
transformations, since a lot of information is lost through them.

\begin{figure}
  \centering
  \begin{subfigure}{0.8\textwidth}
    \includegraphics[width=\textwidth]{./figs/batch/pos-img-seq.png}
    \caption{$x_{ij}^0$}
    \label{fig:pos-img-seq}
  \end{subfigure} \\
  \begin{subfigure}{0.8\textwidth}
    \includegraphics[width=\textwidth]{./figs/batch/pos-img-aug.png}
    \caption{$x_{ij}^1$}
    \label{fig:pos-img-aug}
  \end{subfigure} \\
  \begin{subfigure}{0.8\textwidth}
    \includegraphics[width=\textwidth]{./figs/batch/neg-img-seq.png}
    \caption{$\hat{x}_{ij}^{00}$}
    \label{fig:neg-img-seq}
  \end{subfigure} \\
  \begin{subfigure}{0.8\textwidth}
    \includegraphics[width=\textwidth]{./figs/batch/neg-img-aug.png}
    \caption{$\hat{x}_{ij}^{10}$}
    \label{fig:neg-img-aug}
  \end{subfigure} \\
  \caption{Positive (\subref{fig:pos-img-seq}-\subref{fig:pos-img-aug}) and
  negative (\subref{fig:neg-img-seq}-\subref{fig:neg-img-aug}) input samples
  after applying the augmentation functions sampled from $\mathcal{A}$.}
  \label{fig:aug-batch}
\end{figure}

"""Define the enumeration types used in the configs."""
import enum


class Rotation(str, enum.Enum):
  """Define the rotation types used to represent a pose."""

  quat = "quat"
  axis = "axis"

"""Defines the configuration parameters and methods for data manipulation."""
import os

from sacred import Ingredient

data_ingredient = Ingredient("dataset_config")


@data_ingredient.config
def data_config():
  """Data retrieval configuration."""
  # The input image shape for the network. The original image is cropped and
  # resized to match this shape.
  image_shape = (120, 120)
  # The name of the dataset to train on. One of {'kitti', 'euroc', 'msft7'}.
  dataset_name = "kitti"
  # The base datasets directory where all datasets are stored.
  datasets_dir = os.path.join("data", "raw")
  # The number of steps to compute gradients for before applying them.
  steps_per_update = 1
  # The number of sequences of length max_seq_length to use when generating
  # a single batch of training data.
  train_nr_seqs = 3
  # The number of sequences of length max_seq_length to use when generating
  # data for evaluation.
  eval_nr_seqs = 10
  # The number of negative samples to generate for every positive sample in
  # a training batch.
  neg_samples = 1
  # The maximum distance between a pair of frames that can be used as a
  # sample.
  max_seq_length = 5
  # The minimum distance between frames for a CTC constraint. All of the
  # constraints with length between min_ctc_length and max_seq_length are
  # created in order to train the network. If -1, this parameter is ignored.
  min_ctc_length = 4
  # The number of postive samples that are generated for every frame pair
  # by applying a random warp transformation.
  pos_warp_samples = 0
  # If true, random patches of the training images are cropped. If false,
  # a center crop is taken.
  random_crop = False
  # A float in the range [0.0, 1.0] with the ratio of each trajectory in the
  # training data that should be reserved for validation. The validation
  # frames are always taken from the end of the trajectory.
  val_split = 0.05

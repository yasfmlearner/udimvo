"""Experiments module.

Defines the base abstract class used to define an experiment, which
consists of a set of configurations that should be run.
"""
import abc
import enum
import fcntl
import itertools
import json
import os
import traceback

import src.config.train as train_config
import src.scripts.train
from src.config.types import Rotation
from src.functions.utils import create_dir


class Status(str, enum.Enum):
  """Experiment status enumeration."""

  RUNNING = "RUNNING"
  PENDING = "PENDING"
  COMPLETED = "COMPLETED"
  INTERRUPTED = "INTERRUPTED"
  FAILED = "FAILED"


def value_to_string(value):
  """Cast value to a string."""
  if isinstance(value, enum.Enum):
    return str(value.name)
  if isinstance(value, bool):
    return str(value).lower()
  if isinstance(value, tuple):
    return "x".join(map(str, value))
  return str(value)


class Experiment(abc.ABC):
  """Run an experiment with multiple configurations."""

  def __init__(self, exp_dir):
    """Initialize experiment with base directory."""
    self.exp_dir = exp_dir
    create_dir(self.exp_dir)
    self.config_fname = os.path.join(self.exp_dir, "config.json")
    self.base_config = {}
    self._hyperparams = None
    self._config_updates = None
    self._config_fd = None

  def get_config_updates(self):
    """Return a dict that maps keys to run infos with status and config."""
    if self._config_updates is None:
      hyperparam_lists = []
      for hp_name, hp_vals in self.hyperparams.items():
        hyperparam_lists.append([(hp_name, hp_val) for hp_val in hp_vals])
      hyperparam_combs = itertools.product(*hyperparam_lists)

      self._config_updates = {}
      for hpcomb in hyperparam_combs:
        run_key = []
        run_config_dict = {}
        for hp_name, hp_value in hpcomb:
          run_key.append(value_to_string(hp_value))
          run_config_dict[hp_name] = hp_value
        run_info = {
            "config": run_config_dict,
            "status": Status.PENDING,
        }
        run_key = "_".join(run_key)
        self._config_updates[run_key] = run_info
    return self._config_updates

  @property
  @abc.abstractmethod
  def hyperparams(self):
    """Return a dict of hyperparams configs mapping name to a list of values."""

  def update_status(self, run_key, status):
    """Update the status of the run with the given key.

    Args:
      run_key: A string with a valid key in the config_updates dictionary.
      status: The status we wish to change the run with run_key to.

    Returns:
      True if the update was successful and false otherwise.
    """
    # TODO: Return a boolean if no exception is thrown.
    with open(self.config_fname, "r+") as config_fd:
      fcntl.flock(config_fd, fcntl.LOCK_EX)
      config_updates = json.load(config_fd)
      config_updates[run_key]["status"] = status
      config_fd.seek(0)
      config_fd.truncate(0)
      json.dump(config_updates, config_fd, indent=2)

  def pending_runs(self):
    """Retrieve pending run tuples with (key, config) if any exist.

    If the config file doesn't exist, we first write the config_updates dict
    to it with all runs' status as PENDING. Once we have a config file, we
    retrieve a lock to access it and read from it again, looking for a run with
    status PENDING. If found, we return the run key. Otherwise, we return None
    to signal the caller that the experiment is over.
    """
    if not os.path.isfile(self.config_fname):
      config_updates = self.get_config_updates()
      with open(self.config_fname, "w") as config_fd:
        json.dump(config_updates, config_fd, indent=2)

    while True:
      pending_run = None
      with open(self.config_fname, "r+") as config_fd:
        fcntl.flock(config_fd, fcntl.LOCK_EX)
        config_updates = json.load(config_fd)
        for run_key, run_info in config_updates.items():
          if run_info["status"] == Status.PENDING:
            run_config_dict = config_updates[run_key]["config"]
            config_updates[run_key]["status"] = Status.RUNNING
            config_fd.seek(0)
            config_fd.truncate(0)
            json.dump(config_updates, config_fd, indent=2)
            pending_run = (run_key, run_config_dict)
            break
      if pending_run is None:
        print("No pending experiments...")
        return
      yield pending_run

  def run(self):
    """Run all of the configs of the experiment."""
    for run_key, run_config in self.pending_runs():
      cur_run_config = {**self.base_config, **run_config}
      try:
        cur_run_config["cur_run_dir"] = os.path.join(self.exp_dir, run_key)
        create_dir(cur_run_config["cur_run_dir"])
        train_config.ex.run(
            command_name="train",
            config_updates=cur_run_config,
            options={"--name": run_key},
        )
        run_status = Status.COMPLETED
      except Exception as e:
        traceback.print_tb(e.__traceback__)
        run_status = Status.FAILED
      except KeyboardInterrupt:
        run_status = Status.INTERRUPTED
        break
      finally:
        # TODO: Handle possible errors and use a timeout for this update.
        self.update_status(run_key, run_status)


class ConvergeExperiment(Experiment):
  """Experiment to test which parameter was necessary for convergence."""

  def __init__(self, exp_dir):  # noqa
    super().__init__(exp_dir)
    self.base_config["epochs"] = 10
    self.base_config["disc_logits_activation"] = None

  @property
  def hyperparams(self):  # noqa
    if self._hyperparams is None:
      self._hyperparams = {
          "disc_logits_activation": [None, "tanh"],
          "weight_decay_scale": [0.0, 3e-4],
          "grad_reg_scale": [0.0, 1e4]
      }
    return self._hyperparams


class DataExperiment(Experiment):
  """Experiment to test the performance of different data parameters."""

  def __init__(self, exp_dir):  # noqa
    super().__init__(exp_dir)
    self.base_config["ctc_loss_scale"] = 0
    self.base_config["epochs"] = 10
    self.base_config["disc_logits_activation"] = "tanh"
    self.base_config["dataset_config.nr_samples"] = 10

  @property
  def hyperparams(self):  # noqa
    if self._hyperparams is None:
      self._hyperparams = {
          "image_shape": [(140, 140), (120, 120)],
          "steps_per_update": [5, 1],
          "neg_samples": [2, 1],
          "pos_warp_samples": [1, 0],
          "max_seq_length": [5],
          "random_crop": [True, False],
      }
    return self._hyperparams


class BaselineCTCExperiment(Experiment):
  """Experiment to come up with a baseline result for the CTC net."""

  def __init__(self, exp_dir):  # noqa
    super().__init__(exp_dir)
    self.base_config["epochs"] = 30
    self.base_config["debug"] = True
    self.base_config["histogram_freq"] = 500
    self.base_config["infomax_loss_scale"] = 0
    self.base_config["dataset_config.neg_samples"] = 0
    self.base_config["dataset_config.steps_per_update"] = 5

  @property
  def hyperparams(self):  # noqa
    if self._hyperparams is None:
      self._hyperparams = {
          "optimizer_name": ["sgd", "adam"],
          "batch_norm": [True, False],
          "rot_type": [Rotation.quat, Rotation.axis]
      }
    return self._hyperparams

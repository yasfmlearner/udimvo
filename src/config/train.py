# pylint: disable=unused-argument,unused-variable
"""Defines the training configuration hyperparameters."""
import sacred
from docopt import docopt
from sacred.arg_parser import get_config_updates

from src.config.data import data_ingredient
from src.config.types import Rotation

ex = sacred.Experiment("udimvo", ingredients=[data_ingredient])


def parse_sacred_command_line(argv):
  """Parse the command line arguments in sacred format and return a config dict.

  Args:
    argv: Command-line as string or list of strings. The list of arguments
      should only the part starting from "with arg=value".

  Returns:
    A dictionary with the config options selected from the command line.
  """
  internal_usage = ex.get_usage()[-1]
  args = docopt(internal_usage, argv, help=False)
  config_updates, _ = get_config_updates(args["UPDATE"])
  return config_updates


@ex.config
def train_config():
  """Training configuration."""
  # Activation function for discriminator output
  disc_logits_activation = "tanh"
  # The number of epochs to train for.
  epochs = 30
  # The name of the optimizer we wish to use. Must match one of the keys in
  # the optimize_config dictionary.
  optimizer_name = "sgd"
  # Optimizer configuration parameters
  optimizer_configs = {
      "sgd": {
          # The learning rate used with the SGD optimizer.
          "learning_rate": 1e-4,
      },
      "adam": {
          # The learning rate used with the Adam optimizer.
          "learning_rate": 1e-4,
          # The exponential decay rate for 1st moment estimates.
          "beta_1": 0.9,
          # The exponential decay rate for 2nd moment estimates.
          "beta_2": 0.99,
      },
  }
  # Whether to apply batch normalization after every convolutional layer.
  batch_norm = False
  # The rotation representation type to we wish to learn. One of {"quaternion",
  # "axis"}
  rot_type = Rotation.quat
  # The scale factor of the infomax loss.
  infomax_loss_scale = 1.0
  # The scale factor of the CTC loss.
  ctc_loss_scale = 1.0
  # The scale factor of the gradient regularization term.
  grad_reg_scale = 0.0
  # The scale factor of the weight decay regularization term.
  weight_decay_scale = 0.0
  # The scale factor of the ground truth regularization term.
  gt_reg_scale = 0.0
  # The name of the infomax loss to use.
  infomax_loss_name = "dv"
  # GPU device id used for multi-gpu environments.
  gpu_device_id = 0
  # Whether to log debugging info.
  debug = False
  # Number of steps after which we should save weight histograms in debug mode.
  histogram_freq = 1000
  # Current run directory.
  cur_run_dir = None
  # Experiment runs directory.
  runs_dir = "runs"
  # Model checkpoints directory.
  ckpt_dir = "ckpts"
  # Errors directory
  errors_dir = "errors"
  # Artifacts directory.
  artifacts_dir = "artifacts"
  # Logging directory
  log_dir = "logs"
  # Whether to load the error for the given run
  resume_error = False


@ex.pre_run_hook()
def validate_config(_config, _log):
  """Validate hyperparameter configuration."""
  conf, dconf = _config, _config["dataset_config"]
  assert 0.0 <= dconf["val_split"] <= 1.0, (
      "Validation split ratio should be in the range [0, 1].")
  assert (conf["infomax_loss_scale"] > 0 or
          conf["ctc_loss_scale"] > 0), ("All loss scales can't be negative.")
  assert (conf["infomax_loss_scale"] == 0 or dconf["neg_samples"] >= 1), (
      "At least one negative sample is needed for infomax loss.")
  assert (conf["infomax_loss_scale"] > 0 or dconf["neg_samples"] == 0), (
      "If infomax loss scale is 0, the number of negative samples generated "
      "should also be 0")
  assert (conf["ctc_loss_scale"] == 0 or
          2 < dconf["min_ctc_length"] <= dconf["max_seq_length"]), (
              "Minimum CTC length should be greater than two, in order to "
              "avoid trivial constraints (i.e. between consecutive frames) "
              "and should be at most equal to the max sequence length.")
  assert (conf["ctc_loss_scale"] > 0 or dconf["min_ctc_length"] == -1), (
      "If CTC loss scale is zero, we always want to sample all possible "
      "non-consecutive frames, so min_ctc_length has to be -1.")

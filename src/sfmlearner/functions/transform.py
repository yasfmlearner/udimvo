import enum

import numpy as np
import torch


class RotationMode(str, enum.Enum):
  """Supported rotation modes."""

  EULER = "euler"
  QUAT = "quat"


def euler_to_so3(angle):
  """Convert euler angles to rotation matrix.

  Args:
      angle: rotation angle along 3 axis (in radians) -- size = [B, 3]
  Returns:
      Rotation matrix corresponding to the euler angles -- size = [B, 3, 3]
  """
  B = angle.size(0)
  x, y, z = angle[:, 0], angle[:, 1], angle[:, 2]

  cosz = torch.cos(z)
  sinz = torch.sin(z)

  zeros = z.detach() * 0
  ones = zeros.detach() + 1
  zmat = torch.stack(
      [cosz, -sinz, zeros, sinz, cosz, zeros, zeros, zeros, ones],
      dim=1).reshape(B, 3, 3)

  cosy = torch.cos(y)
  siny = torch.sin(y)

  ymat = torch.stack(
      [cosy, zeros, siny, zeros, ones, zeros, -siny, zeros, cosy],
      dim=1).reshape(B, 3, 3)

  cosx = torch.cos(x)
  sinx = torch.sin(x)

  xmat = torch.stack(
      [ones, zeros, zeros, zeros, cosx, -sinx, zeros, sinx, cosx],
      dim=1).reshape(B, 3, 3)

  rotMat = xmat @ ymat @ zmat
  return rotMat


def quat_to_so3(quat):
  """Convert quaternion coefficients to rotation matrix.

  Args:
    quat: First three coeff of quaternion of rotation. Fourth is then
      computed to have a norm of 1 -- size = [B, 3]
  Returns:
    Rotation matrix corresponding to the quaternion -- size = [B, 3, 3]
  """
  norm_quat = torch.cat([quat[:, :1].detach() * 0 + 1, quat], dim=1)
  norm_quat = norm_quat / norm_quat.norm(p=2, dim=1, keepdim=True)
  w, x, y, z = norm_quat[:, 0], norm_quat[:, 1], norm_quat[:, 2], norm_quat[:,
                                                                            3]
  B = quat.size(0)

  w2, x2, y2, z2 = w.pow(2), x.pow(2), y.pow(2), z.pow(2)
  wx, wy, wz = w * x, w * y, w * z
  xy, xz, yz = x * y, x * z, y * z

  rotMat = torch.stack([
      w2 + x2 - y2 - z2, 2 * xy - 2 * wz, 2 * wy + 2 * xz, 2 * wz + 2 * xy,
      w2 - x2 + y2 - z2, 2 * yz - 2 * wx, 2 * xz - 2 * wy, 2 * wx + 2 * yz,
      w2 - x2 - y2 + z2
  ],
                       dim=1).reshape(B, 3, 3)
  return rotMat


def pose_vec_to_se3(vec, rotation_mode=RotationMode.EULER):
  """Convert 6DoF parameters to SE(3) transformation matrix.

  Args:
    vec: 6DoF parameters in the order of tx, ty, tz, rx, ry, rz -- [B, 6]
  Returns:
    A transformation matrix -- [B, 4, 4]
  """
  trans = vec[:, :3].unsqueeze(-1)  # [B, 3, 1]
  rot = vec[:, 3:]
  if rotation_mode == RotationMode.EULER:
    so3 = euler_to_so3(rot)  # [B, 3, 3]
  elif rotation_mode == RotationMode.QUAT:
    so3 = quat_to_so3(rot)  # [B, 3, 3]
  return se3_matrix(so3, trans)


def se3_matrix(so3, trans):
  """Return an SE(3) matrix from an SO(3) rotation and a translation.

  Args:
    so3: An n x 3 x 3 tensor with valid SO(3) matrices.
    trans: An n x 3 x 1 tensor with xyz translations.

  Returns:
    An n x 4 x 4 array with SE(3) matrices.
  """
  se3 = torch.cat([so3, trans], axis=-1)
  filler = torch.tensor([0, 0, 0, 1], dtype=torch.float32)
  filler = filler.expand(se3.shape[0], 1, 4)
  se3 = torch.cat([se3, filler], axis=1)
  return se3


def inverse_so3(so3):
  """Compute the inverse so3 rotation matrix.

  Args:
    se3: An n x 3 x 3 tensor with valid SO(3) matrices.

  Returns:
    An n x 3 x 3 tensor with inverse SO(3) matrices.
  """
  return torch.transpose(so3, 1, 2)


def inverse_se3(se3):
  """Compute the inverse se3 matrix.

  Args:
    se3: An n x 4 x 4 tensor with valid SE(3) matrices.

  Returns:
    An n x 4 x 4 tensor with inverse SE(3) matrices.
  """
  so3, trans = se3_split(se3)
  so3_inv = inverse_so3(so3)
  trans_inv = -so3_inv @ trans
  return se3_matrix(so3_inv, trans_inv)


def relative_se3(se3_matrix1, se3_matrix2):
  """Compute the relative SE(3) pose between two SE(3) pose matrices.

  This functions expects se3_matrix2 to be a pose in a time step after
  se3_matrix1 and computes the relative pose as R = M2 * M1^-1. This way it's
  possible to convert relative poses to an absolute pose by always placing the
  lower time step poses in the right hand side of the matrix multiplication,
  such that M3 = R2 * R1 = M3 * M2^-1 * M2 * M1^-1 * M1 = M3.

  Args:
    se3_matrix1: An n x 4 x 4 tensor with SE(3) pose matrices.
    se3_matrix2: Another n x 4 x 4 tensor with SE(3) pose matrices.

  Returns:
    An n x 4 x 4 tensor with the relative poses between the SE(3) matrix in
      the respective index of the input tensors.
  """
  return inverse_se3(se3_matrix1) @ se3_matrix2


def se3_split(se3):
  """Split an SE(3) matrix into rotation and translation matrices.

  Args:
    se3: An n x 4 x 4 tensor with SE(3) matrices.

  Returns:
    so3: An n x 3 x 3 tensor with SO(3) matrices
    trans: An n x 3 x 1 with the translation matrices.
  """
  return se3[:, :3, :3], se3[:, :3, 3:]

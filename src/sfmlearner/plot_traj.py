import sys
from pathlib import Path

import fire
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import torch
from tqdm import tqdm

from src.debug.logger import log_info, log_warning
from src.functions.trajectory import TrajectoryEvalPair
from src.sfmlearner.consts import DEVICE
from src.sfmlearner.data.proc import prepare_torch_input
from src.sfmlearner.data.test_pose_fwk import PoseTestFrameworkKITTI
from src.sfmlearner.functions.transform import (RotationMode, inverse_se3,
                                                pose_vec_to_se3, relative_se3,
                                                se3_split)
from src.sfmlearner.models.pose_net import PoseExpNet

matplotlib.use("TkAgg")

POSE_ARTIFACTS_DIR = "pose_eval"


def evaluate_sample(sample, poses_tgt_to_src):
  """Evaluate a sample's poses.

  Args:
    sample: Dictionary object with the 'poses' key populated with ground-truth
      absolute poses for the predicted `poses_tgt_to_src`.
    poses_tgt_to_src: An n x 4 x 4 numpy array with the relative poses predicted
      by PoseNet from the target image to the source (i.e. ref) images adjacent
      to it in the sample.

  Returns:
    Tuple of computed error metrics.
  """
  # Convert the predicted poses from the target frame to the reference frames
  # surrounding it to relative poses from each frame to the first frame in the
  # sample sequence.
  # For a sequence of 3 frames with indices 1, 2, and 3, we are performing the
  # following operation, where Ti->j is the transformation from frame i to j:
  #   T2->1 @ [T1->2, T2->2, T3->2] = [T1->1, T2->1, T3->1]
  poses_src_to_tgt = inverse_se3(torch.tensor(poses_tgt_to_src)).numpy()
  first_pose_tgt_to_src = poses_tgt_to_src[0]
  pred_rel_poses = first_pose_tgt_to_src @ poses_src_to_tgt

  # Convert ground-truth absolute poses to relative poses from the each frame
  # in the sample to the first one.
  # For a given sample of 3 frames i, i+1, and i+2, we are performing the
  # following operation:
  #   Ti->1 @ [T1->i, T1->(i+1), T1->(i+2)] = [T1->1, T(i+1)->i, T(i+2)->i]
  sample_abs_poses = sample["poses"]
  first_abs_pose_inv = inverse_se3(torch.tensor(sample_abs_poses[:1])).numpy()
  sample_rel_poses = first_abs_pose_inv @ sample_abs_poses

  ate = compute_ate(pred_rel_poses, sample_rel_poses)
  re = compute_re(pred_rel_poses, sample_rel_poses)
  return ate, re


def print_errors(errors, error_names, file=sys.stdout):
  """Print the mean and standar deviation for the computed errors."""
  assert errors.shape[1] == len(error_names)
  mean_errors = np.mean(errors, axis=0)
  std_errors = np.std(errors, axis=0)
  log_info(f"{'Metric':8s}|{'Mean':>8s} ± {'Std':8s}", file=file)
  for name, mean, std in zip(error_names, mean_errors, std_errors):
    log_info(f"{name:8s}|{mean:8.4f} ± {std:<8.4f}", file=file)


@torch.no_grad()
def eval_seq(model_path,
             dataset_dir,
             img_height=128,
             img_width=416,
             no_resize=False,
             output_dir=None,
             sequences=["09", "10"],
             rotation_mode=RotationMode.EULER):
  pose_net = PoseExpNet(
      num_ref_imgs=None, output_exp=False, load_path=model_path).to(DEVICE)
  pose_net.eval()
  seq_length = pose_net.num_ref_imgs + 1

  if output_dir is not None:
    output_dir = Path(output_dir) / POSE_ARTIFACTS_DIR
    output_dir.mkdir(parents=True, exist_ok=False)
  else:
    output_dir = Path(model_path).parent / POSE_ARTIFACTS_DIR
    output_dir.mkdir(exist_ok=True)
  log_warning(f"Saving evaluation artifacts to {output_dir}.")

  dataset_dir = Path(dataset_dir)

  for sequence in sequences:
    sequence = Path(str(sequence))
    framework = PoseTestFrameworkKITTI(dataset_dir, [sequence], seq_length)

    log_info(f"Total of {len(framework)} samples to test.")

    errors = []
    pred_rel_poses, true_abs_poses = [], []
    for sample in tqdm(framework, colour="yellow"):
      imgs = sample['imgs']
      ref_imgs = [
          prepare_torch_input(
              img, height=img_height, width=img_width, no_resize=no_resize)
          for img in imgs
      ]
      tgt_img = ref_imgs.pop(seq_length // 2)
      _, pose_vecs = pose_net(tgt_img, ref_imgs)
      pose_vecs = pose_vecs.cpu()[0]
      pose_vecs = torch.cat([
          pose_vecs[:seq_length // 2],
          torch.zeros(1, 6, dtype=torch.float32),
          pose_vecs[seq_length // 2:],
      ])
      poses_tgt_to_src = pose_vec_to_se3(pose_vecs, rotation_mode=rotation_mode)
      poses_tgt_to_src = poses_tgt_to_src.numpy()
      ate, re = evaluate_sample(sample, poses_tgt_to_src)
      errors.append([ate, re])
      first_forward_pose_index = seq_length // 2 + 1

      pred_rel_poses.append(poses_tgt_to_src[first_forward_pose_index])
      true_abs_poses.append(sample['poses'][first_forward_pose_index])

    # Print computed error metrics
    print_errors(np.array(errors), ["ATE", "RE"])

    # Plot predicted and ground-truth trajectories
    seq_output_dir = Path(output_dir) / sequence
    seq_output_dir.mkdir(parents=True, exist_ok=True)
    with open(seq_output_dir / "metrics.txt", "w") as f:
      print_errors(np.array(errors), ["ATE", "RE"], file=f)
    np.save(seq_output_dir / "pred-rel-poses.npy", np.array(pred_rel_poses))
    np.save(seq_output_dir / "true-abs-poses.npy", np.array(true_abs_poses))
    traj = TrajectoryEvalPair(
        np.array(true_abs_poses), np.array(pred_rel_poses), rel=True)
    for name, fig in traj.figs.items():
      fig.waitforbuttonpress()
      if seq_output_dir is not None:
        fig.savefig(seq_output_dir / f"{name}.pdf")
      plt.close(fig)


def compute_ate(preds, trues):
  """Compute the Averate Translation Error (ATE) between poses.

  Args:
    preds: An (N, 4, 4) array with the predicted SE3 poses.
    trues: An (N, 4, 4) array with the ground-truth SE3 poses.

  Returns:
    A scalar with the mean ATE error.
  """
  seq_length = preds.shape[0]
  preds_trans = se3_split(preds)[1]
  trues_trans = se3_split(trues)[1]
  scale_factor = np.sum(preds_trans * trues_trans / np.sum(preds_trans**2))
  # TODO: Check if it's better/correct to get the L2 error between each
  # pose's translation vector or the L2 error between all elements scaled
  # by the sequence length.
  ate = np.linalg.norm(trues_trans - scale_factor * preds_trans)
  return ate / seq_length


def compute_sin(so3):
  """Compute sine of rotation matrix."""
  _, r01, r02 = so3[:, 0].T
  r10, _, r12 = so3[:, 1].T
  r20, r21, _ = so3[:, 2].T
  return np.linalg.norm([r01 - r10, r12 - r21, r02 - r20], axis=0)


def compute_cos(so3):
  """Compute cosine of rotation matrix."""
  return np.trace(so3, axis1=1, axis2=2) - 1


def compute_re(preds, trues):
  """Compute the Rotational Error (RE) between poses.

  Args:
    preds: An (N, 4, 4) array with the predicted SE3 poses.
    trues: An (N, 4, 4) array with the ground-truth SE3 poses.

  Returns:
    A scalar with the mean RE error.
  """
  pose_error = relative_se3(torch.tensor(preds), torch.tensor(trues))
  rot_error = se3_split(pose_error)[0].numpy()
  rot_sin = compute_sin(rot_error)
  rot_cos = compute_cos(rot_error)
  rot_angle = np.arctan2(rot_sin, rot_cos)
  return np.mean(rot_angle)


if __name__ == '__main__':
  fire.Fire(eval_seq)

import sys
from pathlib import Path

import cv2
import fire
import matplotlib.cm as cm
import numpy as np
import torch
from scipy.ndimage.interpolation import zoom
from skimage.transform import resize
from tqdm import tqdm

from src.debug.logger import log_info, log_warning
from src.sfmlearner.consts import DEVICE
from src.sfmlearner.data.proc import prepare_torch_input
from src.sfmlearner.data.test_depth_fwk import DepthTestFrameworkKITTI
from src.sfmlearner.models.disp_net import DispNet
from src.sfmlearner.models.pose_net import PoseExpNet

DEPTH_ARTIFACTS_DIR = "depth_eval"
ERROR_NAMES = [
    'abs_diff', 'abs_rel', 'sq_rel', 'rms', 'log_rms', 'abs_log', 'a1', 'a2',
    'a3'
]
CMAP = cm.get_cmap("magma")


def save_depth_video(depth_frames, output_dir):
  """Save video with original frames next to gt and predicted depth.

  Shapes of input frames are:
    orig: (H, W, 3)
    true_disp: (H, W)
    pred_disp: (H, W)
  """
  # Compute frame height and width.
  orig, true_disp, pred_disp = depth_frames[0]
  oh, ow = orig.shape[:2]
  th, tw = true_disp.shape[:2]
  ph, pw = pred_disp.shape[:2]
  max_w = max(ow, tw, pw)
  total_h = oh + th + ph

  # Create VideoWriter object
  fourcc = cv2.VideoWriter_fourcc(*'XVID')
  out = cv2.VideoWriter(
      str(output_dir / "preds.avi"), fourcc, 5, (max_w, total_h))
  log_info("Processing predictions video...")
  for orig, true_disp, pred_disp in tqdm(depth_frames, colour="yellow"):
    # Colorize disparity maps.
    true_disp = CMAP(true_disp / np.max(true_disp), bytes=True)[..., :3]
    pred_disp = CMAP(pred_disp / np.max(pred_disp), bytes=True)[..., :3]

    frame = np.empty((total_h, max_w, 3), dtype=np.uint8)
    frame[:oh] = orig
    frame[oh:oh + th] = true_disp
    frame[oh + th:] = pred_disp

    frame = cv2.cvtColor(frame, cv2.COLOR_RGB2BGR)
    out.write(frame)
  out.release()


def log_depth_errors(mean_errors, error_names, file=sys.stdout):
  """Print table with error names and mean values."""
  assert len(mean_errors) == len(error_names)
  name_single_format = r"{:>10}"
  name_format = " | ".join([name_single_format] * len(error_names))
  log_info(name_format.format(*error_names), file=file)
  mean_single_format = r"{:10.4f}"
  mean_format = " | ".join([mean_single_format] * len(mean_errors))
  log_info(mean_format.format(*mean_errors), file=file)


def log_depth_info(med_scaled_errors, pose_scaled_errors, file=sys.stdout):
  """Log all depth information to file."""
  log_info(
      "Results with scale factor determined by ground-truth/prediction ratio:",
      file=file)
  med_scaled_mean_errors = np.mean(np.array(med_scaled_errors), axis=0)
  log_depth_errors(med_scaled_mean_errors, ERROR_NAMES, file=file)

  if pose_scaled_errors:
    log_info("Results with scale determined by PoseNet:", file=file)
    pose_scaled_mean_errors = np.mean(np.array(pose_scaled_errors), axis=0)
    log_depth_errors(pose_scaled_mean_errors, ERROR_NAMES, file=file)


def compute_errors(gt, pred):
  """Compute all depth-related metric errors."""
  thresh = np.maximum((gt / pred), (pred / gt))
  a1 = (thresh < 1.25).mean()
  a2 = (thresh < 1.25**2).mean()
  a3 = (thresh < 1.25**3).mean()

  rmse = (gt - pred)**2
  rmse = np.sqrt(rmse.mean())

  rmse_log = (np.log(gt) - np.log(pred))**2
  rmse_log = np.sqrt(rmse_log.mean())

  abs_log = np.mean(np.abs(np.log(gt) - np.log(pred)))

  abs_rel = np.mean(np.abs(gt - pred) / gt)
  abs_diff = np.mean(np.abs(gt - pred))

  sq_rel = np.mean(((gt - pred)**2) / gt)

  return abs_diff, abs_rel, sq_rel, rmse, rmse_log, abs_log, a1, a2, a3


@torch.no_grad()
def eval_depth(dispnet_path,
               dataset_dir,
               posenet_path=None,
               dataset_file=None,
               img_height=128,
               img_width=416,
               no_resize=False,
               min_depth=1e-3,
               max_depth=80,
               gps=False,
               img_ext="png",
               output_dir=None):
  disp_net = DispNet(load_path=dispnet_path).to(DEVICE)
  disp_net.eval()

  if posenet_path is None:
    seq_length = 1
    log_info("No PoseNet specified. The scale_factor will be determined by "
             "median ratio.")
  else:
    pose_net = PoseExpNet(
        num_ref_imgs=None, output_exp=False, load_path=posenet_path).to(DEVICE)
    seq_length = pose_net.num_ref_imgs + 1

  if output_dir is not None:
    output_dir = Path(output_dir) / DEPTH_ARTIFACTS_DIR
    output_dir.mkdir(parents=True, exist_ok=False)
  else:
    output_dir = Path(dispnet_path).parent / DEPTH_ARTIFACTS_DIR
    output_dir.mkdir(exist_ok=True)
  log_warning(f"Saving evaluation artifacts to {output_dir}.")

  dataset_dir = Path(dataset_dir)
  if dataset_file is not None:
    with open(dataset_file, "r") as f:
      test_files = f.read().splitlines()
  else:
    test_files = [
        f.relative_to(dataset_dir) for f in dataset_dir.glob(f"*.{img_ext}")
    ]
  log_info(f"Total of {len(test_files)} files to test.")

  framework = DepthTestFrameworkKITTI(
      dataset_dir, test_files, seq_length, min_depth, max_depth, use_gps=gps)

  depth_frames = []
  med_scaled_errors, pose_scaled_errors = [], []
  for sample in tqdm(framework, colour="yellow"):
    tgt_img = prepare_torch_input(sample["tgt"], img_height, img_width,
                                  no_resize)
    ref_imgs = [
        prepare_torch_input(ref_img, img_height, img_width, no_resize)
        for ref_img in sample["ref"]
    ]

    true_depth = sample["gt_depth"]

    pred_disp = disp_net(tgt_img).cpu().numpy()[0, 0]
    pred_depth = 1 / pred_disp

    pred_depth_zoomed = zoom(pred_depth,
                             (true_depth.shape[0] / pred_depth.shape[0],
                              true_depth.shape[1] / pred_depth.shape[1])).clip(
                                  min_depth, max_depth)

    # Ignore pixels where the depth is smaller than min_depth or greater than
    # max_depth. Also, filter out pixels that don't belong to Eigen et. al's
    # crops.
    if sample["mask"] is not None:
      true_depth = true_depth[sample["mask"]]
      pred_depth_zoomed = pred_depth_zoomed[sample["mask"]]

    # Compute median-scaled error
    med_scale_factor = np.median(true_depth) / np.median(pred_depth_zoomed)
    med_scaled_error = compute_errors(true_depth,
                                      pred_depth_zoomed * med_scale_factor)
    med_scaled_errors.append(med_scaled_error)

    # Append original frame, ground-truth depth and predicted depth to compose
    # video of predictions.
    true_depth_clipped = resize(
        true_depth, pred_depth.shape,
        anti_aliasing=True).clip(min_depth, max_depth)
    pred_depth_clipped = pred_depth.clip(min_depth, max_depth)
    tgt_img_resized = resize(
        sample["tgt"], pred_depth.shape, anti_aliasing=True)
    depth_frames.append(
        (tgt_img_resized, 1 / true_depth_clipped, 1 / pred_depth_clipped))

    # Compute pose-scaled error
    if seq_length > 1:
      # Reorganize ref_imgs: tgt is middle frame but not necessarily the one
      # used in DispNetS (in case sample to test was in end or beginning of the
      # image sequence)
      middle_index = seq_length // 2
      tgt = ref_imgs[middle_index]
      reorganized_refs = ref_imgs[:middle_index] + ref_imgs[middle_index + 1:]
      _, poses = pose_net(tgt, reorganized_refs)
      displacement_magnitudes = poses[0, :, :3].norm(2, 1).cpu().numpy()

      scale_factor = np.mean(sample['displacements'] / displacement_magnitudes)
      pose_scaled_error = compute_errors(true_depth,
                                         pred_depth_zoomed * scale_factor)
      pose_scaled_errors.append(pose_scaled_error)

  log_depth_info(med_scaled_errors, pose_scaled_errors)
  with open(output_dir / "metrics.txt", "w") as f:
    log_depth_info(med_scaled_errors, pose_scaled_errors, file=f)

  save_depth_video(depth_frames, output_dir)


if __name__ == '__main__':
  fire.Fire(eval_depth)

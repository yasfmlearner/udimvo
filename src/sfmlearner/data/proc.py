"""Pre-processing functions."""
import numpy as np
import torch
from skimage.transform import resize

from src.sfmlearner.consts import DEVICE


def prepare_torch_input(img, height, width, no_resize=False):
  """Prepare image to feed models.

  Args:
      img: Numpy array image with shape (H, W, C).
      height: The desired image height.
      width: The desired image width.
      no_resize: Whether to resize the image or not. If True, height and width
        are ignored.

  Returns:
      A torch tensor ready to feed the network with shape (1, C, H, W).
  """
  h, w, _ = img.shape
  if (not no_resize) and (h != height or w != width):
    img = resize(img, (height, width), anti_aliasing=True)
  img = np.transpose(img, (2, 0, 1))
  img = (img / 255 - 0.5) / 0.5
  torch_img = torch.from_numpy(img).unsqueeze(0)
  return torch_img.to(DEVICE)

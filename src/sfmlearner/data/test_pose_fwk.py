from pathlib import Path

import numpy as np
import torch
from skimage.io import imread
from tqdm import tqdm

from src.debug.logger import log_info
from src.sfmlearner.functions.transform import se3_matrix


class PoseTestFrameworkKITTI:
  """Generator class for KITTI test samples for pose estimation."""

  def __init__(self, root, sequence_set, seq_length=3, step=1):
    self.root = root
    seqs_data = read_kitti_odom_scene_data(
        data_root=self.root,
        sequence_set=sequence_set,
        seq_length=seq_length,
        step=step,
    )
    self.seqs_img_fnames, self.seqs_poses, self.seqs_samples_indices = seqs_data

  def __iter__(self):
    for img_fnames, poses, samples_indices in zip(self.seqs_img_fnames,
                                                  self.seqs_poses,
                                                  self.seqs_samples_indices):
      for sample_indices in samples_indices:
        sample_imgs = [
            imread(img_fnames[i]).astype(np.float32) for i in sample_indices
        ]
        sample_abs_poses = poses[sample_indices]
        yield {"imgs": sample_imgs, "poses": sample_abs_poses}

  def __len__(self):
    """Total number of samples for all sequences."""
    return sum(
        len(samples_indices) for samples_indices in self.seqs_samples_indices)


def read_kitti_odom_scene_data(data_root, sequence_set, seq_length=3, step=1):
  """Create data samples for all KITTI sequences in `sequence_set`.

  Args:
    data_root: Root odometry dataset directory string.
    sequence_set: List of strings with the KITTI sequence ids we wish to
        generate samples for (e.g. '01', '09', '10').
    seq_length: Integer for the number of sequential frames per sample.
    step: Integer step between sequential frames in a sample. If step = 1,
        we sample consecutive frames, such as [-1, 0, 1]. If step = 2, we skip
        one frame between each one in the sequence, such as [-2, 0, 2].

  Returns:
    img_seqs: List of lists with the filenames of each KITTI sequence in the
        `sequence_set`.
    pose_seqs: List of numpy arrays with shape (N, 3, 4), where N is the length
        of the sequence the poses belong to.
    index_seqs: List of numpy arrays with shape (N - seq_length + 1, seq_length)
        with the indices of the frames for each sample in a given sequence.
  """
  assert seq_length % 2 == 1, "Sequence length must be an odd integer."
  half_length = seq_length // 2
  shift_range = step * np.arange(-half_length, half_length + 1)
  shift_range = np.expand_dims(shift_range, 0)

  data_root = Path(data_root)
  seqs = set()
  for seq in sequence_set:
    seq_dir = data_root / "sequences" / seq
    if not seq_dir.is_dir():
      raise ValueError(f"Sequence directory {seq_dir} doesn't exist.")
    seqs.add(seq_dir)
  log_info(f"Getting test metadata for sequences: {[seq.name for seq in seqs]}")

  img_seqs, pose_seqs, index_seqs = [], [], []
  for seq in tqdm(seqs, colour="yellow"):
    pose_fname = data_root / "poses" / f"{seq.name}.txt"
    poses = np.genfromtxt(pose_fname, dtype=np.float32).reshape(-1, 3, 4)
    poses_se3 = se3_matrix(
        torch.tensor(poses[:, :3, :3]), torch.tensor(poses[:, :, 3:])).numpy()
    imgs_path = seq / "image_2"
    img_fnames = sorted(imgs_path.glob("*.png"))
    tgt_indices = np.arange(half_length, len(img_fnames) - half_length)
    tgt_indices = np.expand_dims(tgt_indices, -1)
    sample_indices = shift_range + tgt_indices
    img_seqs.append(img_fnames)
    pose_seqs.append(poses_se3)
    index_seqs.append(sample_indices)
  return img_seqs, pose_seqs, index_seqs

"""Torch model building blocks."""
import torch.nn as nn
from torch.nn.init import xavier_uniform_, zeros_


def xavier_zeros_initializer(modules):
  """Initialize all kernel weights with Xavier uniform and bias with zeros.

  Args:
    modules: A sequence of torch Modules.
  """
  for m in modules:
    if isinstance(m, (nn.Conv2d, nn.ConvTranspose2d)):
      xavier_uniform_(m.weight.data)
      if m.bias is not None:
        zeros_(m.bias)


def conv(in_planes, out_planes, kernel_size=3, stride=1):
  """Convolutional layer with padding that preserves input dimensions."""
  return nn.Conv2d(
      in_planes,
      out_planes,
      kernel_size=kernel_size,
      padding=kernel_size // 2,
      stride=stride,
  )


def convsigmoid(in_planes, out_planes, kernel_size=3, stride=1):
  """Convolutional layer followed by sigmoid activation."""
  return nn.Sequential(
      conv(in_planes, out_planes, kernel_size=kernel_size, stride=stride),
      nn.Sigmoid(),
  )


def convrelu(in_planes, out_planes, kernel_size=3, stride=1):
  """Convolutional layer followed by ReLU activation."""
  return nn.Sequential(
      conv(in_planes, out_planes, kernel_size=kernel_size, stride=stride),
      nn.ReLU(inplace=True),
  )


def downconv(in_planes, out_planes, kernel_size=3):
  """Return module with convolution followed by ReLU activation."""
  return convrelu(in_planes, out_planes, kernel_size=kernel_size, stride=2)


def upconv(in_planes, out_planes, kernel_size=3, padding=1, output_padding=0):
  """Return module with transpose convolution followed by ReLU activation."""
  return nn.Sequential(
      nn.ConvTranspose2d(
          in_planes,
          out_planes,
          kernel_size=kernel_size,
          stride=2,
          padding=padding,
          output_padding=output_padding),
      nn.ReLU(inplace=True),
  )


def crop_like(inputs, target):
  """Crop inputs height and width dimensions to match target shape."""
  assert (inputs.size(2) >= target.size(2) and inputs.size(3) >= target.size(3))
  return inputs[:, :, :target.size(2), :target.size(3)]

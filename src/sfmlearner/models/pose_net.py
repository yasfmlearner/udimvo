"""Pose estimation network module."""
import torch
import torch.nn as nn

from src.sfmlearner.models.utils import (convsigmoid, crop_like, downconv,
                                         upconv, xavier_zeros_initializer)


def predict_exp(in_planes, out_planes, kernel_size=3):
  """Output a disparity map with a single channel."""
  return convsigmoid(in_planes, out_planes, kernel_size=kernel_size)


class PoseExpNet(nn.Module):
  """Model for pose and explainability mask estimation."""

  def __init__(self, num_ref_imgs=2, output_exp=False, load_path=None):
    super().__init__()
    self.output_exp = output_exp
    self.num_ref_imgs = num_ref_imgs
    self.weights = None
    if load_path is not None:
      self.weights = torch.load(load_path)
      seq_length = self.weights['state_dict']['conv1.0.weight'].size(1) // 3
      self.num_ref_imgs = seq_length - 1

    conv_planes = [16, 32, 64, 128, 256, 256, 256]
    self.conv1 = downconv(
        3 * (1 + self.num_ref_imgs), conv_planes[0], kernel_size=7)
    self.conv2 = downconv(conv_planes[0], conv_planes[1], kernel_size=5)
    self.conv3 = downconv(conv_planes[1], conv_planes[2])
    self.conv4 = downconv(conv_planes[2], conv_planes[3])
    self.conv5 = downconv(conv_planes[3], conv_planes[4])
    self.conv6 = downconv(conv_planes[4], conv_planes[5])
    self.conv7 = downconv(conv_planes[5], conv_planes[6])

    self.pose_pred = nn.Conv2d(
        conv_planes[6], 6 * self.num_ref_imgs, kernel_size=1, padding=0)

    if self.output_exp:
      upconv_planes = [256, 128, 64, 32, 16]
      self.upconv5 = upconv(conv_planes[4], upconv_planes[0], kernel_size=4)
      self.upconv4 = upconv(upconv_planes[0], upconv_planes[1], kernel_size=4)
      self.upconv3 = upconv(upconv_planes[1], upconv_planes[2], kernel_size=4)
      self.upconv2 = upconv(upconv_planes[2], upconv_planes[3], kernel_size=4)
      self.upconv1 = upconv(upconv_planes[3], upconv_planes[4], kernel_size=4)

      self.predict_mask4 = predict_exp(upconv_planes[1], self.num_ref_imgs)
      self.predict_mask3 = predict_exp(upconv_planes[2], self.num_ref_imgs)
      self.predict_mask2 = predict_exp(upconv_planes[3], self.num_ref_imgs)
      self.predict_mask1 = predict_exp(upconv_planes[4], self.num_ref_imgs)

    if self.weights is not None:
      self.load_state_dict(self.weights["state_dict"], strict=False)

  def init_weights(self):
    """Initialize kernels with Xavier uniform and bias with zeros."""
    xavier_zeros_initializer(self.modules())

  def forward(self, tgt_img, ref_imgs):
    """Output estimated pose with explainability masks."""
    assert len(ref_imgs) == self.num_ref_imgs, (
        f"Unexpected number of reference images. Expected {self.num_ref_imgs} "
        f"and got {len(ref_imgs)}.")
    inputs = torch.cat([tgt_img] + ref_imgs, 1)
    out_conv1 = self.conv1(inputs)
    out_conv2 = self.conv2(out_conv1)
    out_conv3 = self.conv3(out_conv2)
    out_conv4 = self.conv4(out_conv3)
    out_conv5 = self.conv5(out_conv4)
    out_conv6 = self.conv6(out_conv5)
    out_conv7 = self.conv7(out_conv6)

    pose = self.pose_pred(out_conv7)
    pose = pose.mean(3).mean(2)
    pose = 0.01 * pose.view(pose.size(0), self.num_ref_imgs, 6)

    if self.output_exp:
      out_upconv5 = crop_like(self.upconv5(out_conv5), out_conv4)
      out_upconv4 = crop_like(self.upconv4(out_upconv5), out_conv3)
      out_upconv3 = crop_like(self.upconv3(out_upconv4), out_conv2)
      out_upconv2 = crop_like(self.upconv2(out_upconv3), out_conv1)
      out_upconv1 = crop_like(self.upconv1(out_upconv2), inputs)
      exp_mask4 = self.predict_mask4(out_upconv4)
      exp_mask3 = self.predict_mask3(out_upconv3)
      exp_mask2 = self.predict_mask2(out_upconv2)
      exp_mask1 = self.predict_mask1(out_upconv1)
    else:
      exp_mask4 = None
      exp_mask3 = None
      exp_mask2 = None
      exp_mask1 = None

    if self.training:
      return [exp_mask1, exp_mask2, exp_mask3, exp_mask4], pose
    return exp_mask1, pose

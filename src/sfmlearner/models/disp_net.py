"""Depth estimation network module."""
import re

import torch
import torch.nn as nn
import torch.nn.functional as F

from src.sfmlearner.models.utils import (convrelu, convsigmoid, crop_like,
                                         downconv, upconv,
                                         xavier_zeros_initializer)


def state_dict_mismatch(state_dict1, state_dict2):
  """Verify if there's a key mismatch between state dictionaries."""
  return set(state_dict1.keys()) != set(state_dict2.keys())


def update_legacy_state_dict(leg_state_dict):
  """Update legacy state dict to new version."""
  conv_pattern = re.compile(r"([0-9])\.([02])")
  new_state_dict = {}
  for key in leg_state_dict.keys():
    if key.startswith("conv"):
      conv_match = conv_pattern.search(key)
      layer_id, layer_subid = map(int, conv_match.groups())
      new_layer_id = f"{layer_id}.{layer_subid // 2}.0"
      old_layer_id = conv_match.group()
      new_key = key.replace(old_layer_id, new_layer_id)
    else:
      new_key = key
    new_state_dict[new_key] = leg_state_dict[key]
  return new_state_dict


def predict_disp(in_planes, kernel_size=3):
  """Output a disparity map with a single channel."""
  return convsigmoid(in_planes, out_planes=1, kernel_size=kernel_size)


def downsample_block(in_planes, out_planes, kernel_size=3):
  """Downsampling block for DispNet."""
  return nn.Sequential(
      downconv(in_planes, out_planes, kernel_size=kernel_size),
      convrelu(out_planes, out_planes, kernel_size=kernel_size),
  )


def upsample_block(in_planes, out_planes):
  """Upsampling block for DispNet."""
  return upconv(
      in_planes,
      out_planes,
      kernel_size=3,
      padding=1,
      output_padding=1,
  )


class DispNet(nn.Module):
  """Model for disparity map estimation."""

  def __init__(self, alpha=10, beta=0.01, load_path=None):
    super().__init__()

    self.alpha = alpha
    self.beta = beta

    conv_planes = [32, 64, 128, 256, 512, 512, 512]
    self.conv1 = downsample_block(3, conv_planes[0], kernel_size=7)
    self.conv2 = downsample_block(conv_planes[0], conv_planes[1], kernel_size=5)
    self.conv3 = downsample_block(conv_planes[1], conv_planes[2])
    self.conv4 = downsample_block(conv_planes[2], conv_planes[3])
    self.conv5 = downsample_block(conv_planes[3], conv_planes[4])
    self.conv6 = downsample_block(conv_planes[4], conv_planes[5])
    self.conv7 = downsample_block(conv_planes[5], conv_planes[6])

    upconv_planes = [512, 512, 256, 128, 64, 32, 16]
    self.upconv7 = upsample_block(conv_planes[6], upconv_planes[0])
    self.upconv6 = upsample_block(upconv_planes[0], upconv_planes[1])
    self.upconv5 = upsample_block(upconv_planes[1], upconv_planes[2])
    self.upconv4 = upsample_block(upconv_planes[2], upconv_planes[3])
    self.upconv3 = upsample_block(upconv_planes[3], upconv_planes[4])
    self.upconv2 = upsample_block(upconv_planes[4], upconv_planes[5])
    self.upconv1 = upsample_block(upconv_planes[5], upconv_planes[6])

    self.iconv7 = convrelu(upconv_planes[0] + conv_planes[5], upconv_planes[0])
    self.iconv6 = convrelu(upconv_planes[1] + conv_planes[4], upconv_planes[1])
    self.iconv5 = convrelu(upconv_planes[2] + conv_planes[3], upconv_planes[2])
    self.iconv4 = convrelu(upconv_planes[3] + conv_planes[2], upconv_planes[3])
    self.iconv3 = convrelu(1 + upconv_planes[4] + conv_planes[1],
                           upconv_planes[4])
    self.iconv2 = convrelu(1 + upconv_planes[5] + conv_planes[0],
                           upconv_planes[5])
    self.iconv1 = convrelu(1 + upconv_planes[6], upconv_planes[6])

    self.predict_disp4 = predict_disp(upconv_planes[3])
    self.predict_disp3 = predict_disp(upconv_planes[4])
    self.predict_disp2 = predict_disp(upconv_planes[5])
    self.predict_disp1 = predict_disp(upconv_planes[6])

    if load_path is not None:
      state_dict = torch.load(load_path)["state_dict"]
      if state_dict_mismatch(state_dict, self.state_dict()):
        state_dict = update_legacy_state_dict(state_dict)
      self.load_state_dict(state_dict)

  def init_weights(self):
    """Initialize kernels with Xavier uniform and bias with zeros."""
    xavier_zeros_initializer(self.modules())

  def forward(self, x):
    """Output final disparity map for evaluation and all scales for training."""
    out_conv1 = self.conv1(x)
    out_conv2 = self.conv2(out_conv1)
    out_conv3 = self.conv3(out_conv2)
    out_conv4 = self.conv4(out_conv3)
    out_conv5 = self.conv5(out_conv4)
    out_conv6 = self.conv6(out_conv5)
    out_conv7 = self.conv7(out_conv6)

    out_upconv7 = crop_like(self.upconv7(out_conv7), out_conv6)
    concat7 = torch.cat((out_upconv7, out_conv6), 1)
    out_iconv7 = self.iconv7(concat7)

    out_upconv6 = crop_like(self.upconv6(out_iconv7), out_conv5)
    concat6 = torch.cat((out_upconv6, out_conv5), 1)
    out_iconv6 = self.iconv6(concat6)

    out_upconv5 = crop_like(self.upconv5(out_iconv6), out_conv4)
    concat5 = torch.cat((out_upconv5, out_conv4), 1)
    out_iconv5 = self.iconv5(concat5)

    out_upconv4 = crop_like(self.upconv4(out_iconv5), out_conv3)
    concat4 = torch.cat((out_upconv4, out_conv3), 1)
    out_iconv4 = self.iconv4(concat4)
    disp4 = self.alpha * self.predict_disp4(out_iconv4) + self.beta

    out_upconv3 = crop_like(self.upconv3(out_iconv4), out_conv2)
    disp4_up = crop_like(
        F.interpolate(
            disp4, scale_factor=2, mode="bilinear", align_corners=False),
        out_conv2)
    concat3 = torch.cat((out_upconv3, out_conv2, disp4_up), 1)
    out_iconv3 = self.iconv3(concat3)
    disp3 = self.alpha * self.predict_disp3(out_iconv3) + self.beta

    out_upconv2 = crop_like(self.upconv2(out_iconv3), out_conv1)
    disp3_up = crop_like(
        F.interpolate(
            disp3, scale_factor=2, mode="bilinear", align_corners=False),
        out_conv1)
    concat2 = torch.cat((out_upconv2, out_conv1, disp3_up), 1)
    out_iconv2 = self.iconv2(concat2)
    disp2 = self.alpha * self.predict_disp2(out_iconv2) + self.beta

    out_upconv1 = crop_like(self.upconv1(out_iconv2), x)
    disp2_up = crop_like(
        F.interpolate(
            disp2, scale_factor=2, mode="bilinear", align_corners=False), x)
    concat1 = torch.cat((out_upconv1, disp2_up), 1)
    out_iconv1 = self.iconv1(concat1)
    disp1 = self.alpha * self.predict_disp1(out_iconv1) + self.beta

    if self.training:
      return disp1, disp2, disp3, disp4
    return disp1

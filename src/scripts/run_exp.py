"""Script to run an experiment with multiple configurations."""
import argparse

import src.config.exp as exp

parser = argparse.ArgumentParser()
parser.add_argument(
    "name",
    choices=["converge", "data", "baseline_ctc"],
    help="The name of the experiment to run. Each experiment corresponds to a "
    "set of configs that will be tested.")
parser.add_argument("exp_dir", help="Directory to save the experiment runs to.")
parser.add_argument(
    "-d",
    "--datasets_dir",
    help="Common base directory where datasets are stored.")
parser.add_argument(
    "-g",
    "--gpu_device_id",
    type=int,
    help="The GPU device id to run the experiment on.")
args = parser.parse_args()

experiment = None
if args.name == "converge":
  experiment = exp.ConvergeExperiment(args.exp_dir)
if args.name == "data":
  experiment = exp.DataExperiment(args.exp_dir)
if args.name == "baseline_ctc":
  experiment = exp.BaselineCTCExperiment(args.exp_dir)
if args.datasets_dir is not None:
  experiment.base_config["dataset_config.datasets_dir"] = args.datasets_dir
if args.gpu_device_id is not None:
  experiment.base_config["gpu_device_id"] = args.gpu_device_id
experiment.run()

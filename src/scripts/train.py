# pylint: disable=E1120,wrong-import-position
"""This module defines the UDIMVO class to build and train the model."""
import argparse
import copy
import json
import logging
import os
import shutil
import sys

import absl.flags as flags
import tensorflow as tf
import tensorflow.keras as keras

import src.config.train as train_config
from src.config.types import Rotation
from src.data import get_dataset
from src.data.dataset import EvalSequenceGenerator, SequenceType, TrainSequence
from src.functions import utils
from src.keras.models.udimvo import UDIMVO


@train_config.ex.command
def train(optimizer_name, optimizer_configs, gt_reg_scale, rot_type,
          cur_run_dir, debug, gpu_device_id, resume_error, _config):
  """Train the model with the given config."""
  # Enable eager mode if debug is True
  tf.config.experimental_run_functions_eagerly(debug)

  # Adds assertions to tensorflow graphics package. Checks that rotations are
  # normalized before converting to rotation matrix and that rotation
  # matrices are valid before converting to rotation representation. We only
  # add these checks for quaternions since axis angles typically throw
  # assertion errors due to the tolerance being too low.
  # TODO: Find a way to correct the accumulated error from composing SE(3)
  # matrices and uncomment these assertions.
  # if debug and rot_type == Rotation.quat:
  # flags.FLAGS(["", "--tfg_add_asserts_to_graph"])

  # Set device to run tensorflow
  utils.set_device(gpu_device_id)

  # Create a new unique run directory if we're starting training from scratch.
  config_path = os.path.join(cur_run_dir, "config.json")
  if not os.path.isfile(config_path):
    with open(os.path.join(cur_run_dir, "config.json"), "w") as cf:
      config = copy.deepcopy(_config)
      config["cur_run_dir"] = cur_run_dir
      cf.write(json.dumps(config, indent=2))

  optimizer_config = {
      "class_name": optimizer_name,
      "config": optimizer_configs[optimizer_name],
  }
  optimizer = keras.optimizers.deserialize(optimizer_config)

  # Create metrics
  batch_metrics = {
      "loss": keras.metrics.Mean(name="batch_loss"),
  }
  epoch_metrics = {
      "loss": keras.metrics.Mean(name="loss"),
      "mi": keras.metrics.Mean(name="mi"),
      "ctc_loss": keras.metrics.Mean(name="ctc_loss"),
      "wd": keras.metrics.Mean(name="weight_decay"),
      "gt": keras.metrics.Mean(name="ground_truth_reg"),
      "gr": keras.metrics.Mean(name="grad_reg"),
      "val_ate": keras.metrics.Mean(name="ate"),
      "val_rpe_rot": keras.metrics.Mean(name="rpe_rot"),
      "val_rpe_trans": keras.metrics.Mean(name="rpe_trans"),
  }

  # Create dataset and sequences
  dataset = get_dataset()
  train_sequence = TrainSequence(
      dataset=dataset,
      traj_ids=dataset.get_train_traj_ids(),
      gt_reg_scale=gt_reg_scale,
      rot_type=rot_type)
  val_sequence = EvalSequenceGenerator(
      dataset=dataset,
      traj_ids=dataset.get_train_traj_ids(),
      seq_type=SequenceType.VAL,
      rot_type=rot_type)
  model = UDIMVO(
      dataset=dataset,
      ctc_contract=train_sequence.ctc_contract,
      run_dir=cur_run_dir)
  model.compile(
      optimizer=optimizer,
      batch_metrics=batch_metrics,
      epoch_metrics=epoch_metrics)

  if resume_error:
    model.resume_error()
  else:
    model.fit(dataset=train_sequence, validation_dataset=val_sequence)


if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Process some integers.')
  parser.add_argument(
      "--resume", help="Path to the directory with the run we wish to resume.")
  parser.add_argument(
      "--runs_dir",
      type=str,
      default="runs",
      help="The directory to store the experiment runs.")
  parser.add_argument(
      "-e",
      "--epochs",
      type=int,
      help="The new target number of epochs to resume training for.")
  parser.add_argument(
      "--error",
      action="store_true",
      help="Whether to resume an error for the given run.")
  parser.add_argument(
      "--rm",
      action="store_true",
      help="Whether to remove the run directory after run is complete (i.e. "
      "don't keep any logs or ckpts for the run).")
  args, unknownargs = parser.parse_known_args()

  if args.error and not args.resume:
    parser.error("In order to resume an error, a resume path must be specified "
                 "with --resume.")
  if args.epochs and not args.resume:
    parser.error("You can only specify a new number of epochs when resuming "
                 "trainig.")

  if args.resume:
    cur_run_dir = args.resume
    utils.assert_valid_resume_dir(cur_run_dir)
    with open(os.path.join(cur_run_dir, "config.json"), "r+") as f:
      resume_config = json.load(f)
      resume_config["cur_run_dir"] = cur_run_dir
      if args.epochs:
        resume_config["epochs"] = args.epochs
      f.seek(0)
      f.truncate(0)
      f.write(json.dumps(resume_config, indent=2))
      if args.error:
        resume_config["resume_error"] = True
    config_updates = resume_config
  else:
    config_updates = train_config.parse_sacred_command_line(unknownargs)
    cur_run_dir = utils.unique_dir(args.runs_dir)
    config_updates["cur_run_dir"] = cur_run_dir
    config_updates["runs_dir"] = args.runs_dir
  try:
    train_config.ex.run(command_name="train", config_updates=config_updates)
  finally:
    if args.rm and not args.resume:
      shutil.rmtree(cur_run_dir, ignore_errors=True)
      logging.warning("Removed directory %s", cur_run_dir)

# pylint: disable=no-value-for-parameter
import argparse
import json
import os

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
from tqdm import tqdm

import src.config.train as train_config
from src.data import get_dataset
from src.data.dataset import EvalSequenceGenerator, SequenceType, TrainSequence
from src.functions import lie, trajectory, utils
from src.keras.models.udimvo import UDIMVO

# matplotlib.use('TkAgg')

matplotlib.use("pgf")
matplotlib.rcParams.update({
    "pgf.texsystem": "pdflatex",
    'font.family': 'serif',
    'text.usetex': True,
    'pgf.rcfonts': False,
})

POSE_STRS = ["p_x", "p_y", "p_z", "o_x", "o_y", "o_z", "o_w"]


@train_config.ex.command
def eval(rot_type, optimizer_name, optimizer_configs, cur_run_dir, gt_reg_scale,
         _config):
  gpus = tf.config.experimental.list_physical_devices('GPU')
  tf.config.experimental.set_visible_devices(gpus[0], "GPU")
  tf.config.experimental.set_memory_growth(gpus[0], True)
  dataset = get_dataset()
  train_sequence = TrainSequence(
      dataset=dataset,
      traj_ids=dataset.get_train_traj_ids(),
      gt_reg_scale=gt_reg_scale,
      rot_type=rot_type)
  val_sequence = EvalSequenceGenerator(
      dataset=dataset,
      traj_ids=dataset.get_train_traj_ids()[:1],
      seq_type=SequenceType.VAL,
      rot_type=rot_type)
  optimizer_config = {
      "class_name": optimizer_name,
      "config": optimizer_configs[optimizer_name],
  }
  optimizer = keras.optimizers.deserialize(optimizer_config)
  udimvo = UDIMVO(
      dataset=dataset,
      ctc_contract=train_sequence.ctc_contract,
      run_dir=cur_run_dir)
  udimvo.compile(optimizer=optimizer, batch_metrics={}, epoch_metrics={})
  cur_run_dir = os.path.join(cur_run_dir, "plots")
  os.makedirs(cur_run_dir, exist_ok=True)
  for val_traj_id in val_sequence:
    y_trues, y_preds = [], []
    for x_val_batch, y_val_batch in tqdm(val_traj_id, desc="Val Seqs"):
      y_pred = udimvo.eval_model(x_val_batch, training=False)
      y_trues.append(y_val_batch)
      y_preds.append(y_pred)
    y_true = np.concatenate(y_trues, axis=0)
    y_pred = np.concatenate(y_preds, axis=0)
    abs_y_true = np.concatenate([lie.null_pose(rot_type), y_true], axis=0)
    rel_y_true = lie.abs_to_rel_pose(abs_y_true, rot_type)
    plot_pose_histograms(y_pred, f"traj-{val_traj_id.traj_id}-pred",
                         cur_run_dir)
    plot_pose_histograms(rel_y_true, f"traj-{val_traj_id.traj_id}-true",
                         cur_run_dir)
    traj = trajectory.TrajectoryEvalPair(
        y_true, y_pred, rot_type, f"{val_traj_id.traj_id}", rel=True)
    traj.plot()
    name = os.path.basename(cur_run_dir)
    for key, fig in traj.figs.items():
      key = key.replace("_", "-")
      fig.savefig(
          os.path.join(cur_run_dir, f"traj-{val_traj_id.traj_id}-{key}.pdf"))
      fig.savefig(
          os.path.join(cur_run_dir, f"traj-{val_traj_id.traj_id}-{key}.png"))
      fig.savefig(
          os.path.join(cur_run_dir, f"traj-{val_traj_id.traj_id}-{key}.pgf"))


def plot_pose_histograms(pose, name, base_dir):
  fig, axes = plt.subplots(nrows=3, ncols=2)
  for i in range(pose.shape[-1]):
    row_idx, col_idx = i // 2, i % 2
    ax = axes[row_idx][col_idx]
    ax.hist(pose[:, i], color=(0.95, 0.5, 0), bins=10)
    ax.set_xlabel(f"${POSE_STRS[i]}$")
    ax.set_facecolor('white')
  fig.tight_layout(pad=0.1)
  fig.savefig(os.path.join(base_dir, f"{name}.pgf"))
  fig.savefig(os.path.join(base_dir, f"{name}.png"))
  fig.savefig(os.path.join(base_dir, f"{name}.pdf"))


# tf.summary.histogram(
# f"poses/{pose_strs[i]}/{name}", pose[:, i], buckets=15, step=step)

if __name__ == '__main__':
  parser = argparse.ArgumentParser(description='Process some integers.')
  parser.add_argument(
      "--run_dir",
      help="Path to the directory with the run we wish to evaluate.")
  args, unknownargs = parser.parse_known_args()

  cur_run_dir = args.run_dir
  utils.assert_valid_resume_dir(cur_run_dir)
  with open(os.path.join(cur_run_dir, "config.json"), "r+") as f:
    resume_config = json.load(f)
    resume_config["cur_run_dir"] = cur_run_dir
  train_config.ex.run(command_name="eval", config_updates=resume_config)

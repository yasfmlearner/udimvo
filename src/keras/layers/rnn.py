# pylint: disable=arguments-differ
"""This module defines all recurrent network layers."""
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K

import src.functions.lie as lie
import src.keras.layers.utils as utils


class LSTMBlock(keras.layers.Layer):
  """Maps a series of time distributed poses to a sequence of poses."""

  def __init__(self,
               rot_type,
               sizes=[1024],
               input_shape=None,
               name="lstm_block",
               **kwargs):
    """Instantiate a simple LSTM-based recurrent layer.

    Args:
        rot_type: A Rotation config type.
        sizes: Optional list of integers with the sizes for each of the
            intermediate lstm layers that encode the input time series data
            up to the final dense layer that outputs a pose. If None, the
            LSTMBlock is a no-op and just flattens the batch and time
            series dimension into a single batch dimension. Defaults to
            a single LSTM layer with 1024 units.
        input_shape: The batch input shape to the first LSTM layer in the block.
            This is needed to avoid a spurious error with RNN layers in GPUs
            documented by this issue:
              https://github.com/tensorflow/tensorflow/issues/37942.
    """
    super().__init__(name=name, **kwargs)
    self.rot_type = rot_type
    self.lstm_layers = []
    lstm_kwargs = {
        "return_sequences": True,
        "stateful": False,
        "activation": "tanh"
    }
    for idx, size in enumerate(sizes):
      layer_kwargs = lstm_kwargs.copy()
      layer_kwargs.update({"units": size, "name": f"lstm{idx + 1}"})
      if idx == 0:
        self.lstm_layers.append(
            keras.layers.LSTM(batch_input_shape=input_shape, **layer_kwargs))
      else:
        self.lstm_layers.append(keras.layers.LSTM(**layer_kwargs))
    self.recurrent_output_units = sizes[-1]
    self.lstm_output = utils.dense_layer(self.rot_type, name="lstm_output")

  def call(self, inputs):  # noqa: D102
    x = inputs
    for layer in self.lstm_layers:
      x = layer(x)
    x = K.reshape(x, (-1, self.recurrent_output_units))
    poses = self.lstm_output(x)
    normalized_poses = lie.normalize_pose(poses, self.rot_type)
    return normalized_poses

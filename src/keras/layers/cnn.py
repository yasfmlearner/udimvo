# pylint: disable=arguments-differ
"""This module defines all convolutional networks used to build our model."""
import h5py
import numpy as np
import tensorflow.keras as keras
from tensorflow.python.keras.saving import hdf5_format
from tensorflow.python.keras.utils import data_utils

from src.keras.layers.utils import Conv2DBatchNorm

WEIGHTS_PATH_NO_TOP = ('https://storage.googleapis.com/tensorflow/'
                       'keras-applications/vgg16/'
                       'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5')


class VGG16(keras.layers.Layer):
  """VGG16 model as a keras Layer.

  This version of the VGG16 model accepts inputs with 6 channels, which
  are needed for pose estimation networks that accept a pair of images
  stacked along the channel dimension. In order to transfer the
  pretrained weights on ImageNet, the learned weights for the first
  convolutional kernel where tiled to match the increase in the input
  channels.
  """

  def __init__(self, batch_norm=False, name="vgg16", **kwargs):
    """Instantiate VGG16 model layers."""
    super().__init__(name=name, **kwargs)
    self.layers = [
        # Block 1
        Conv2DBatchNorm(
            64, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block1_conv1"),
        Conv2DBatchNorm(
            64, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block1_conv2"),
        keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block1_pool'),

        # Block 2
        Conv2DBatchNorm(
            128, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block2_conv1"),
        Conv2DBatchNorm(
            128, (3, 3),
            activation="relu",
            padding="same",
            batch_norm=batch_norm,
            name="block2_conv2"),
        keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block2_pool'),

        # Block 3
        Conv2DBatchNorm(
            256, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block3_conv1"),
        Conv2DBatchNorm(
            256, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block3_conv2"),
        Conv2DBatchNorm(
            256, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block3_conv3"),
        keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block3_pool'),

        # Block 4
        Conv2DBatchNorm(
            512, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block4_conv1"),
        Conv2DBatchNorm(
            512, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block4_conv2"),
        Conv2DBatchNorm(
            512, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block4_conv3"),
        keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block4_pool'),

        # Block 5
        Conv2DBatchNorm(
            512, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block5_conv1"),
        Conv2DBatchNorm(
            512, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block5_conv2"),
        Conv2DBatchNorm(
            512, (3, 3),
            activation="relu",
            batch_norm=batch_norm,
            padding="same",
            name="block5_conv3"),
        keras.layers.MaxPooling2D((2, 2), strides=(2, 2), name='block5_pool'),

        # Flatten
        keras.layers.Flatten(),
    ]

  def load_weights(self):
    """Load weights pre-trained on ImageNet."""
    # Download weights to local disk.
    weights_path = data_utils.get_file(
        'vgg16_weights_tf_dim_ordering_tf_kernels_notop.h5',
        WEIGHTS_PATH_NO_TOP,
        cache_subdir='models',
        file_hash='6d6bbae143d832006294945121d1f1fc')

    # Set model weights.
    with h5py.File(weights_path, 'r') as f:
      if 'layer_names' not in f.attrs and 'model_weights' in f:
        f = f['model_weights']

      # Load weights for all layers except the first convolutional layer.
      for i, layer in enumerate(self.layers):
        if layer.name in f and len(f[layer.name]) == 2:
          g = f[layer.name]
          w_k_name, w_b_name = g
          w_k = np.asarray(g[w_k_name])
          w_b = np.asarray(g[w_b_name])

          # Tile weights along input channel dimension for the first layer.
          if i == 0:
            w_k = np.tile(w_k, reps=[1, 1, 2, 1])
          conv_layer = layer.layers[0]
          conv_layer.set_weights([w_k, w_b])

  def compute_output_shape(self, input_shape):  # noqa: D102
    output_shape = input_shape
    for layer in self.layers:
      output_shape = layer.compute_output_shape(output_shape)
    return output_shape

  def call(self, inputs):  # noqa: D102
    output = inputs
    for layer in self.layers:
      output = layer(output)
    return output

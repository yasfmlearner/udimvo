# pylint: disable=arguments-differ
"""This module defines all discriminator networks used to build our model."""
import tensorflow.keras as keras


class DiscriminatorBlock(keras.layers.Layer):
  """Maps a vector to a real value through a series of dense layers."""

  def __init__(self,
               activation=None,
               sizes=None,
               name="discriminator",
               **kwargs):
    """Instantiate a basic linear discriminator layer.

    Args:
        activation: The activation function that should be used for the
            output of the last discriminator layer. All the intermediate
            layers default to ReLU activation.
        sizes: Optional list of integers with the sizes for each of the
            intermediate dense layers that encode the input vector up to
            the final dense layer that outputs a real value.
    """
    super(DiscriminatorBlock, self).__init__(name=name, **kwargs)
    self.disc_layers = [
        keras.layers.Dense(
            size, activation="relu", name=f"disc_linear{idx + 1}")
        for idx, size in enumerate(sizes)
    ]
    self.disc_output = keras.layers.Dense(
        1, activation=activation, name="disc_output")

  def call(self, inputs):  # noqa: D102
    x = inputs
    for layer in self.disc_layers:
      x = layer(x)
    return self.disc_output(x)

# pylint: disable=arguments-differ
"""This module defines Keras utility layers."""
import tensorflow.keras as keras
import tensorflow.keras.backend as K

from src.config.types import Rotation


def dense_layer(rot_type, name="pose_output"):
  """Return a Dense layer that outputs a pose for the given rotation type."""
  if rot_type == Rotation.quat:
    units = 7
  elif rot_type == Rotation.axis:
    units = 6
  else:
    raise ValueError(f"Unsupported rotation representation {rot_type}.")
  return keras.layers.Dense(units, name=name)


class Repeat(keras.layers.Layer):
  """Wrapper class for the repeat function."""

  def __init__(self, repetitions, axis=-1, name=None, **kwargs):
    """Initialize the layer with the number of repetitions."""
    super().__init__(name=name, **kwargs)
    self.axis = axis
    self.repetitions = repetitions

  def call(self, inputs):  # noqa: D102
    return K.repeat_elements(inputs, rep=self.repetitions, axis=self.axis)


class Conv2DBatchNorm(keras.layers.Layer):
  """Convolutional layer with optional batch norm applied before activation."""

  def __init__(self,
               filters,
               kernel_size,
               batch_norm=False,
               activation=None,
               padding="same",
               name=None,
               **kwargs):
    """Initialize the layers."""
    super().__init__(name=name, **kwargs)
    self.layers = [
        keras.layers.Conv2D(
            filters=filters, kernel_size=kernel_size, padding=padding)
    ]
    if batch_norm:
      self.layers.append(keras.layers.BatchNormalization(axis=-1))
    if activation:
      self.layers.append(keras.layers.Activation(activation))

  def compute_output_shape(self, input_shape):  # noqa: D102
    output_shape = input_shape
    for layer in self.layers:
      output_shape = layer.compute_output_shape(output_shape)
    return output_shape

  def call(self, inputs, training=False):  # noqa: D102
    outputs = inputs
    for layer in self.layers:
      outputs = layer(outputs, training=training)
    return outputs

# pylint: disable=arguments-differ
"""This module defines all encoder networks used to build our model."""
import tensorflow.keras as keras

import src.functions.lie as lie
import src.keras.layers.utils as utils


class EncoderBlock(keras.layers.Layer):
  """Maps a vector to a 7-D pose through a series of dense layers."""

  def __init__(self, rot_type, sizes, name="encoder", **kwargs):
    """Create an encoder with dense layers of dimensions `sizes`.

    Args:
        rot_type: A Rotation config type.
        sizes: Optional list of integers with the sizes for each of the
            intermediate dense layers that encode the input vector up to
            the final dense layer that outputs a 7-D pose.
    """
    super().__init__(name=name, **kwargs)
    self.rot_type = rot_type
    self.encoder_layers = [
        keras.layers.Dense(
            size, activation="relu", name=f"encoder_linear{idx + 1}")
        for idx, size in enumerate(sizes)
    ]

    self.encoder_output = utils.dense_layer(
        rot_type=self.rot_type, name="encoder_pose")

  def compute_output_shape(self, input_shape):  # noqa: D102
    output_shape = input_shape
    for layer in self.encoder_layers:
      output_shape = layer.compute_output_shape(output_shape)
    return self.encoder_output.compute_output_shape(output_shape)

  def call(self, inputs):  # noqa: D102
    x = inputs
    for layer in self.encoder_layers:
      x = layer(x)
    poses = self.encoder_output(x)
    normalized_poses = lie.normalize_pose(poses, self.rot_type)
    return normalized_poses

# pylint: disable=no-value-for-parameter,arguments-differ
"""This module defines all discriminator networks used to build our model."""
import os

import ipdb
import numpy as np
import tensorflow as tf
import tensorflow.keras as keras
import tensorflow.keras.backend as K
from tensorflow.keras.layers import Concatenate, Input, TimeDistributed
from tensorflow.python.keras import backend as K
from tensorflow.python.ops import summary_ops_v2
from tqdm import tqdm

import src.config.train as train_conf
from src.debug.error import NumericError
from src.functions import losses, regularizers, trajectory
from src.functions.metrics import MeanArrayList
from src.functions.utils import create_dir, fig_to_image
from src.keras.layers.cnn import VGG16
from src.keras.layers.discriminator import DiscriminatorBlock
from src.keras.layers.encoder import EncoderBlock
from src.keras.layers.rnn import LSTMBlock
from src.keras.layers.utils import Repeat


class UDIMVO:
  """Maps a vector to a real value through a series of dense layers."""

  @train_conf.ex.capture
  def __init__(self,
               dataset,
               ctc_contract,
               run_dir,
               ckpt_dir,
               log_dir,
               errors_dir,
               artifacts_dir,
               dataset_config,
               epochs,
               batch_norm,
               rot_type,
               infomax_loss_scale,
               ctc_loss_scale,
               weight_decay_scale,
               grad_reg_scale,
               gt_reg_scale,
               infomax_loss_name,
               disc_logits_activation,
               histogram_freq,
               debug,
               name="udimvo"):
    self.name = name
    self.dataset = dataset
    self.ctc_contract = ctc_contract
    self.debug = debug
    self.log_base_dir = os.path.join(run_dir, log_dir)
    self.ckpt_base_dir = os.path.join(run_dir, ckpt_dir)
    self.error_base_dir = os.path.join(run_dir, errors_dir)
    self.artifacts_base_dir = os.path.join(run_dir, artifacts_dir)
    create_dir(self.artifacts_base_dir)

    # Define summary writer
    self.writer = tf.summary.create_file_writer(self.log_base_dir)
    self.writer.set_as_default()

    # Save config
    self.c_epochs = epochs
    self.c_batch_norm = batch_norm
    self.c_rot_type = rot_type
    self.c_infomax_loss_scale = infomax_loss_scale
    self.c_ctc_loss_scale = ctc_loss_scale
    self.c_weight_decay_scale = weight_decay_scale
    self.c_grad_reg_scale = grad_reg_scale
    self.c_gt_reg_scale = gt_reg_scale
    self.c_infomax_loss_name = infomax_loss_name
    self.c_disc_logits_activation = disc_logits_activation
    self.c_histogram_freq = histogram_freq
    self.c_max_seq_length = dataset_config["max_seq_length"]
    self.c_image_shape = dataset_config["image_shape"]
    self.c_neg_samples = dataset_config["neg_samples"]
    self.c_steps_per_update = dataset_config["steps_per_update"]

    # Keep track of epochs and steps
    self.epoch = tf.Variable(0, trainable=False, dtype=tf.int64)
    self.step = tf.Variable(0, trainable=False, dtype=tf.int64)

    self.log_activations = {}
    self.train_model, self.eval_model = self._build_models()

    # Save model pngs to artifacts directory
    keras.utils.plot_model(
        self.train_model,
        to_file=os.path.join(self.artifacts_base_dir, "train_model.png"),
        show_shapes=True)
    keras.utils.plot_model(
        self.eval_model,
        to_file=os.path.join(self.artifacts_base_dir, "eval_model.png"),
        show_shapes=True)

    # Save model graph to tensorboard
    summary_ops_v2.graph(K.get_graph(), step=0)
    # summary_ops_v2.keras_model("keras", self.train_model, step=0)
    tf.summary.flush()

    self.optimizer = None
    self._batch_metrics = None
    self._epoch_metrics = None
    self.accum_grads = None
    self.ckpt = None
    self.manager = None
    self.numeric_error = None

  def _build_models(self):
    # Positive inputs
    height, width = self.c_image_shape
    pos_input = Input(
        shape=(height, width, 6), dtype=tf.float32, name="pos_batch")
    pos_input_td = Input(
        shape=(self.c_max_seq_length - 1, height, width, 6),
        dtype=tf.float32,
        name="pos_batch_td")

    # Create inputs and outputs for models
    train_inputs = [pos_input, pos_input_td]
    train_outputs = {}
    eval_inputs = [pos_input_td]

    # Regular layers
    cnn_net = VGG16(batch_norm=self.c_batch_norm)
    encoder_block = EncoderBlock(rot_type=self.c_rot_type, sizes=[1024])
    pos_cnn_output = cnn_net(pos_input)
    pos_encoder_output = encoder_block(pos_cnn_output)
    if self.debug:
      tf.debugging.check_numerics(
          pos_cnn_output, "Positive CNN output", name="pos_cnn_output")
      tf.debugging.check_numerics(
          pos_encoder_output,
          "Positive encoder output",
          name="pos_encoder_output")

    # Load pre-trained weights
    cnn_net.load_weights()

    # Time distributed layers
    cnn_net_td = TimeDistributed(cnn_net)
    encoder_block_td = TimeDistributed(encoder_block)
    pos_cnn_output_td = cnn_net_td(pos_input_td)
    pos_encoder_output_td = encoder_block_td(pos_cnn_output_td)
    if self.debug:
      tf.debugging.check_numerics(
          pos_cnn_output_td,
          "Positive time-distributed CNN output",
          name="pos_cnn_output_td")
      tf.debugging.check_numerics(
          pos_encoder_output_td,
          "Positive time-distributed encoder output",
          name="pos_encoder_output_td")
    eval_outputs = [
        tf.reshape(pos_encoder_output_td, (-1, pos_encoder_output_td.shape[-1]))
    ]

    self.log_activations.update({
        "pos_input": pos_input,
        "pos_input_td": pos_input_td,
        "pos_cnn_output": pos_cnn_output,
        "pos_encoder_output": pos_encoder_output,
        "pos_cnn_output_td": pos_cnn_output_td,
        "pos_encoder_output_td": pos_encoder_output_td
    })

    # Define infomax layers
    if self.c_infomax_loss_scale > 0:
      # Negative inputs
      neg_input = Input(
          shape=(height, width, 6), dtype=tf.float32, name="neg_batch")
      neg_input_td = Input(
          shape=(self.c_max_seq_length - 1, height, width, 6),
          dtype=tf.float32,
          name="neg_batch_td")
      train_inputs.extend([neg_input, neg_input_td])

      neg_cnn_output = cnn_net(neg_input)
      neg_cnn_output_td = cnn_net_td(neg_input_td)
      if self.debug:
        tf.debugging.check_numerics(
            neg_cnn_output, "Negative CNN output", name="neg_cnn_output")
        tf.debugging.check_numerics(
            neg_cnn_output_td,
            "Negative time-distributed CNN output",
            name="neg_cnn_output_td")
      concat_cnn_output = Concatenate(name="cnn_output", axis=0)
      cnn_output = concat_cnn_output([
          K.reshape(pos_cnn_output_td, (-1, K.shape(pos_cnn_output_td)[-1])),
          pos_cnn_output,
          K.reshape(neg_cnn_output_td, (-1, K.shape(neg_cnn_output_td)[-1])),
          neg_cnn_output,
      ])

      concat_pos_encoder_output = Concatenate(name="pos_encoder_output", axis=0)
      full_pos_encoder_output = concat_pos_encoder_output([
          K.reshape(pos_encoder_output_td,
                    (-1, K.shape(pos_encoder_output_td)[-1])),
          pos_encoder_output,
      ])

      repeat_encoder_output = Repeat(
          repetitions=self.c_neg_samples, name="neg_encoder_output", axis=0)
      full_neg_encoder_output = repeat_encoder_output(full_pos_encoder_output)

      concat_encoder_output = Concatenate(name="encoder_output", axis=0)
      encoder_output = concat_encoder_output(
          [full_pos_encoder_output, full_neg_encoder_output])

      # Discriminator layers
      concat_disc_input = Concatenate(name="disc_input", axis=1)
      disc_input = concat_disc_input([cnn_output, encoder_output])
      discriminator_block = DiscriminatorBlock(
          activation=self.c_disc_logits_activation, sizes=[512, 512])
      disc_logits = discriminator_block(disc_input)
      if self.debug:
        tf.debugging.check_numerics(
            disc_logits, "Discriminator logits", name="disc_logits")
      train_outputs["disc_logits"] = disc_logits
      self.log_activations.update({
          "neg_input": neg_input,
          "neg_input_td": neg_input_td,
          "neg_cnn_output": neg_cnn_output,
          "neg_cnn_output_td": neg_cnn_output_td,
          "disc_input": disc_input,
          "disc_logits": disc_logits
      })

    # Define CTC net layers
    if self.c_ctc_loss_scale > 0:
      input_shape = tf.shape(pos_encoder_output_td)
      rnn_block = LSTMBlock(
          rot_type=self.c_rot_type, sizes=[1024], input_shape=input_shape)
      pos_rnn_output = rnn_block(pos_encoder_output_td)
      if self.debug:
        tf.debugging.check_numerics(
            pos_rnn_output, "Positive RNN output", name="pos_rnn_output")
      train_outputs["pos_encoder_output"] = pos_encoder_output
      train_outputs["pos_rnn_output"] = pos_rnn_output
      eval_outputs = [pos_rnn_output]
      self.log_activations["pos_rnn_output"] = pos_rnn_output

    # Define models
    train_model = keras.Model(
        inputs=train_inputs, outputs=train_outputs, name=f"train_{self.name}")
    eval_model = keras.Model(
        inputs=eval_inputs, outputs=eval_outputs, name=f"eval_{self.name}")
    return train_model, eval_model

  def fit(self, dataset, validation_dataset):
    """Train the model with the given training and validation dataset."""
    latest_ckpt = self.manager.latest_checkpoint
    status = self.ckpt.restore(latest_ckpt)
    if latest_ckpt and status.assert_existing_objects_matched():
      print(f"Restored from {latest_ckpt}")
    else:
      print("Initializing from scratch.")

    for epoch in range(self.epoch.numpy(), self.c_epochs):
      epoch_prog = f"{epoch + 1}/{self.c_epochs}"

      for data in tqdm(dataset, desc=f"Epoch {epoch_prog}"):
        try:
          self.train_step(data)
        except tf.errors.InvalidArgumentError as e:
          print(f"Encountered error in train step {self.step.numpy()}.")
          print(e.message)
          self._log_weights(step=self.step)
          self._log_activations(data, step=self.step)
          self.numeric_error.save(e, data)
          return

        if self.step % self.c_steps_per_update == 0:
          self._log_metrics(self._batch_metrics, step=self.step)

      for val_data in tqdm(validation_dataset, desc="Val Trajs"):
        self.test_step(val_data)

      self._log_metrics(self._epoch_metrics, step=self.epoch)

      self.epoch.assign_add(1)

      save_path = self.manager.save(checkpoint_number=self.epoch)
      print(f"Saved checkpoint for epoch {self.epoch.numpy()}: {save_path}")

      dataset.on_epoch_end()
      validation_dataset.on_epoch_end()

  def compile(self, optimizer, batch_metrics, epoch_metrics):
    """Initialize the optimizer and metrics for the network."""
    self.optimizer = optimizer
    self._batch_metrics = batch_metrics
    self._epoch_metrics = epoch_metrics

    # Use the metric interface to accumulate gradients
    self.accum_grads = MeanArrayList(
        len(self.train_model.trainable_variables), name="train.grads")

    # Create checkpoint and manager.
    self.ckpt = tf.train.Checkpoint(
        epoch=self.epoch,
        step=self.step,
        optimizer=self.optimizer,
        train_model=self.train_model,
        eval_model=self.eval_model)
    self.manager = tf.train.CheckpointManager(
        self.ckpt, self.ckpt_base_dir, max_to_keep=10)
    if self.debug:
      self.numeric_error = NumericError(self.ckpt, self.error_base_dir)

  def resume_error(self):
    """Resume the latest error information and set trace for debugging."""
    data, msg = self.numeric_error.load()
    ipdb.set_trace()

  @tf.function
  def train_step(self, data):
    with tf.GradientTape(persistent=True) as tape:
      if self.c_grad_reg_scale > 0:
        tape.watch(data[0])

      # Forward pass
      outputs = self.train_model(data[0], training=True)

      # Compute loss
      loss = self.compute_loss(**outputs)
      if self.debug:
        tf.debugging.check_numerics(
            loss, "Checking total loss", name="total_loss")

      # Compute regularization
      if self.c_gt_reg_scale > 0:
        # TODO: Make sure this generalizes to when infomax_loss_scale > 0
        pos_rnn_output = outputs["pos_rnn_output"]
        pos_encoder_output = outputs["pos_encoder_output"]
        ys_pred = tf.concat([pos_encoder_output, pos_rnn_output], axis=0)
        ys_pos, ys_pos_td = data[1][0], data[1][1]
        ys_pos_td = tf.reshape(ys_pos_td, shape=(-1, tf.shape(ys_pos_td)[-1]))
        ys_true = tf.concat([ys_pos, ys_pos_td], axis=0)
        gt_reg = self.c_gt_reg_scale * tf.nn.l2_loss(ys_pred - ys_true)
        self._update_metric("gt_reg", gt_reg)
        loss += gt_reg
      if self.c_weight_decay_scale > 0:
        weight_decay = regularizers.weight_decay(
            self.train_model.trainable_variables)
        if self.debug:
          tf.debugging.check_numerics(
              weight_decay, "Checking weight decay", name="weight_decay")
        weight_decay = self.c_weight_decay_scale * weight_decay
        self._update_metric("wd", weight_decay)
        loss += weight_decay
      if self.c_grad_reg_scale > 0:
        input_grads = tape.gradient(loss, data[0])
        grad_reg = regularizers.gradient_regularization(input_grads)
        if self.debug:
          for i, input_grad in enumerate(input_grads):
            tf.debugging.check_numerics(
                input_grad, "Checking input gradients", name=f"input_grads_{i}")
          tf.debugging.check_numerics(
              grad_reg, "Checking gradient regularization", name="grad_reg")
        grad_reg = self.c_grad_reg_scale * input_grads
        self._update_metric("grad_reg", grad_reg)
        loss += grad_reg

      # Update both loss metrics at once. The difference is that we
      # reset the batch loss before every call to train_step.
      self._update_metric("loss", loss)

    # Compute gradients with respect to weights for current batch.
    weight_grads = tape.gradient(loss, self.train_model.trainable_variables)
    if self.debug:
      for i, weight_grad in enumerate(weight_grads):
        tf.debugging.check_numerics(
            weight_grad, "Weight gradients", name=f"weight_grads_{i}")
    self.accum_grads(weight_grads)
    self.step.assign_add(1)

    if self.debug and self.step % self.c_histogram_freq == 0:
      self._log_weights(step=self.step)
      self._log_gradients(weight_grads, step=self.step)
      self._log_activations(data[0], step=self.step)

    # Update the model gradients after accumulating for `steps_per_update`
    # steps.
    if self.step % self.c_steps_per_update == 0:
      self.optimizer.apply_gradients(
          zip(self.accum_grads.result(), self.train_model.trainable_variables))
      self.accum_grads.reset_states()

  @tf.function
  def compute_loss(self,
                   disc_logits=None,
                   pos_encoder_output=None,
                   pos_rnn_output=None):
    loss = tf.constant(0, dtype=tf.float32)
    if self.c_infomax_loss_scale > 0:
      infomax_loss = losses.infomax_loss(disc_logits, self.c_neg_samples,
                                         self.c_infomax_loss_name)
      if self.debug:
        tf.debugging.check_numerics(
            infomax_loss, "Infomax loss", name="infomax_loss")
      infomax_loss = self.c_infomax_loss_scale * infomax_loss
      self._update_metric("mi", infomax_loss)
      loss += infomax_loss
    if self.c_ctc_loss_scale > 0:
      ctc_loss = tf.constant(0, dtype=tf.float32)
      for direct_idx, start_idx, stop_idx, in self.ctc_contract:
        comp_poses = pos_rnn_output[start_idx:stop_idx]
        direct_pose = pos_encoder_output[direct_idx]
        ctc_loss += losses.ctc_loss(comp_poses, direct_pose, self.c_rot_type)
      if self.debug:
        tf.debugging.check_numerics(ctc_loss, "CTC loss", name="ctc_loss")
      ctc_loss = self.c_ctc_loss_scale * ctc_loss
      self._update_metric("ctc_loss", ctc_loss)
      loss += ctc_loss
    return loss

  def test_step(self, data):
    """Computes a forward pass on an entire trajectory.

    The validation loop computes the predicted relative poses for each
    sequential frame pair in each trajectory. After doing so, we compose
    the relative poses to create a sequence of absolute poses, that can
    be used to build a new trajectory. With the absolute poses of the
    predicted and ground-truth trajectories, we can compute the
    evaluation metrics. After doing so, both trajectories are plotted
    against each other and saved as artifacts.

    Args:
        data: The trajectory id with which to execute the forward pass.
    """
    val_sequence = data
    y_trues, y_preds = [], []
    for x_val_batch, y_val_batch in tqdm(val_sequence, desc="Val Seqs"):
      y_pred = self.eval_model(x_val_batch, training=False)
      y_trues.append(y_val_batch)
      y_preds.append(y_pred)
    y_true = np.concatenate(y_trues, axis=0)
    y_pred = np.concatenate(y_preds, axis=0)
    self._log_pose("val_pred", y_pred, step=self.step)
    self._log_pose("val_true", y_true, step=self.step)
    traj = trajectory.TrajectoryEvalPair(
        y_true, y_pred, self.c_rot_type, f"{val_sequence.traj_id}", rel=True)
    traj.plot()
    self._log_figs(traj.figs, step=self.epoch)
    traj.close_figs()
    self._update_metric("val_ate", traj.ate, len(traj))
    self._update_metric("val_rpe_rot", traj.rpe_rot, len(traj))
    self._update_metric("val_rpe_trans", traj.rpe_trans, len(traj))

  def _update_metric(self, name, value, sample_weight=None):
    """Update both batch and epoch level metrics with the new value."""
    if name in self._epoch_metrics:
      self._epoch_metrics[name](value, sample_weight)
    if name in self._batch_metrics:
      self._batch_metrics[name](value, sample_weight)

  def _log_figs(self, figs, step=None):
    for name, fig in figs.items():
      tf.summary.image(name, fig_to_image(fig), step=step)

  def _log_scalars(self, metrics, step=None):
    for m in metrics:
      tf.summary.scalar(name=m.name, data=m.result(), step=step)
      m.reset_states()

  def _log_metrics(self, metrics, step=None):
    """Log metrics to summary writer and reset states.

    Args:
      metrics: Dictionary of metrics we wish to log.
      step: The step value used to log the metrics.
    """
    metrics = [m for k, m in metrics.items()]
    if metrics:
      self._log_scalars(metrics, step=step)

  def _log_pose(self, name, pose, step=None):
    """Log the ground truth and predicted pose distributions."""
    pose_strs = ["pos_x", "pos_y", "pos_z", "ori_x", "ori_y", "ori_z", "ori_w"]
    for i in range(pose.shape[-1]):
      tf.summary.histogram(
          f"poses/{pose_strs[i]}/{name}", pose[:, i], buckets=15, step=step)
    self.writer.flush()

  def _log_weights(self, step=None):
    """Log the weights of the Model to TensorBoard."""
    for layer in self.train_model.layers:
      for weight in layer.weights:
        weight_name = f"weights/{weight.name.replace(':', '_')}"
        tf.summary.histogram(weight_name, weight, buckets=15, step=step)
    self.writer.flush()

  def _log_gradients(self, grads, step=None):
    """Log given gradients as histograms."""
    for i, grad in enumerate(grads):
      weight_name = self.train_model.weights[i].name.replace(':', '_')
      grad_name = f"grads/{weight_name}"
      tf.summary.histogram(grad_name, grad, buckets=15, step=step)
    self.writer.flush()

  def _log_activations(self, data, step=None):
    """Log activations for given input data as histograms."""
    act_names, act_tensors = [], []
    for name, tensor in self.log_activations.items():
      act_names.append(name)
      act_tensors.append(tensor)
    inputs = self.train_model.input
    outputs = [act_tensor for act_tensor in act_tensors]
    keras_func = K.function(inputs, outputs)
    activations = keras_func(data)
    for name, act in zip(act_names, activations):
      act_name = f"activations/{name}"
      tf.summary.histogram(act_name, act, buckets=15, step=step)
    self.writer.flush()

  def _log_weight_as_image(self, weight, weight_name, step=None):
    """Log a weight as a TensorBoard image."""
    w_img = tf.squeeze(weight)
    shape = K.int_shape(w_img)
    if len(shape) == 1:  # Bias case
      w_img = tf.reshape(w_img, [1, shape[0], 1, 1])
    elif len(shape) == 2:  # Dense layer kernel case
      if shape[0] > shape[1]:
        w_img = tf.transpose(w_img)
        shape = K.int_shape(w_img)
      w_img = tf.reshape(w_img, [1, shape[0], shape[1], 1])
    elif len(shape) == 3:  # ConvNet case
      if K.image_data_format() == 'channels_last':
        # Switch to channels_first to display every kernel as a separate
        # image.
        w_img = tf.transpose(w_img, perm=[2, 0, 1])
        shape = K.int_shape(w_img)
      w_img = tf.reshape(w_img, [shape[0], shape[1], shape[2], 1])

    shape = K.int_shape(w_img)
    # Not possible to handle 3D convnets etc.
    if len(shape) == 4 and shape[-1] in [1, 3, 4]:
      tf.summary.image(weight_name, w_img, step=step)

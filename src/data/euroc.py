"""Module used to process the EuRoC MAV dataset."""
import csv
import glob
import logging
import os
import re

import cv2
import numpy as np
from evo.tools import file_interface

from src.data.dataset import (Dataset, DatasetProperty, DatasetErrorCode,
                              TrajectorySample)
from src.data.util import redefine_trajectory

TRAIN_SPLIT = {
    "MH_01_easy", "MH_03_medium", "MH_05_difficult", "V1_02_medium",
    "V2_01_easy", "V2_03_difficult"
}
TEST_SPLIT = {
    "MH_02_easy", "MH_04_difficult", "V1_03_difficult", "V2_02_medium",
    "V1_01_easy"
}


class EUROC(Dataset):
  """Class to interact with the EuRoC MAV dataset."""

  def __init__(self, dataset_dir):
    """Initialize the EuRoC MAV dataset directories."""
    super().__init__(dataset_dir)
    if not self.isvalid(dataset_dir):
      logging.error(
          DatasetErrorCode.INVALID_DIR_STRUCTURE.value,
          "Invalid sequence directory names for EuRoC dataset. "
          "Expected format <NAME>_<NUM:02d>_<DIFFICULTY>.")
    self.seq_dirs = glob.glob(os.path.join(dataset_dir, "*"))

  @staticmethod
  def isvalid(dataset_dir):
    """Whether a valid EUROC dataset exists in the given directory."""
    seq_dirs = glob.glob(os.path.join(dataset_dir, "*"))
    if len(seq_dirs) == 0:
      return False
    euroc_pattern = re.compile(
        "^((MH)|(V[12]))_0[1-5]_(easy|medium|difficult)$")
    for seq_dir in seq_dirs:
      if euroc_pattern.match(os.path.basename(seq_dir)) is None:
        return False
    return True

  @property
  def properties(self):
    """Return the DatasetProperties that this dataset provides."""
    return {
        DatasetProperty.TIME, DatasetProperty.POSE, DatasetProperty.LCAM,
        DatasetProperty.RCAM
    }

  def get_train_traj_ids(self):
    """Return a list o trajectory IDs for the EuRoC training split."""
    train_ids = []
    for seq_dir in self.seq_dirs:
      seq_name = os.path.basename(seq_dir)
      if seq_name in TRAIN_SPLIT:
        train_ids.append(seq_name)
    return sorted(train_ids)

  def get_test_traj_ids(self):
    """Return a list o trajectory IDs for the EuRoC test split."""
    test_ids = []
    for seq_dir in self.seq_dirs:
      seq_name = os.path.basename(seq_dir)
      if seq_name in TEST_SPLIT:
        test_ids.append(seq_name)
    return sorted(test_ids)

  def get_traj_sample(self,
                      traj_id,
                      props,
                      sample_length=-1,
                      random_sample=False):
    """Return a generator with a sequence of the specified arguments.

    Args:
        traj_id: A string used to uniquely reference the sequence in the
            dataset. For the EUROC dataset, this the name of the sequence,
            such as 'MH_01_easy'.
        props: Set of DatasetProperty enums indicating which properties we
            wish to retrieve from the sequence. This dataset provides:
                1. TIME
                2. POSE
                3. LCAM
                4. RCAM
        traj_length: An integer specifying the length of the trajectory we
            wish to retrieve. If this number is greater than the
            trajectory's length, a warning is issued.
        random: A boolean indicating if we wish the trajectory of size
            traj_length to be sampled randomly from the original
            trajectory. If false, the frames are taken from in order from
            the start of the trajectory.
    Returns:
        A TrajectorySample object populated with the specified dataset
        properties.
    """
    if DatasetProperty.DEPTH in props:
      logging.error(DatasetErrorCode.INVALID_DATASET_PROP.value,
                    self.__class__.__name__, DatasetProperty.DEPTH)

    lcam_dir = os.path.join(self.dataset_dir, traj_id, "mav0", "cam0", "data")
    rcam_dir = os.path.join(self.dataset_dir, traj_id, "mav0", "cam1", "data")

    lcam_csv = os.path.join(self.dataset_dir, traj_id, "mav0", "cam0",
                            "data.csv")
    lcam_filenames = list(self._read_csv(lcam_csv, ["ts", "fn"]))
    traj_length = len(lcam_filenames)

    start_index, traj_length = redefine_trajectory(traj_length, sample_length,
                                                   random_sample)
    keep_indices = range(start_index, start_index + traj_length)

    dataset_props = {}
    if DatasetProperty.TIME in props:
      timestamps = np.array(
          [int(lcam_filenames[i]["ts"]) for i in keep_indices])
      timestamps = (timestamps - timestamps[0]) * 1e-6
      dataset_props[DatasetProperty.TIME] = timestamps
    if DatasetProperty.POSE in props:
      pose_fn = os.path.join(self.dataset_dir, traj_id, "mav0",
                             "state_groundtruth_estimate0", "data.csv")
      pose_path = file_interface.read_euroc_csv_trajectory(pose_fn)
      pose_path.reduce_to_ids(keep_indices)
      dataset_props[DatasetProperty.POSE] = pose_path
    if DatasetProperty.LCAM in props:
      dataset_props[DatasetProperty.LCAM] = \
          lambda index: cv2.imread(
              os.path.join(lcam_dir,
                           lcam_filenames[start_index + index]["fn"]))
    if DatasetProperty.RCAM in props:
      rcam_csv = os.path.join(self.dataset_dir, traj_id, "mav0", "cam1",
                              "data.csv")
      rcam_filenames = list(self._read_csv(rcam_csv, ["ts", "fn"]))
      dataset_props[DatasetProperty.RCAM] = \
          lambda index: cv2.imread(
              os.path.join(rcam_dir,
                           rcam_filenames[start_index + index]["fn"]))
    return TrajectorySample(
        traj_length=traj_length,
        poses=dataset_props.get(DatasetProperty.POSE, None),
        timestamps=dataset_props.get(DatasetProperty.TIME, None),
        lcam_fn=dataset_props.get(DatasetProperty.LCAM, None),
        rcam_fn=dataset_props.get(DatasetProperty.RCAM, None),
        depth_fn=None)

  def _read_csv(self, csv_filename, fieldnames):
    """Read the csv file and return a DictReader."""
    csv_file = open(csv_filename, "r", newline="")
    csv_reader = csv.DictReader(csv_file, fieldnames=fieldnames)
    next(csv_reader)
    return csv_reader

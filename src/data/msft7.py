"""Module used to process Microsoft's 7-Scenes dataset."""
import glob
import logging
import os

import cv2
import numpy as np
from evo.core import trajectory

from src.data.dataset import (Dataset, DatasetErrorCode, DatasetProperty,
                              TrajectorySample)
from src.data.util import redefine_trajectory


class MSFT7(Dataset):
  """Class to interact with Microsoft's 7-Scenes dataset."""

  def __init__(self, dataset_dir):
    """Initialize Microsoft's 7-Scenes dataset directories."""
    super().__init__(dataset_dir)
    if not self.isvalid(dataset_dir):
      logging.error(
          DatasetErrorCode.INVALID_DIR_STRUCTURE.value,
          "Either there are no scene directories or one "
          "of the scene names is invalid. Expected one of "
          "['chess', 'fire', 'heads', 'office', 'pumpkin', "
          "'redkitchen', 'stairs']")
    self.scene_dirs = glob.glob(os.path.join(dataset_dir, "*"))

  @staticmethod
  def isvalid(dataset_dir):
    """Whether a valid MSFT7 dataset exists in the given directory."""
    scene_dirs = glob.glob(os.path.join(dataset_dir, "*"))
    if len(scene_dirs) == 0:
      return False
    for scene_dir in scene_dirs:
      scene_name = os.path.basename(scene_dir)
      if scene_name not in ("chess", "fire", "heads", "office", "pumpkin",
                            "redkitchen", "stairs"):
        return False
    return True

  @property
  def properties(self):
    """Return the DatasetProperties that this dataset provides."""
    return {DatasetProperty.POSE, DatasetProperty.LCAM, DatasetProperty.DEPTH}

  def get_train_traj_ids(self):
    """Return a list o trajectory IDs for the 7-Scenes training split."""
    train_ids = []
    for scene_dir in self.scene_dirs:
      split_fn = os.path.join(scene_dir, "TrainSplit.txt")
      with open(split_fn, "r") as f:
        seq_nrs = [int(line.split("sequence")[-1]) for line in f.readlines()]
        for seq_nr in seq_nrs:
          train_ids.append(os.path.join(scene_dir, f"seq-{seq_nr:02d}"))
    return sorted(train_ids)

  def get_test_traj_ids(self):
    """Return a list o trajectory IDs for the 7-Scenes training split."""
    test_ids = []
    for scene_dir in self.scene_dirs:
      split_fn = os.path.join(scene_dir, "TestSplit.txt")
      with open(split_fn, "r") as f:
        seq_nrs = [int(line.split("sequence")[-1]) for line in f.readlines()]
        for seq_nr in seq_nrs:
          test_ids.append(os.path.join(scene_dir, f"seq-{seq_nr:02d}"))
    return sorted(test_ids)

  def get_traj_sample(self,
                      traj_id,
                      props,
                      sample_length=-1,
                      random_sample=False):
    """Return a generator with a sequence of the specified arguments.

    Args:
        traj_id: A string used to uniquely reference the sequence in the
            dataset. For the 7-Scenes dataset, this is the name of the
            directories that reference the sequence, such as
            'chess/seq-01'.
        props: Set of DatasetProperty enums indicating which properties we
            wish to retrieve from the sequence. This dataset provides:
                1. POSE
                2. LCAM
                3. DEPTH
        traj_length: An integer specifying the length of the trajectory we
            wish to retrieve. If this number is greater than the
            trajectory's length, a warning is issued.
        random: A boolean indicating if we wish the trajectory of size
            traj_length to be sampled randomly from the original
            trajectory. If false, the frames are taken from in order from
            the start of the trajectory.
    Returns:
        A TrajectorySample object populated with the specified dataset
        properties.
    """
    if DatasetProperty.TIME in props or DatasetProperty.RCAM in props:
      logging.error(DatasetErrorCode.INVALID_DATASET_PROP.value,
                    self.__class__.__name__,
                    f"{DatasetProperty.TIME} or {DatasetProperty.RCAM}")
    traj_length = \
        len(os.listdir(traj_id)) // 3

    start_index, traj_length = redefine_trajectory(traj_length, sample_length,
                                                   random_sample)
    keep_indices = range(start_index, start_index + traj_length)

    dataset_props = {}
    if DatasetProperty.POSE in props:
      poses_se3 = []
      for index in keep_indices:
        pose_fn = os.path.join(traj_id, f"frame-{index:06d}.pose.txt")
        pose_iter = open(pose_fn, "r").read().split()
        poses_se3.append(
            np.fromiter(pose_iter, dtype=np.float64).reshape((4, 4)))
      dataset_props[DatasetProperty.POSE] = \
          trajectory.PosePath3D(poses_se3=poses_se3)
    if DatasetProperty.LCAM in props:
      dataset_props[DatasetProperty.LCAM] = \
          lambda index: cv2.imread(os.path.join(
              traj_id, f"frame-{index:06d}.color.png"))
    if DatasetProperty.DEPTH in props:
      dataset_props[DatasetProperty.DEPTH] = \
          lambda index: cv2.imread(os.path.join(
              traj_id, f"frame-{index:06d}.depth.png"))
    return TrajectorySample(
        traj_length=traj_length,
        poses=dataset_props.get(DatasetProperty.POSE, None),
        timestamps=None,
        lcam_fn=dataset_props.get(DatasetProperty.LCAM, None),
        rcam_fn=None,
        depth_fn=dataset_props.get(DatasetProperty.DEPTH, None))

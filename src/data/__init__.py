"""Define a helper function to retrieve a dataset object."""
import enum
import os

import src.config.data as data_conf
import src.data.euroc as euroc
import src.data.kitti as kitti
import src.data.msft7 as msft7


class DatasetName(enum.Enum):
  """Enumeration for the supported datasets."""

  KITTI = "kitti"
  EUROC = "euroc"
  MSFT7 = "msft7"


@data_conf.data_ingredient.capture
def get_dataset(dataset_name, datasets_dir):
  """Retrieve a dataset object for the dataset with the given name.

  Args:
    dataset_name: A DatasetName enum value. One of {'kitti', 'euroc', 'msft7'}.
    dataset_base_dir: The base directory to look for the datasets.

  Returns:
    A Dataset object.

  Raises:
    ValueError: If no dataset with dataset_name is implemented.
  """
  dataset_dir = os.path.join(datasets_dir, dataset_name)
  if DatasetName(dataset_name) is DatasetName.KITTI:
    return kitti.KITTI(dataset_dir)
  if DatasetName(dataset_name) is DatasetName.EUROC:
    return euroc.EUROC(dataset_dir)
  if DatasetName(dataset_name) is DatasetName.MSFT7:
    return msft7.MSFT7(dataset_dir)
  raise ValueError(f"Dataset {dataset_name} isn't supported.")

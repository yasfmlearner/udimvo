"""Generic dataset processing classes."""
import abc
import enum
import logging
import math

import cv2
import numpy as np
import tensorflow.keras as keras

import src.data.util as util
from src.config.data import data_ingredient
from src.config.types import Rotation


class DatasetProperty(enum.Enum):
  """Enumeration for the properties that can be retrieved from a dataset."""

  LCAM = 1  # Image from the left camera.
  RCAM = 2  # Image from the right camera.
  POSE = 3  # An evo.PosePath3D object.
  TIME = 4  # The time that the frame was captured in milliseconds.
  DEPTH = 5  # Pixel-wise depth information for the scene.


class DatasetErrorCode(enum.Enum):
  """Dataset error codes and messages."""

  INVALID_DIR_STRUCTURE = ("Wrong directory structure for the specified "
                           "dataset directory. %s")
  INVALID_DATASET_PROP = ("The %s dataset doesn't provide property %s.")
  TRAJ_PROP_UNAVAILABLE = ("The dataset property %s isn't available for "
                           "this trajectory.")


class Dataset(abc.ABC):
  """Common interface for all datasets."""

  @abc.abstractmethod
  def __init__(self, dataset_dir):
    """Initialize the dataset object."""
    self.dataset_dir = dataset_dir

  @staticmethod
  @abc.abstractmethod
  def isvalid(dataset_dir):
    """Whether the dataset has a valid directory structure."""

  @property
  @abc.abstractmethod
  def properties(self):
    """Return the DatasetProperties that this dataset provides."""

  @abc.abstractmethod
  def get_train_traj_ids(self):
    """Get the trajectory IDs for the training split."""

  @abc.abstractmethod
  def get_test_traj_ids(self):
    """Get the trajectory IDs for the testing split."""

  def get_traj_ids(self):
    """Get all trajectory IDs for the dataset."""
    return sorted(self.get_train_traj_ids() + self.get_test_traj_ids())

  @abc.abstractmethod
  def get_traj_sample(self,
                      traj_id,
                      props,
                      sample_length=-1,
                      random_sample=False):
    """Return a generator with a sequence of the specified arguments.

    Args:
        traj_id: A string used to uniquely reference the sequence in the
            dataset.
        props: Set of DatasetProperty enums indicating which properties we
            wish to retrieve from the sequence.
        traj_length: An integer specifying the length of the trajectory we
            wish to retrieve. If this number is greater than the
            trajectory's length, a warning is issued.
        random: A boolean indicating if we wish the trajectory of size
            traj_length to be sampled randomly from the original
            trajectory. If false, the frames are taken from in order from
            the start of the trajectory.
    Returns:
        A TrajectorySample object populated with the specified dataset
        properties.
    """

  def play_sequence(self, traj_id, props):
    """Plays the frame sequence with traj_id as a video."""
    props.add(DatasetProperty.LCAM)
    trajectory = self.get_traj_sample(traj_id, props)

    lcam_gen = trajectory.lcam
    if DatasetProperty.RCAM in props:
      rcam_gen = trajectory.rcam

    for index in range(len(trajectory)):
      limg = next(lcam_gen)
      if DatasetProperty.RCAM in props:
        rimg = next(rcam_gen)
        assert limg.shape == rimg.shape
        # Stack images horizontally
        height, width, chan = limg.shape
        new_img = np.empty((height, width * 2, chan), np.uint8)
        new_img[:, :width, :] = limg
        new_img[:, width:, :] = rimg
      else:
        new_img = limg

      # Write elapsed time to frame
      if DatasetProperty.TIME in props:
        time_sec = trajectory.timestamps[index] / 1000
        time_str = f"t={time_sec:.2f}s"

        cv2.putText(
            new_img,
            text=time_str,
            org=(10, 30),
            fontFace=cv2.FONT_HERSHEY_SIMPLEX,
            fontScale=1,
            color=(0, 255, 0),  # Green
            thickness=2)
      cv2.imshow(traj_id, new_img)
      key_code = cv2.waitKey(10)
      if key_code != -1:
        if key_code == 27:  # Press 'esc' to exit.
          cv2.destroyWindow(f"{traj_id}")
          raise KeyboardInterrupt
        if chr(key_code) == "n":  # Press 'n' to jump to next video.
          break
        if chr(key_code) == "p":  # Press 'p' to pause video.
          res_code = cv2.waitKey(0)
          while chr(res_code) != " ":
            res_code = cv2.waitKey(0)  # Press 'space' to continue.
    cv2.destroyWindow(f"{traj_id}")


class ImageIterator:

  def __init__(self, read_img_fn, size):
    self._read_img_fn = read_img_fn
    self._index = 0
    self._size = size

  def __iter__(self):
    return self

  def __next__(self):
    if self._index < self._size:
      img = self[self._index]
      self._index += 1
      return img
    raise StopIteration

  def __getitem__(self, index):
    return self._read_img_fn(index)

  def reset(self):
    self._index = 0


class TrajectorySample:
  """A trajectory sampled from a Dataset.

  Attributes:
      poses: A evo.PosePath3D object with the poses for the trajectory.
      timestamps: The timestamps for the trajectory.
      lcam: A generator with the left camera frames of the trajectory.
      rcam: A generator with the right camera frames of the trajectory.
      depth: A generator with the depth frames of the trajectory.
  """

  def __init__(self,
               traj_length,
               poses=None,
               timestamps=None,
               lcam_fn=None,
               rcam_fn=None,
               depth_fn=None):
    """Initialize the trajectory sample object with optional properties."""
    self._traj_length = traj_length
    self._poses = poses
    self._timestamps = timestamps
    self._lcam = None
    if lcam_fn is not None:
      self._lcam = ImageIterator(lcam_fn, traj_length)
    self._rcam = None
    if rcam_fn is not None:
      self._rcam = ImageIterator(rcam_fn, traj_length)
    self._depth = None
    if depth_fn is not None:
      self._depth = ImageIterator(depth_fn, traj_length)

  def __len__(self):
    return self._traj_length

  @property
  def poses(self):
    if self._poses is None:
      logging.error(DatasetErrorCode.TRAJ_PROP_UNAVAILABLE.value,
                    DatasetProperty.POSE)
    return self._poses

  @property
  def timestamps(self):
    if self._timestamps is None:
      logging.error(DatasetErrorCode.TRAJ_PROP_UNAVAILABLE.value,
                    DatasetProperty.TIME)
    return self._timestamps

  @property
  def lcam(self):
    if self._lcam is None:
      logging.error(DatasetErrorCode.TRAJ_PROP_UNAVAILABLE.value,
                    DatasetProperty.LCAM)
    return self._lcam

  @property
  def rcam(self):
    if self._rcam is None:
      logging.error(DatasetErrorCode.TRAJ_PROP_UNAVAILABLE.value,
                    DatasetProperty.RCAM)
    return self._rcam

  @property
  def depth(self):
    if self._depth is None:
      logging.error(DatasetErrorCode.TRAJ_PROP_UNAVAILABLE.value,
                    DatasetProperty.DEPTH)
    return self._depth


class SequenceType(enum.Enum):
  """Enumeration for the different types of sequence generators."""

  TRAIN = "train"
  VAL = "val"
  TEST = "test"


class TrainSequence(keras.utils.Sequence):
  """Sequence generator to iterate over the training samples of a Dataset.

  Attributes:
      batch_size: The effective batch size of the dataset sequence.
  """

  @data_ingredient.capture
  def __init__(self,
               dataset,
               traj_ids,
               train_nr_seqs,
               neg_samples,
               pos_warp_samples,
               image_shape,
               max_seq_length,
               min_ctc_length=-1,
               random_crop=False,
               rot_type=Rotation.quat,
               gt_reg_scale=0,
               val_split=0.0,
               preproc=True,
               shuffle=True,
               _rnd=None):
    """Create an instance of the dataset sequence generator.

    Args:
      dataset: A Dataset object which can be used to retrieve any
          trajectory belonging to the dataset.
      traj_ids: The trajectory ids we wish to create the sequence with.
      train_nr_seqs: The number of sequences of max_seq_length to sample
          for every train batch.
      neg_samples: The number of negative samples generated for every
          positive one.
      pos_warp_samples: The number of positive samples to generate by
          applying a warp transformation to both the source and target
          images.
      image_shape: The target shape that we need to transform the images
          to in order to feed the network.
      max_seq_length: The maximum number of consecutive frames from where
          we can sample a pair of frames from. If max_seq_length=10, this
          means that the largest distance between a pair of frames is 10
          and pairing up frames 1 and 11 of the sequence would be
          invalid.
      min_ctc_length: The minimum number of frames between a CTC block.
          A CTC block is generated for each of the of the possible frame
          sequences of length greater than or equal to min_ctc_length up
          to the maximum possible length specified by max_seq_length. The
          minimum value for this parameter to be meaningful is 3, since
          a ctc_length of 2 doesn't make sense, considering we can't
          apply transformations to create a meaningful constraint for the
          network to learn.
      rot_type: The rotation type used to generate the ground-truth poses.
          One of {'quaternion', 'axis'}.
      gt_reg_scale: The scale of the ground truth regularization term. If
        positive, the ground truth poses for each image pair are returned.
      random_crop: If true, random patches of the image are cropped
          before resizing it to the 'target_shape'. If false, a center
          crop of the image is taken.
      val_split: The percentage of the training trajectories that should
          be reserved for validation.
      preproc: If true, the preprocessing step for the pretrained
          network being used is applied to the image. This should always
          be true if the sequence is used for training.
      shuffle: Whether to shuffle the batches at the start of every
          epoch.
    The effective batch size of a TrainSequence is
        (1 + neg_samples) * nr_seqs * (1 + pos_warp_samples) * \
            seq_length * (seq_length - 1) / 2.

    This is because for every sequence of frames of size 'seq_length', we
    generate a positive sample for every pair of frames. The argument
    'nr_seqs' tells us how many of these sequences will be sampled from the
    dataset for each batch and 'neg_samples' tells us how many negative
    samples are generated for every positive sample.

    For example, if neg_samples=1, nr_seqs=3, seq_length=5, this means
    that 3 sequences of 5 frames are going to be randomly sampled from the
    dataset. Every sequence of 5 frames has a total of 5 * 4 / 2 = 10
    possible pairs of frames, each of which serves as an input to the
    network. Therefore, for these parameters, there are a total of
    3 * 10 = 30 postive samples. Since neg_samples is 1, the effective
    batch will be 2 * 30 = 60.
    """
    self.samples_per_seq = max_seq_length * (max_seq_length - 1) / 2
    self._epoch = 0
    self._dataset = dataset
    self._traj_ids = traj_ids
    self._nr_seqs = train_nr_seqs
    self._pos_warp_samples = pos_warp_samples
    self._neg_samples = neg_samples
    self._target_shape = image_shape
    self._random_crop = random_crop
    self._preproc = preproc
    self._val_split = val_split
    self._shuffle = shuffle
    self._rnd = _rnd
    self._rot_type = rot_type
    self._gt_reg_scale = gt_reg_scale

    self.max_seq_length = max_seq_length
    self.min_ctc_length = min_ctc_length
    self._ctc_contract = []
    self._sample_ids = self._get_sample_ids()
    if self._shuffle:
      self._rnd.shuffle(self._sample_ids)

  @property
  def batch_size(self):
    """Return the effective number of samples in a train batch."""
    return int(self._nr_seqs * self.samples_per_seq * (1 + self._neg_samples) *
               (1 + self._pos_warp_samples))

  def _get_sample_ids(self):
    train_trajs = [
        self._dataset.get_traj_sample(
            traj_id, {DatasetProperty.LCAM, DatasetProperty.POSE})
        for traj_id in self._get_train_traj_ids()
    ]
    sample_ids = []
    for traj in train_trajs:
      for i in range(len(traj) // self.max_seq_length):
        sample_ids.append((traj, i * self.max_seq_length))
    return sample_ids

  def _get_train_traj_ids(self):
    """Return the list of training trajectory ids for the current epoch."""
    rolled_traj_ids = np.roll(self._traj_ids, shift=-self._epoch)
    split_idx = math.ceil(len(self._traj_ids) * self._val_split)
    return self._traj_ids[split_idx:]

  @property
  def ctc_contract(self):
    """Define a contract for the relationship between CTC frames.

    The CTC contract defines the relationship between direct and
    composite frames that must be followed by both the data generator
    and the model in order to build an architecture with the correct CTC
    blocks. The contract is composed of 3-uples of the form (f_d,
    f_c_src, f_c_dst) where f_d is the index of the frame of the direct
    transformation, f_c_src is the index of the first frame and f_c_dst
    is the index of the last frame of the composite transformation
    (exclusive). We can assume that poses for the pairs of frames in the
    composite transformation are contiguous in the batch, allowing us to
    use the indices to slice it.
    """
    if not self._ctc_contract and self.min_ctc_length != -1:
      # The minimum and maximum distances between frames
      min_dist = self.min_ctc_length - 1
      max_dist = self.max_seq_length - 1

      for seq in range(self._nr_seqs):
        # Compute the base_idx offset for the composite frames
        base_idx = seq * max_dist

        # We wish to find the starting index for the direct
        # transformations we'll use in the CTC blocks
        non_consec_dists = max_dist - min_dist + 1
        non_consec_samples_per_seq = \
          non_consec_dists * (non_consec_dists + 1) / 2
        direct_idx = int(seq * non_consec_samples_per_seq)
        # for dist in range(2, min_dist):
        # direct_idx += self.max_seq_length - dist

        # Once we have the starting index, we just need to build the
        # tuples that match the direct transform to the pair of frames
        # at the edges of the composite transform
        for dist in range(min_dist, self.max_seq_length):
          for comp_src_index in range(self.max_seq_length - dist):
            self._ctc_contract.append((direct_idx, base_idx + comp_src_index,
                                       base_idx + comp_src_index + dist))
            direct_idx += 1
    return self._ctc_contract

  def __len__(self):
    """Return the number of batches in the dataset sequence.

    If steps_per_update > 1, this means that each batch of data
    effectively runs steps_per_update steps in the model before applying
    the gradients. We found it easier to generate the computational
    graph by generating all the data for a given gradient update,
    instead of the data for one training step.
    """
    return 10
    return math.floor(len(self._sample_ids) / self._nr_seqs)

  def __getitem__(self, index):
    """Generate one batch of training data.

    Before returning the batch, images are converted from BGR to RGB in
    order to apply the standard keras preprocessing function.
    """
    sample_ids = self._sample_ids[self._nr_seqs * index:self._nr_seqs *
                                  (index + 1)]
    batch = self._generate_step_batch(sample_ids)
    return batch

  def on_epoch_end(self):
    """Shuffle the batch indices at the start of every epoch."""
    self._epoch += 1
    if self._shuffle:
      self._sample_ids = self._get_sample_ids()
      self._rnd.shuffle(self._sample_ids)

  def _generate_step_batch(self, sample_ids):
    """Generate a batch of data for one training step."""
    # Generate positive samples
    xs_pos_td, ys_pos_td, xs_pos, ys_pos = self._generate_pos_samples(
        sample_ids)
    xs_step_batch = [xs_pos, xs_pos_td]
    ys_step_batch = [ys_pos, ys_pos_td]

    # Generate negative samples
    if self._neg_samples > 0:
      # Generate consecutive negative pairs
      frame_shape = xs_pos_td.shape[2:]
      flat_xs_pos_td = xs_pos_td.reshape((-1,) + frame_shape)
      flat_xs_neg_td = self._generate_neg_samples(flat_xs_pos_td)
      xs_neg_td = flat_xs_neg_td.reshape((-1, self.max_seq_length - 1) +
                                         frame_shape)
      # Generate non-consecutive negative pairs
      xs_neg = self._generate_neg_samples(xs_pos)
      xs_step_batch.extend([xs_neg, xs_neg_td])

    resized_xs_step_batch = list(
        map(self._crop_and_resize_batch, xs_step_batch))
    step_batch = [resized_xs_step_batch]
    if self._gt_reg_scale > 0:
      step_batch.append(ys_step_batch)
    return step_batch

  def _crop_and_resize_batch(self, batch):
    new_batch_shape = batch.shape[:-3] + tuple(self._target_shape) + (6,)
    img_shape = batch.shape[-3:]
    flattened_batch = batch.reshape((-1,) + img_shape)
    resized_batch = []
    for img_pair in flattened_batch:
      src_frame = util.crop_and_resize(img_pair[..., :3], self._target_shape,
                                       self._random_crop)
      dst_frame = util.crop_and_resize(img_pair[..., 3:], self._target_shape,
                                       self._random_crop)
      resized_img_pair = np.concatenate([src_frame, dst_frame], axis=-1)
      resized_batch.append(resized_img_pair)
    return np.array(resized_batch, dtype=np.float32).reshape(new_batch_shape)

  def _generate_pos_samples(self, sample_ids):
    """Generate a tuple of with positive frame pairs from the sample params.

    Args:
      sample_ids: A list of tuples of the form (trajectory, start_index) used
        to retrieve `max_seq_length` frames from the trajectory and generate
        the pairs.

    Returns:
      A tuple with the positive frames for the sample_id. The first element
      of the tuple is a numpy array with shape (max_seq_length - 1, height,
      width, 6) with the consecutive frames for the samples. The second element
      of the tuple is a numpy array with the non-consecutive frame pairs for
      the given samples. The number of frame pairs in this batch varies with
      respect to the dataset parameters. If no min_ctc_length is specified, we
      generate all possible non-consecutive frame pairs. Otherwise, we generate
      only the ones separated by min_ctc_length - 1 frames up until
      max_seq_length - 1 frames. Finally, if a number of pos_warp_samples is
      specified, we generate a respective warped sample for every frame pair
      generated, including the consecutive ones and stack it along the
      non-consecutive batch.

    Examples:
      1) When no min_ctc_length is specified, we generate all possible
      non-consecutive frame pairs. For a max_seq_length of 5, we have 6
      non-consecutive pairs for each sample (18 = 6 * 3).

      Input params:
        - len(sample_ids): 3
        - max_seq_length: 5
        - min_ctc_length = -1
        - pos_warp_samples = 0
      Output shapes: (3, 4, height, width, 6), (18, height, width, 6)

      2) When a min_ctc_length is specified, we only generate the
      non-consecutive frames separated from a distance of min_ctc_length - 1.
      In this case, it means that we generate only 3 non-consecutive pairs per
      sequence.

      Input params:
        - len(sample_ids): 3
        - max_seq_length: 5
        - min_ctc_length = 4
        - pos_warp_samples = 0
      Output shapes: (3, 4, height, width, 6), (9, height, width, 6)

      3) When a min_ctc_length  and pos_warp_samples are specified, we generate
      pos_warp_samples for any valid positive sample. Since we have 4
      (consecutive) and 3 (non-consecutive) frame pairs for each sample, we
      generate a total of 42 = (4 + 3) * 3 * 2 pos_warp_samples and stack them
      along the non-consecutive batch.

      Input params:
        - len(sample_ids): 3
        - max_seq_length: 5
        - min_ctc_length = 4
        - pos_warp_samples = 2
      Output shapes: (3, 4, height, width, 6), (51, height, width, 6)
    """
    pos_xs_td, pos_ys_td, pos_xs, pos_ys = [], [], [], []
    for traj, start_index in sample_ids:
      stop_index = start_index + self.max_seq_length

      # Generate consecutive frame pairs for a sample id.
      consec_pos_xs = []
      consec_pos_ys = []
      for src_idx in range(start_index, stop_index - 1):
        frame_pair, pose = self._generate_frame_pair(traj, src_idx, src_idx + 1)
        consec_pos_xs.append(frame_pair)
        consec_pos_ys.append(pose)
      pos_xs_td.append(consec_pos_xs)
      pos_ys_td.append(consec_pos_ys)

      # Generate non-consecutive frame pairs for a sample id.
      min_dist = 2
      if self.min_ctc_length > 2:
        min_dist = self.min_ctc_length - 1
      for dist in range(min_dist, self.max_seq_length):
        for src_idx in range(start_index, stop_index - dist):
          frame_pair, pose = self._generate_frame_pair(traj, src_idx,
                                                       src_idx + dist)
          pos_xs.append(frame_pair)
          pos_ys.append(pose)

    # Convert samples lists to numpy float arrays.
    pos_xs = np.array(pos_xs, dtype=np.float32)
    pos_ys = np.array(pos_ys, dtype=np.float32)
    pos_xs_td = np.array(pos_xs_td, dtype=np.float32)
    pos_ys_td = np.array(pos_ys_td, dtype=np.float32)

    # Generate positive warp samples and stack them along the non-consecutive
    # frame pairs. We should ignore them when generating the CTC constraints
    # due from the indices specified in the CTC contract.
    if self._pos_warp_samples > 0:
      all_pos_xs = np.concatenate(
          [pos_xs_td.reshape((-1,) + pos_xs_td.shape[2:]), pos_xs])
      all_pos_ys = np.concatenate(
          [pos_ys_td.reshape((-1,) + pos_ys_td.shape[2:]), pos_ys])
      pos_warp_xs = self._generate_pos_warp_samples(all_pos_xs)
      pos_warp_ys = np.tile(all_pos_ys, reps=(self._pos_warp_samples, 1))
      pos_xs = np.concatenate([pos_xs, pos_warp_xs], axis=0)
      pos_ys = np.concatenate([pos_ys, pos_warp_ys], axis=0)
    return pos_xs_td, pos_ys_td, pos_xs, pos_ys

  def _generate_frame_pair(self, trajectory, src_idx, dst_idx):
    """Generate a frame pair from the trajectory and indices."""
    src_frame = util.crop_and_resize(trajectory.lcam[src_idx],
                                     self._target_shape, self._random_crop)
    dst_frame = util.crop_and_resize(trajectory.lcam[dst_idx],
                                     self._target_shape, self._random_crop)
    pose = util.rel_pose_from_trajectory(trajectory, self._rot_type, src_idx,
                                         dst_idx)

    if self._preproc:
      src_frame = src_frame[..., ::-1]
      src_frame = keras.applications.vgg16.preprocess_input(
          src_frame, data_format="channels_last")
      dst_frame = dst_frame[..., ::-1]
      dst_frame = keras.applications.vgg16.preprocess_input(
          dst_frame, data_format="channels_last")
    return np.concatenate([src_frame, dst_frame], axis=-1), pose

  def _generate_pos_warp_samples(self, pos_samples):
    """Generate positive warp samples for every positive sample."""
    pos_warp_samples = []
    for _ in range(self._pos_warp_samples):
      for pos_sample in pos_samples:
        src_frame = pos_sample[..., :3]
        dst_frame = pos_sample[..., 3:]
        M = util.random_affine(self._target_shape)
        src_warped_frame = util.warp_image(src_frame, M)
        dst_warped_frame = util.warp_image(dst_frame, M)
        pos_warp_samples.append(
            np.concatenate([src_warped_frame, dst_warped_frame], axis=-1))
    pos_warp_samples = np.array(pos_warp_samples).astype(np.float32)
    return pos_warp_samples

  def _generate_neg_samples(self, xs_pos):
    """Generate a list of negative frame pairs from the positive ones."""
    xs_neg = []
    for x_pos in xs_pos:
      x_pos_src, x_pos_dst = x_pos[..., :3], x_pos[..., 3:]
      for _ in range(self._neg_samples):
        M = util.random_affine(self._target_shape)
        x_neg_dst = util.warp_image(x_pos_dst, M)
        x_neg = np.concatenate([x_pos_src, x_neg_dst], axis=-1)
        xs_neg.append(x_neg)
    xs_neg = np.array(xs_neg).astype(np.float32)
    return xs_neg


class EvalSequenceGenerator(keras.utils.Sequence):
  """Generator for EvalSequences."""

  @data_ingredient.capture
  def __init__(self,
               dataset,
               traj_ids,
               seq_type,
               rot_type,
               eval_nr_seqs,
               max_seq_length,
               image_shape,
               val_split,
               preproc=True):
    self._epoch = 0
    self._dataset = dataset
    self._traj_ids = traj_ids
    self._seq_type = seq_type
    self._rot_type = rot_type
    self._nr_seqs = eval_nr_seqs
    self._max_seq_length = max_seq_length
    self._image_shape = image_shape
    self._val_split = val_split
    self._preproc = preproc

    eval_traj_ids = self._traj_ids
    if self._seq_type is SequenceType.VAL:
      eval_traj_ids = self._get_val_traj_ids()
      self._eval_seqs = [
          EvalSequence(
              dataset=self._dataset,
              traj_id=traj_id,
              rot_type=self._rot_type,
              eval_nr_seqs=self._nr_seqs,
              max_seq_length=self._max_seq_length,
              image_shape=self._image_shape,
              preproc=self._preproc) for traj_id in eval_traj_ids
      ]

  def __len__(self):
    """Return the number of EvalSequences generated per epoch."""
    return len(self._eval_seqs)

  def __getitem__(self, index):
    """Generate an EvalSequence."""
    return self._eval_seqs[index]

  def on_epoch_end(self):
    """Update the list of EvalSequences for validation sequences."""
    self._epoch += 1
    if self._seq_type is SequenceType.VAL:
      val_traj_ids = self._get_val_traj_ids()
      self._eval_seqs = [
          EvalSequence(
              dataset=self._dataset,
              traj_id=traj_id,
              rot_type=self._rot_type,
              eval_nr_seqs=self._nr_seqs,
              max_seq_length=self._max_seq_length,
              image_shape=self._image_shape,
              preproc=self._preproc) for traj_id in val_traj_ids
      ]

  def _get_val_traj_ids(self):
    """Return the list of validation trajectories for the current epoch."""
    rolled_traj_ids = np.roll(self._traj_ids, shift=-self._epoch)
    split_idx = math.ceil(len(self._traj_ids) * self._val_split)
    return self._traj_ids[:split_idx]


class EvalSequence(keras.utils.Sequence):
  """Sequence generator to create batches that can be used for evaluation.

  Attributes:
      nr_seqs: The number of sequences to sample for every each batch. The
          last batch of an EvalSequence can have less than `nr_seqs`
          sequences.
  """

  def __init__(self,
               dataset,
               traj_id,
               rot_type,
               eval_nr_seqs,
               max_seq_length,
               image_shape,
               preproc=True):
    """Create an instance of the dataset sequence generator.

    Args:
        dataset: A Dataset object which can be used to retrieve any
            trajectory belonging to the dataset.
        traj_id: The trajectory id we wish to create the sequence for.
        rot_type: The rotation type used to generate the ground-truth poses.
          One of {'quaternion', 'axis'}.
        eval_nr_seqs: The number of sequences to generate per eval batch.
        max_seq_length: The maximum number of frames per sequence.
        image_shape: The target shape that we need to transform the images
            to in order to feed the network.
        preproc: Whether or not to preprocess the frames to be fed to the
            network.
    """
    self.traj_id = traj_id
    self._rot_type = rot_type
    self._nr_seqs = eval_nr_seqs
    self._max_nr_poses = max_seq_length - 1
    self._target_shape = image_shape
    self._preproc = preproc
    self._trajectory = dataset.get_traj_sample(
        traj_id, {DatasetProperty.LCAM, DatasetProperty.POSE})
    self._sample_ids = [
        (self._trajectory, i) for i in range(len(self._trajectory) - 1)
    ]

  def __len__(self):
    """Return the number of batches in the dataset sequence."""
    total_nr_seqs = len(self._sample_ids) // self._max_nr_poses
    total_nr_batches = math.ceil(total_nr_seqs / self._nr_seqs)
    return total_nr_batches

  def __getitem__(self, index):
    """Generate one batch of test data.

    Before returning the batch, images are converted from BGR to RGB in
    order to apply the standard keras preprocessing function.
    """
    samples_per_batch = int(self._nr_seqs * self._max_nr_poses)
    sample_ids = self._sample_ids[samples_per_batch * index:samples_per_batch *
                                  (index + 1)]

    # The network only accepts as input sequences of max_nr_poses frame
    # pairs, so we remove any exceeding samples.
    nr_seqs = len(sample_ids) // self._max_nr_poses
    sample_ids = sample_ids[:nr_seqs * self._max_nr_poses]

    # Compute all of the frame pairs for each sample.
    xs, ys = [], []
    for trajectory, idx in sample_ids:
      src_img = util.crop_and_resize(trajectory.lcam[idx], self._target_shape)
      dst_img = util.crop_and_resize(trajectory.lcam[idx + 1],
                                     self._target_shape)
      if self._preproc:
        src_img = keras.applications.vgg16.preprocess_input(
            src_img[..., ::-1], data_format="channels_last")
        dst_img = keras.applications.vgg16.preprocess_input(
            dst_img[..., ::-1], data_format="channels_last")
      img_pair = np.concatenate([src_img, dst_img], axis=-1)
      pose = util.abs_pose_from_trajectory(trajectory, self._rot_type, idx + 1)
      xs.append(img_pair)
      ys.append(pose)
    xs = np.stack(xs, axis=0).astype(np.float32)
    ys = np.stack(ys, axis=0).astype(np.float32)

    # Reshape the input batch in order for it to be fed to any time-distributed
    # layers.
    sample_shape = xs.shape[1:]
    xs = np.reshape(xs, (-1, self._max_nr_poses) + sample_shape)
    return xs, ys

import logging

import cv2
import numpy as np
import scipy.spatial.transform as sst

import src.functions.lie as lie
from src.config.data import data_ingredient
from src.config.types import Rotation

ANGLE_RANGE = [-np.pi / 18, np.pi / 18]
TRANS_RANGE = [-0.05, 0.05]
SCALE_RANGE = [0.9, 1.1]


@data_ingredient.capture
def crop_and_resize(image, target_shape, random_crop=False, _rnd=None):
  """Crop the image and resize it in order to match target_shape.

  Args:
      image: The original image we wish to crop and resize.
      target_shape: The target shape of the cropped image.
      random_crop: If false, a center crop is taken, otherwise a random patch
          of the image is cropped.

  Returns:
      A cropped and resized version of the original image.
  """
  src_h, src_w = image.shape[:2]
  dst_h, dst_w = target_shape

  # First, crop the image to the desired aspect ratio
  if src_h / dst_h > src_w / dst_w:
    new_src_h = round(src_w * dst_h / dst_w)
    if random_crop:
      offset = _rnd.randint(0, src_h - new_src_h)
    else:
      offset = round((src_h - new_src_h) / 2)
    cropped_image = image[offset:offset + new_src_h, :]
  else:
    new_src_w = round(src_h * dst_w / dst_h)
    if random_crop:
      offset = _rnd.randint(0, src_w - new_src_w)
    else:
      offset = round((src_w - new_src_w) / 2)
    cropped_image = image[:, offset:offset + new_src_w]

  # Second, resize the image to the target shape.
  target_image = cv2.resize(
      cropped_image, dsize=(dst_w, dst_h), interpolation=cv2.INTER_LINEAR)
  return target_image


def scale_value(value_range, value):
  """Scale a value in [0, 1] to the given range."""
  min_val, max_val = value_range
  return (max_val - min_val) * value + min_val


@data_ingredient.capture
def random_affine(shape, _rnd=None):
  """Sample a random homography with the given constraints."""
  height, width = shape
  rand_vals = _rnd.rand(5)
  rot_angle = scale_value(ANGLE_RANGE, rand_vals[0])
  R = np.array([[np.cos(rot_angle), -np.sin(rot_angle), 0],
                [np.sin(rot_angle), np.cos(rot_angle), 0], [0, 0, 1]])
  t_x = scale_value(TRANS_RANGE, rand_vals[1]) * width
  t_y = scale_value(TRANS_RANGE, rand_vals[2]) * height
  T = np.array([[1, 0, t_x], [0, 1, t_y], [0, 0, 1]])
  s_x = scale_value(SCALE_RANGE, rand_vals[3])
  s_y = scale_value(SCALE_RANGE, rand_vals[4])
  S = np.array([[s_x, 0, 0], [0, s_y, 0], [0, 0, 1]])
  A = S.dot(T.dot(R))
  return A[:2]


def warp_image(image, M):
  """Apply an affine transformation to the given image.

  Args:
      image: The source image we wish to apply the transformation to.
      M: The affine transformation matrix with size 2 x 3.

  Returns:
      The transformed image with zero value borders.
  """
  return cv2.warpAffine(
      image,
      M,
      dsize=image.shape[1::-1],
      borderMode=cv2.BORDER_CONSTANT,
      borderValue=0)


def abs_pose_from_trajectory(trajectory, rot_type, t):
  """Retrieve the absolute pose from a trajectory at a given timestep.

  Args:
    trajectory: A TrajectorySample object with the pose field populated.
    rot_type: A Rotation type object.
    t: The timestep of the absolute pose.

  Returns:
    An array with the absolute pose for the given timestep.
  """
  position_xyz = trajectory.poses.positions_xyz[t]
  orientation_wxyz = trajectory.poses.orientations_quat_wxyz[t]
  orientation_xyzw = np.roll(orientation_wxyz, shift=-1, axis=0)
  if rot_type == Rotation.quat:
    rot = orientation_xyzw
  elif rot_type == Rotation.axis:
    rot = sst.Rotation.from_quat(orientation_xyzw).as_rotvec()
  return np.concatenate([position_xyz, rot]).astype(np.float32)


def rel_pose_from_trajectory(trajectory, rot_type, ts, td):
  """Retrieve the relative pose from a trajectory.

  Args:
    trajectory: A TrajectorySample object with the pose field populated.
    rot_type: A Rotation type object.
    ts: The source timestep of the relative pose.
    td: The target timestep of the relative pose. Should be at a timestep
      ahead of ts.

  Returns:
    An array with the relative pose between the two timesteps for the given
    trajectory.
  """
  pose_ts = abs_pose_from_trajectory(trajectory, rot_type, ts)
  pose_td = abs_pose_from_trajectory(trajectory, rot_type, td)
  abs_poses = np.stack([pose_ts, pose_td])
  rel_poses = lie.abs_to_rel_pose(abs_poses, rot_type)
  return np.squeeze(rel_poses)


@data_ingredient.capture
def redefine_trajectory(traj_length,
                        sample_length=-1,
                        random_sample=False,
                        _rnd=None):
  """Redefines the trajectory delimiter parameters.

  Arguments:
      traj_length: The length of the original trajectory.
      sample_length: The length of the sample we wish to take from the
          original trajectory.
      random_sample: If true, the sample of length 'sample_length' is
          randomly taken from the original trajectory. If false, the sample
          is taken from the start of the sequence.
  Returns:
      start_index: The starting index of the trajectory.
      traj_length: The new trajectory length.
  """
  start_index = 0

  if sample_length > traj_length:
    logging.warning("Sample length is greater than trajectory length. "
                    "Defaulting to use the entire trajectory.")
    return start_index, traj_length
  if sample_length == -1:
    if random_sample:
      logging.warning("No sample length specified despite a random "
                      "sample being requested. Defaulting to return the "
                      "entire trajectory.")
    return start_index, traj_length
  if random_sample:
    start_index = _rnd.randrange(traj_length - sample_length)
  return start_index, sample_length

"""Module used to process the KITTI dataset."""
import glob
import logging
import os
import re

import cv2
import numpy as np
from evo.tools import file_interface

from src.data.dataset import (Dataset, DatasetErrorCode, DatasetProperty,
                              TrajectorySample)
from src.data.util import redefine_trajectory

TRAIN_SPLIT = {"00", "01", "02", "03", "04", "05", "06", "07", "08"}
TEST_SPLIT = {"09", "10"}


class KITTI(Dataset):
  """Class to interact with the KITTI dataset."""

  ORIGINAL_SHAPE = (370, 1226)

  def __init__(self, dataset_dir):
    """Initialize the KITTI dataset directories."""
    super().__init__(dataset_dir)
    if not self.isvalid(dataset_dir):
      logging.error(DatasetErrorCode.INVALID_DIR_STRUCTURE.value,
                    "Expected subdirectories 'poses' and 'sequences'.")
    self.pose_dir = os.path.join(dataset_dir, "poses")
    self.seqs_dir = os.path.join(dataset_dir, "sequences")

  @staticmethod
  def isvalid(dataset_dir):
    """Whether a valid KITTI dataset exists in the given directory."""
    pose_dir = os.path.join(dataset_dir, "poses")
    seqs_dir = os.path.join(dataset_dir, "sequences")
    if not os.path.isdir(pose_dir) or \
       not os.path.isdir(seqs_dir):
      return False
    pose_pattern = re.compile("^[0-1][0-9].txt$")
    for pose_f in glob.glob(os.path.join(pose_dir, "*")):
      if pose_pattern.match(os.path.basename(pose_f)) is None:
        return False
    seqs_pattern = re.compile("^[0-2][0-9]$")
    for seqs_f in glob.glob(os.path.join(seqs_dir, "*")):
      if seqs_pattern.match(os.path.basename(seqs_f)) is None:
        return False
    return True

  @property
  def properties(self):
    """Return the DatasetProperties that this dataset provides."""
    return {
        DatasetProperty.TIME, DatasetProperty.POSE, DatasetProperty.LCAM,
        DatasetProperty.RCAM
    }

  def get_train_traj_ids(self):
    """Return a list o trajectory IDs for the KITTI training split."""
    train_ids = []
    for pose_fn in os.listdir(self.pose_dir):
      traj_id = pose_fn.split(".")[0]
      if traj_id in TRAIN_SPLIT:
        train_ids.append(traj_id)
    return sorted(train_ids)

  def get_test_traj_ids(self):
    """Return a list o trajectory IDs for the KITTI test split."""
    test_ids = []
    for pose_fn in os.listdir(self.pose_dir):
      traj_id = pose_fn.split(".")[0]
      if traj_id in TEST_SPLIT:
        test_ids.append(traj_id)
    return sorted(test_ids)

  def get_traj_sample(self,
                      traj_id,
                      props,
                      sample_length=-1,
                      random_sample=False):
    """Return a generator with a sequence of the specified arguments.

    Args:
        traj_id: A string used to uniquely reference the sequence in the
            dataset. For the KITTI dataset, this is a two-char digit, such
            as '01'.
        props: Set of DatasetProperty enums indicating which properties we
            wish to retrieve from the sequence. This dataset provides:
                1. TIME
                2. POSE
                3. LCAM
                4. RCAM
        traj_length: An integer specifying the length of the trajectory we
            wish to retrieve. If this number is greater than the
            trajectory's length, a warning is issued.
        random: A boolean indicating if we wish the trajectory of size
            traj_length to be sampled randomly from the original
            trajectory. If false, the frames are taken from in order from
            the start of the trajectory.
    Returns:
        A TrajectorySample object populated with the specified dataset
        properties.
    """
    if DatasetProperty.DEPTH in props:
      logging.error(DatasetErrorCode.INVALID_DATASET_PROP.value,
                    self.__class__.__name__, DatasetProperty.DEPTH)

    # Read the times file to get the trajectory length.
    time_fn = os.path.join(self.seqs_dir, traj_id, "times.txt")
    times = open(time_fn, "r").readlines()
    traj_length = len(times)

    start_index, traj_length = redefine_trajectory(traj_length, sample_length,
                                                   random_sample)

    dataset_props = {}
    if DatasetProperty.TIME in props:
      times_array = np.fromiter(times, dtype=np.float64) * 1000
      dataset_props[DatasetProperty.TIME] = \
          times_array[start_index:start_index + traj_length]
    if DatasetProperty.POSE in props:
      pose_fn = os.path.join(self.pose_dir, f"{traj_id}.txt")
      pose_path = file_interface.read_kitti_poses_file(pose_fn)
      pose_path.reduce_to_ids(range(start_index, start_index + traj_length))
      dataset_props[DatasetProperty.POSE] = pose_path
    if DatasetProperty.LCAM in props:
      lcam_dir = os.path.join(self.seqs_dir, traj_id, "image_2")
      dataset_props[DatasetProperty.LCAM] = \
          lambda index: cv2.imread(
              os.path.join(lcam_dir, f"{start_index + index:06d}.png"))
    if DatasetProperty.RCAM in props:
      rcam_dir = os.path.join(self.seqs_dir, traj_id, "image_3")
      dataset_props[DatasetProperty.RCAM] = \
          lambda index: cv2.imread(
              os.path.join(rcam_dir, f"{start_index + index:06d}.png"))

    return TrajectorySample(
        traj_length=traj_length,
        poses=dataset_props.get(DatasetProperty.POSE, None),
        timestamps=dataset_props.get(DatasetProperty.TIME, None),
        lcam_fn=dataset_props.get(DatasetProperty.LCAM, None),
        rcam_fn=dataset_props.get(DatasetProperty.RCAM, None),
        depth_fn=None)

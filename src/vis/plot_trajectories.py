import argparse
import os

import cv2
import matplotlib.cm as cm
import matplotlib.pyplot as plt

from src.data import get_dataset
from src.data.dataset import DatasetProperty

OUT_DIR = "bin"
DEFAULT_DIR = os.path.join("data", "raw")


class PosePlot:

  def __init__(self, output_dir):
    """Initalize pose plot with rotation and translation axes."""
    self.output_dir = output_dir
    self.fig = plt.figure(figsize=(20, 10))
    self.rot_ax = self.fig.add_subplot(121, projection="3d")
    self.rot_ax.set_title("Rotation")
    self.rot_ax.set_xlabel("Roll [rad]")
    self.rot_ax.set_ylabel("Pitch [rad]")
    self.rot_ax.set_zlabel("Yaw [rad]")
    self.trans_ax = self.fig.add_subplot(122, projection="3d")
    self.trans_ax.set_title("Translation")
    self.trans_ax.set_xlabel("X [m]")
    self.trans_ax.set_ylabel("Y [m]")
    self.trans_ax.set_zlabel("Z [m]")
    self.rot_xs, self.rot_ys, self.rot_zs = [], [], []
    self.trans_xs, self.trans_ys, self.trans_zs = [], [], []

  def rot_scatter(self, **kwargs):
    self.rot_ax.scatter(self.rot_xs, self.rot_ys, self.rot_zs, **kwargs)
    self.rot_xs, self.rot_ys, self.rot_zs = [], [], []

  def trans_scatter(self, **kwargs):
    self.trans_ax.scatter(self.trans_xs, self.trans_ys, self.trans_zs, **kwargs)
    self.trans_xs, self.trans_ys, self.trans_zs = [], [], []

  def append_rot(self, rot):
    self.rot_xs.append(rot[0])
    self.rot_ys.append(rot[1])
    self.rot_zs.append(rot[2])

  def append_trans(self, trans):
    self.trans_xs.append(trans[0])
    self.trans_ys.append(trans[1])
    self.trans_zs.append(trans[2])

  def show(self, legend_title=None):
    self.rot_ax.legend(title=legend_title)
    self.trans_ax.legend(title=legend_title)
    self.fig.waitforbuttonpress()

  def save(self, filename, legend_title=None):
    self.rot_ax.legend(title=legend_title)
    self.trans_ax.legend(title=legend_title)
    self.fig.savefig(os.path.join(self.output_dir, f"{filename}.pdf"))


def plot_datasets(datasets):
  props = {DatasetProperty.POSE}
  cmap = cm.get_cmap("inferno")

  pose_plot = PosePlot(OUT_DIR)
  for dataset_name in datasets:
    dataset = get_dataset(dataset_name, args.base_dir)
    if dataset_name == "kitti":
      marker = 'o'
      color = cmap(0.3)
    elif dataset_name == "euroc":
      marker = '^'
      color = cmap(0.6)
    elif dataset_name == "msft7":
      marker = '*'
      color = cmap(0.9)
    # Group all dataset points
    traj_ids = dataset.get_traj_ids()
    for traj_id in traj_ids:
      trajectory = dataset.get_traj_sample(traj_id, props)
      poses_euler = trajectory.poses.get_orientations_euler()
      poses_trans = trajectory.poses.positions_xyz
      for pose_euler, pose_trans in zip(poses_euler, poses_trans):
        pose_plot.append_trans(pose_euler)
        pose_plot.append_rot(pose_trans)
    pose_plot.rot_scatter(
        s=1,
        marker=marker,
        label=f"{dataset_name}",
        color=color,
    )
    pose_plot.trans_scatter(
        s=1,
        marker=marker,
        label=f"{dataset_name}",
        color=color,
    )
  pose_plot.save("-".join(args.datasets), legend_title="dataset_name")


def plot_seqs(dataset_name):
  props = {DatasetProperty.POSE}
  cmap = cm.get_cmap("inferno")

  pose_plot = PosePlot(OUT_DIR)
  dataset = get_dataset(dataset_name, args.base_dir)

  # Group all dataset points
  traj_ids = dataset.get_traj_ids()
  color_inc = 1.0 / len(traj_ids)
  cur_color = 0.0
  prev_scene = os.path.dirname(traj_ids[0])
  for traj_id in traj_ids:
    trajectory = dataset.get_traj_sample(traj_id, props)
    if dataset_name == "msft7":
      cur_scene = os.path.dirname(traj_id)
      if cur_scene != prev_scene:
        pose_plot.rot_scatter(s=1, label=prev_scene, color=cmap(cur_color))
        pose_plot.trans_scatter(s=1, label=prev_scene, color=cmap(cur_color))
        prev_scene = cur_scene
        cur_color += 1 / 7
    poses_euler = trajectory.poses.get_orientations_euler()
    poses_trans = trajectory.poses.positions_xyz
    for pose_euler, pose_trans in zip(poses_euler, poses_trans):
      pose_plot.append_trans(pose_euler)
      pose_plot.append_rot(pose_trans)
    if dataset_name != "msft7":
      pose_plot.rot_scatter(s=1, label=traj_id, color=cmap(cur_color))
      pose_plot.trans_scatter(s=1, label=traj_id, color=cmap(cur_color))
      cur_color += color_inc
  pose_plot.save(f"{dataset_name}-seqs", legend_title="traj_id")


if __name__ == "__main__":
  parser = argparse.ArgumentParser()
  parser.add_argument(
      "datasets",
      choices=["kitti", "euroc", "msft7"],
      nargs="+",
      help="Specify which datasets to run the demo for")
  parser.add_argument(
      "-i",
      "--base_dir",
      default=DEFAULT_DIR,
      help="Common base directory where datasets are stored.")
  parser.add_argument(
      "-s",
      "--scope",
      choices=["dataset", "seq"],
      help=("Scope of the analysis. If dataset is specified, the data "
            "points between the different datasets will be analyzed. If seq "
            "is specified, the data points between frames from each "
            "sequence will be analyzed. If scope is seq, only one dataset "
            "can be specified."))
  args = parser.parse_args()

  if args.scope == "seq":
    assert len(args.datasets) == 1, (
        "If plotting a sequence, only one dataset can be passed as argument.")

  try:
    if args.scope == "dataset":
      plot_datasets(args.datasets)
    else:
      plot_seqs(args.datasets[0])
  except KeyboardInterrupt:
    cv2.destroyAllWindows()

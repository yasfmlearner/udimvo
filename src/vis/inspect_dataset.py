"""Play the entire dataset as a video."""
import argparse
import os

import cv2

from src.data import get_dataset

DEFAULT_DIR = os.path.join("data", "raw")

parser = argparse.ArgumentParser()
parser.add_argument(
    "datasets",
    choices=["kitti", "euroc", "msft7"],
    nargs="+",
    help="Specify which datasets to run the demo for")
parser.add_argument(
    "-i",
    "--base_dir",
    default=DEFAULT_DIR,
    help="Common base directory where datasets are stored.")
args = parser.parse_args()

try:
  for dataset_name in args.datasets:
    dataset = get_dataset(dataset_name, args.base_dir)
    print(f"Playing videos from {dataset.__class__.__name__} dataset.\n"
          f"Controls:\n"
          f"  esc: Skip to next dataset\n"
          f"  n: Skip to next video\n"
          f"  p: Pause video\n"
          f"  space: Continue paused video\n")
    traj_ids = dataset.get_traj_ids()
    for traj_id in traj_ids:
      try:
        dataset.play_sequence(traj_id, dataset.properties)
      except KeyboardInterrupt:
        break
except KeyboardInterrupt:
  cv2.destroyAllWindows()

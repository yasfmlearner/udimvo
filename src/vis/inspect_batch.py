import argparse
import importlib
import os

import cv2
import numpy as np

from src.config.types import Rotation
from src.data.dataset import EvalSequenceGenerator, SequenceType, TrainSequence

INFOMAX_LOSS_SCALE = 1
NR_SEQS = 3
NEG_SAMPLES = 2
POS_WARP_SAMPLES = 1
SEQ_LENGTH = 5
CTC_LENGTH = 4
TARGET_SHAPE = (128, 416)
STEPS_PER_UPDATE = 1
RANDOM_CROP = False
VAL_SPLIT = 0.05
DEFAULT_DIR = os.path.join(os.path.expanduser("~"), "Documents/datasets")

# If there's no infomax loss component, we shouldn't generate negative
# samples
if INFOMAX_LOSS_SCALE == 0:
  NEG_SAMPLES = 0

parser = argparse.ArgumentParser()
parser.add_argument(
    "dataset",
    choices=["kitti", "euroc", "msft7"],
    help="Specify which dataset to run the demo for")
parser.add_argument(
    "seq_type",
    type=SequenceType,
    choices=list(SequenceType),
    help="Type of sequence we wish to inspect batches for.")
parser.add_argument(
    "-i",
    "--base_dir",
    default=DEFAULT_DIR,
    help="Common base directory where datasets are stored.")
args = parser.parse_args()


def show_train_batches(dataset_obj):
  seq = TrainSequence(
      dataset_obj,
      dataset_obj.get_train_traj_ids(),
      train_nr_seqs=NR_SEQS,
      neg_samples=NEG_SAMPLES,
      pos_warp_samples=POS_WARP_SAMPLES,
      max_seq_length=SEQ_LENGTH,
      min_ctc_length=CTC_LENGTH,
      image_shape=TARGET_SHAPE,
      preproc=False,
      shuffle=True,
      val_split=VAL_SPLIT,
      random_crop=RANDOM_CROP,
  )
  seq.on_epoch_end()

  image_height, image_width = TARGET_SHAPE
  for batch_idx, batch in enumerate(seq):
    print(f"Inspecting batch {batch_idx}/{len(seq)}\r", end="")
    batch = batch[0]

    # Just look at the first batch since steps_per_update is 1.
    frame_shape = batch[0].shape[-3:]
    pos_batch = np.concatenate([
        batch[0].reshape((-1,) + frame_shape),
        batch[1].reshape((-1,) + frame_shape)
    ])
    if NEG_SAMPLES > 0:
      neg_batch = np.concatenate([
          batch[2].reshape((-1,) + frame_shape),
          batch[3].reshape((-1,) + frame_shape)
      ])

    # Create a new frame to populate with the generated batch images.
    frame_height = image_height * (NEG_SAMPLES + 1)
    frame_width = image_width * 2
    frame = np.empty((frame_height, frame_width, 3), dtype=np.uint8)

    # Add positive samples in the first row of the new frame and negative
    # samples below it.
    for pos_idx in range(len(pos_batch)):
      frame[:image_height, :image_width] = pos_batch[pos_idx, ..., :3]
      frame[:image_height, image_width:] = pos_batch[pos_idx, ..., 3:]
      for neg_idx in range(NEG_SAMPLES):
        frame[image_height * (neg_idx + 1):image_height *
              (neg_idx + 2), :image_width] = neg_batch[pos_idx * NEG_SAMPLES +
                                                       neg_idx, ..., :3]
        frame[image_height * (neg_idx + 1):image_height * (neg_idx + 2),
              image_width:] = neg_batch[pos_idx * NEG_SAMPLES + neg_idx, ...,
                                        3:]
      cv2.imshow("Batch", frame)
      cv2.waitKey(60)


def show_eval_batches(dataset_obj, seq_type):
  if seq_type is SequenceType.TEST:
    traj_ids = dataset_obj.get_test_traj_ids()
  else:
    traj_ids = dataset_obj.get_train_traj_ids()

  eval_seq_gen = EvalSequenceGenerator(
      dataset_obj,
      traj_ids,
      seq_type=seq_type,
      rot_type=Rotation.quat,
      eval_nr_seqs=NR_SEQS,
      max_seq_length=SEQ_LENGTH,
      image_shape=TARGET_SHAPE,
      val_split=VAL_SPLIT,
      preproc=False)
  image_height, image_width = TARGET_SHAPE
  for eval_seq in eval_seq_gen:
    for batch_idx, eval_batch in enumerate(eval_seq):
      print(f"Inspecting batch {batch_idx}/{len(eval_seq)}\r", end="")

      img_batch, pose_batch = eval_batch
      img_batch = img_batch.reshape((-1,) + img_batch.shape[-3:])

      # Create a new frame to populate with the generated batch images.
      frame_width = image_width * 2
      frame = np.empty((image_height, frame_width, 3), dtype=np.uint8)

      for img_pair, pose in zip(img_batch, pose_batch):
        frame[:image_height, :image_width] = img_pair[..., :3]
        frame[:image_height, image_width:] = img_pair[..., 3:]
        print("Ground truth pose : ", pose)
        cv2.imshow("Batch", frame)
        cv2.waitKey(30)


try:
  dataset_mod = importlib.import_module(f"src.data.{args.dataset}")
  dataset_cls = getattr(dataset_mod, args.dataset.upper())
  dataset_path = os.path.join(args.base_dir, args.dataset)
  dataset = dataset_cls(dataset_path)

  if args.seq_type == SequenceType.TRAIN:
    show_train_batches(dataset)
  else:
    show_eval_batches(dataset, args.seq_type)

except KeyboardInterrupt:
  cv2.destroyAllWindows()

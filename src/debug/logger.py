"""Message logging utility functions."""
import os
import sys
from enum import Enum


class Color(Enum):
  """Terminal colors."""

  # yapf: disable
  RESET   = '\033[0m'
  GREY    = '\033[90m'  # debug
  RED     = '\033[91m'  # error
  GREEN   = '\033[92m'  # default
  YELLOW  = '\033[93m'  # info
  BLUE    = '\033[94m'
  MAGENTA = '\033[95m'  # warning
  CYAN    = '\033[96m'
  WHITE   = '\033[97m'
  # yapf: enable


def _log_colored(color, *args, file=sys.stdout, **kwds):
  """Log colored only if output file is stdout."""
  if file is sys.stdout:
    print(color.value, end="")
    print(*args, Color.RESET.value, **kwds)
  else:
    print(*args, **kwds, file=file)


def log(*args, **kwds):
  """Log message."""
  _log_colored(Color.GREEN, "[LOG]", *args, **kwds)


def log_debug(*args, **kwds):
  """Log debug message."""
  if os.getenv("DEBUG", None) is not None:
    _log_colored(Color.GREY, "[DEBUG]", *args, **kwds)


def log_info(*args, file=sys.stdout, **kwds):
  """Log info message."""
  _log_colored(Color.YELLOW, "[INFO]", *args, file=file, **kwds)


def log_warning(*args, **kwds):
  """Log warning message."""
  _log_colored(Color.MAGENTA, "[WARNING]", *args, **kwds)


def log_error(*args, **kwds):
  """Log error message. Exits program after logging."""
  _log_colored(Color.RED, "[ERROR]", *args, **kwds)
  sys.exit()

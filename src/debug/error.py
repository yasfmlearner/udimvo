# pylint: disable=no-value-for-parameter
import glob
import os

import numpy as np
import tensorflow as tf
from natsort import natsorted

import src.config.train as train_config
from src.functions import utils


class NumericError:

  @train_config.ex.capture
  def __init__(self, ckpt, error_dir, ckpt_dir):
    self.ckpt = ckpt
    self.error_dir = error_dir
    utils.create_dir(error_dir)
    self.error_ckpt_dir = os.path.join(self.error_dir, ckpt_dir)
    self.manager = tf.train.CheckpointManager(
        self.ckpt, self.error_ckpt_dir, max_to_keep=10)

  def save(self, err, data):
    """Save error info to run directory.

    Includes saving the batch of data that generated the error, the error
    message thrown by enable check numerics, and the checkpoint at the step
    that originated the error.

    Args:
      err: A tf.errors.InvalidArgumentError object thrown by the model.
      data: A sequence of tensors with the input data for the current batch.
    """
    error_unique_dir = utils.unique_dir(self.error_dir)
    utils.create_dir(error_unique_dir)
    for i, input_tensor in enumerate(data):
      np.save(os.path.join(error_unique_dir, f"input_{i}.npy"), input_tensor)
    with open(os.path.join(error_unique_dir, "msg.txt"), "w") as f:
      f.write(err.message)
    save_path = self.manager.save(checkpoint_number=self.ckpt.step)
    print(f"Saved checkpoint to {save_path} after encountering error in "
          "training loop")

  def load(self):
    """Load the model from the latest error checkpoint.

    Returns:
      Two element tuple with input batch data that caused the error and the
      message thrown by tensorflow.
    """
    latest_id = utils.maximum_dir_id(self.error_dir)
    latest_dir = os.path.join(self.error_dir, str(latest_id))
    if not os.path.isdir(latest_dir):
      raise ValueError("Couldn't find any valid error directory to load.")
    status = self.ckpt.restore(self.manager.latest_checkpoint)
    latest_ckpt = self.manager.latest_checkpoint
    if latest_ckpt and status.assert_existing_objects_matched():
      print(f"Restored error from {latest_ckpt}")
    else:
      print(f"Couldn't restore error checkpoint from {latest_ckpt}.")
      return None
    data = []
    for npy_fn in natsorted(glob.glob(os.path.join(latest_dir, "input*.npy"))):
      data.append(np.load(npy_fn))
    with open(os.path.join(latest_dir, "msg.txt")) as f:
      msg = f.read()
    return data, msg

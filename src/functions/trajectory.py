import itertools

import matplotlib.pyplot as plt
import numpy as np
import scipy.spatial.transform as sst
from evo.core import metrics, trajectory
from evo.tools import plot

import src.functions.lie as lie
from src.config.types import Rotation


def create_evo_path(poses, rot_type=None):
  """Create an evo PosePath3D trajectory for poses."""
  if rot_type is None:
    path = trajectory.PosePath3D(poses_se3=poses)
  else:
    trans_xyz, rot = poses[:, :3], poses[:, 3:]
    if rot_type == Rotation.quat:
      quat_wxyz = np.roll(rot, shift=1, axis=1)
    elif rot_type == Rotation.axis:
      quat_xyzw = sst.Rotation.from_rotvec(rot).as_quat()
      quat_wxyz = np.roll(quat_xyzw, shift=1, axis=1)
    path = trajectory.PosePath3D(trans_xyz, quat_wxyz)
  return path


class TrajectoryEvalPair:

  def __init__(self, y_true, y_pred, rot_type=None, name="traj", rel=True):
    """Initialize a trajectory pair for evaluation.

    The shape of y_true and y_pred depend on rot_type. If None, an (N, 4, 4)
    array is expected with the SE(3) matrices. If rot_type is quaternion, then
    an (N, 7) array is expected with the first three positions of each 7D
    array as the translation vector and the last four as the xyzw quaternion.
    If rot_type is axis, we expect an (N, 6) array with the first three
    positions for the xyz translation and last three for the rotation vector
    in the axis angle representation.

    Args:
      y_true: Array with the absolute ground-truth poses of the trajectory.
      y_pred: Array with the estimated poses for the trajectory.
      rot_type: A Rotation config type. If None, poses are expected to be SE(3)
        matrices.
      name: A string with the name assigned to the trajectory. Used as the base
        name for the plots.
      rel: Whether the estimated poses are relative or absolute. If they are
        relative, we need to compose them to form an absolute trajectory that's
        comparable to y_true.
    """
    assert y_true.shape == y_pred.shape
    if rel:
      if rot_type is None:
        y_true = np.concatenate([lie.NULL_SE3, y_true], axis=0)
        y_pred = lie.rel_to_abs_se3(y_pred)
      else:
        y_true = np.concatenate([lie.null_pose(rot_type), y_true], axis=0)
        y_pred = lie.rel_to_abs_pose(y_pred, rot_type)
    self._name = name
    self._nr_poses = len(y_true)
    self._y_true_path = create_evo_path(y_true, rot_type)
    self._y_pred_path = create_evo_path(y_pred, rot_type)
    # TODO: Investigate if there's a bug with trajectory alignment. Position
    # scale should be similar after it but is way off for some trajectories.
    self._y_pred_path = trajectory.align_trajectory(
        self._y_pred_path, self._y_true_path, correct_scale=True)
    self._ate = None
    self._rpe_rot = None
    self._rpe_rot_per_delta = []
    self._rpe_trans = None
    self._rpe_trans_per_delta = []
    max_delta = min(800, 100 * (self._y_true_path.path_length // 100),
                    100 * (self._y_pred_path.path_length // 100))
    self._rpe_deltas = np.arange(100, max_delta + 1, 100)
    self._figsize = (10, 10)
    self._plot_collection = None

  def __len__(self):
    """Return the number of poses in the eval trajectory."""
    return self._nr_poses

  @property
  def figs(self):
    """Return ordered dictionary of matplotlib figures."""
    if self._plot_collection is None:
      self.plot()
    return self._plot_collection.figures

  def close_figs(self):
    """Close all matplotlib figures and reset plot collection."""
    plt.close("all")
    self._plot_collection = None

  def plot(self):
    """Create the plot representation required to show and export."""
    self._plot_collection = plot.PlotCollection(f"traj-{self._name}-plot")
    fig_xyz, axarr_xyz = plt.subplots(
        3, sharex="col", figsize=tuple(self._figsize))
    fig_rpy, axarr_rpy = plt.subplots(
        3, sharex="col", figsize=tuple(self._figsize))
    fig_traj = plt.figure(figsize=tuple(self._figsize))
    plot_mode = plot.PlotMode.xyz
    ax_traj = plot.prepare_axis(fig_traj, plot_mode)

    fig_traj.set_facecolor("white")
    ax_traj.set_facecolor("white")
    for ax in itertools.chain(axarr_xyz, axarr_rpy):
      ax.set_facecolor("white")

    # Plot reference trajectory
    ref_color, ref_alpha = "black", 0.5
    traj_color, traj_alpha = (0.95, 0.5, 0), 0.75
    plot.traj(
        ax_traj, plot_mode, self._y_true_path, color=ref_color, alpha=ref_alpha)
    plot.draw_coordinate_axes(ax_traj, self._y_true_path, plot_mode)
    plot.traj_xyz(
        axarr_xyz, self._y_true_path, color=ref_color, alpha=ref_alpha)
    plot.traj_rpy(
        axarr_rpy, self._y_true_path, color=ref_color, alpha=ref_alpha)

    # Plot estimated trajectory
    plot.traj(
        ax_traj,
        plot_mode,
        self._y_pred_path,
        color=traj_color,
        alpha=traj_alpha)
    plot.traj_xyz(
        axarr_xyz, self._y_pred_path, color=traj_color, alpha=traj_alpha)
    plot.traj_rpy(
        axarr_rpy, self._y_pred_path, color=traj_color, alpha=traj_alpha)

    # Add plots to collection
    self._plot_collection.add_figure("trajectories", fig_traj)
    self._plot_collection.add_figure("xyz-view", fig_xyz)
    self._plot_collection.add_figure("rpy-view", fig_rpy)

  def show(self):
    """Display the plot collection."""
    assert self._plot_collection is not None, (
        "The method plot() must be called before showing the plots.")
    self._plot_collection.show()

  def export(self, path):
    """Save the plot collection to the specified path."""
    assert self._plot_collection is not None, (
        "The method plot() must be called before exporting the plots.")
    self._plot_collection.export(path)

  @property
  def rpe_trans(self):
    """Compute the Relative Positional Error for the translation part.

    The same procedure detailed in the KITTI benchmark is used here to
    compute the RPE_trans error. For every delta in the set {100m, 200m,
    ..., 800m}, we compute the mean RMSE for all the possible pairs of
    frames delta meters away from each other. The mean absolute RMSE is
    then converted to a relative RMSE by dividing it by the delta.
    Finally, we set the rpe_trans value as the mean between the RMSEs
    computed for each delta.
    """
    if self._rpe_trans is None:
      self._rpe_trans_per_delta = self._compute_rpe_per_delta(
          metrics.PoseRelation.translation_part)
      self._rpe_trans = np.mean(self._rpe_trans_per_delta)
    return self._rpe_trans

  @property
  def rpe_rot(self):
    """Compute the Relative Positional Error for the rotation part.

    The same procedure detailed in the KITTI benchmark is used here to
    compute the RPE_trans error. For every delta in the set {100m, 200m,
    ..., 800m}, we compute the mean RMSE for all the possible pairs of
    frames delta meters away from each other. The mean absolute RMSE is
    then converted to a relative RMSE by dividing it by the delta.
    Finally, we set the rpe_trans value as the mean between the RMSEs
    computed for each delta.
    """
    if self._rpe_rot is None:
      self._rpe_rot_per_delta = self._compute_rpe_per_delta(
          metrics.PoseRelation.rotation_angle_deg)
      self._rpe_rot = np.mean(self._rpe_rot_per_delta)
    return self._rpe_rot

  @property
  def ate(self):
    """Compute the Absolute Translational Error for trajectory.

    For the APE, we simply compare the entire sequence and return the
    absolute error in meters.
    """
    if self._ate is None:
      evo_ate = metrics.APE(pose_relation=metrics.PoseRelation.translation_part)
      evo_ate.process_data((self._y_true_path, self._y_pred_path))
      abs_rmse = evo_ate.get_statistic(metrics.StatisticsType.rmse)
      self._ate = abs_rmse
    return self._ate

  def _compute_rpe_per_delta(self, pose_relation):
    """Compute the RMSE error for each delta."""
    rmse_per_delta = []
    for delta in self._rpe_deltas:
      evo_rpe = metrics.RPE(
          pose_relation=pose_relation,
          delta=delta,
          delta_unit=metrics.Unit.meters,
          all_pairs=True)
      evo_rpe.process_data((self._y_true_path, self._y_pred_path))
      abs_rmse = evo_rpe.get_statistic(metrics.StatisticsType.rmse)
      rel_rmse = abs_rmse / delta
      rmse_per_delta.append(rel_rmse)
    return rmse_per_delta

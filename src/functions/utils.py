#pylint: disable=no-value-for-parameter
"""Defines utility functions used across the program."""
import io
import logging
import multiprocessing
import os

import tensorflow as tf

import src.config.train as train_conf


def create_dir(dirname):
  """Create directory if it doesn't exist."""
  if not os.path.isdir(dirname):
    os.makedirs(dirname)
    logging.info("Created directory '%s'.", dirname)
    return dirname
  return None


def maximum_dir_id(base_dirname):
  """Return the numeric id of the latest directory."""
  fns = os.listdir(base_dirname)
  filtered_fns = filter(
      lambda fn: fn.isdigit() and os.path.isdir(os.path.join(base_dirname, fn)),
      fns)
  ids = list(map(int, filtered_fns))
  if len(ids) > 0:
    return max(ids)
  return 0


def unique_dir(base_dirname):
  """Create a unique directory underneath `base_dirname` with a numeric id.

  Args:
    base_dirname: The base directory underneath which the directories with
      numeric ids will be created.

  Returns:
    The name of the new directory if it was created. Otherwise, None.
  """
  max_id = maximum_dir_id(base_dirname)
  return create_dir(os.path.join(base_dirname, str(max_id + 1)))


@train_conf.ex.capture
def set_device(gpu_device_id, _log):
  """Configure the device to train the model on.

  If no GPUs are available we default to using the CPU. If more than
  one GPU is available and there's a valid gpu_device_id, we use it.
  Otherwise, if the device id is invalid, we default to using the 0th
  GPU.
  """
  # Make sure GPU has enough memory to work with.
  gpus = tf.config.experimental.list_physical_devices('GPU')
  if len(gpus) > 0:
    if gpu_device_id > len(gpus) - 1:
      _log.warning(
          "GPU device id %d doesn't exist in this machine. "
          "Defaulting to GPU-0.", gpu_device_id)
      gpu_device_id = 0
    tf.config.experimental.set_visible_devices(gpus[gpu_device_id], "GPU")
    tf.config.experimental.set_memory_growth(gpus[gpu_device_id], True)
  else:
    # Uses at most cores // 2 cores to run tensorflow.
    cores = multiprocessing.cpu_count()
    tf.config.threading.set_inter_op_parallelism_threads(cores // 4)
    tf.config.threading.set_intra_op_parallelism_threads(cores // 4)
    _log.info("No GPU hardware device available. Defaulting to " "CPU.")


def fig_to_image(figure):
  """Convert the matplotlib plot specified by 'figure' to a PNG image."""
  # Save the plot to a PNG in memory.
  buf = io.BytesIO()
  figure.savefig(buf, format='png')
  # Reset file seed
  buf.seek(0)
  # Convert PNG buffer to TF image
  image = tf.image.decode_png(buf.getvalue(), channels=4)
  # Add the batch dimension
  image = tf.expand_dims(image, 0)
  return image


def assert_valid_resume_dir(resume_dir):
  """Check that `resume_dir` exists and has a config json."""
  assert os.path.isdir(resume_dir), "Resume argument must be a directory."
  assert os.path.isfile(os.path.join(resume_dir, "config.json")), (
      "Resume directory must have a 'config.json' file in its root.")

"""This module provides a tensorflow implementation of common lie functions.

These functions are needed to transform data from the latent space to a
composable format, such as SE(3), which allows us to compute composite
transformations.
"""
import tensorflow as tf
import tensorflow_graphics.geometry.transformation as tfg

from src.config.types import Rotation

NULL_SE3 = tf.expand_dims(tf.eye(4, dtype=tf.float32), axis=0)


@tf.function(input_signature=[
    tf.TensorSpec([None, 3, 3], dtype=tf.float32),
    tf.TensorSpec([None, 3], dtype=tf.float32)
])
def se3_matrix(so3, trans):
  """Return an SE(3) matrix from an SO(3) rotation and a translation.

  Args:
    so3: An n x 3 x 3 tensor with valid SO(3) matrices.
    trans: An n x 3 tensor with xyz translations.

  Returns:
    An n x 4 x 4 array with SE(3) matrices.
  """
  se3 = tf.concat([so3, trans[..., None]], axis=-1)
  filler = tf.constant([0, 0, 0, 1], dtype=tf.float32)
  filler = tf.broadcast_to(filler, shape=[tf.shape(se3)[0], 1, 4])
  se3 = tf.concat([se3, filler], axis=1)
  return se3


@tf.function(experimental_relax_shapes=True)
def pose_to_se3(pose, rot_type):
  """Compute the SE(3) matrix tensor from a translation and quaternion.

  Args:
    pose: An n x [6-7] tensor with the poses we wish to normalize. The first
      three positions of the tensor are the translations in x, y, and z,
      respectively.  The last four or three pose elements represent the
    rot_type: A Rotation config type.
      the pose. Can be one of:
        quaternion: Expects four pose elements with the x, y, z, and w
          quaternion elements, respectively.
        axis: Expects three pose elements with that when unit-normalized allows
          to extract an axis x, y, z, and an angle of rotation with respect to
          this axis.
  Returns:
    An n x 4 x 4 tensor with the SE(3) matrices for the given pose.
  Raises:
    InvalidArgumentError: If debug mode is on and the rotation used isn't
      normalized.
  """
  trans, rot = pose[..., :3], pose[..., 3:]
  if rot_type == Rotation.quat:
    so3 = tfg.rotation_matrix_3d.from_quaternion(rot)
  elif rot_type == Rotation.axis:
    angle = tf.norm(rot, axis=-1, keepdims=True)
    axis = tf.math.l2_normalize(rot, axis=-1)
    so3 = tfg.rotation_matrix_3d.from_axis_angle(axis, angle)
  else:
    raise ValueError(f"Unsupported rotation type {rot_type}.")
  return se3_matrix(so3, trans)


@tf.function(experimental_relax_shapes=True)
def se3_to_pose(se3, rot_type):
  """Compute the pose vector from the SE(3) matrix tensor.

  Args:
    se3: An n x 4 x 4 tensor with a series of SE(3) matrices. If debug mode is
      on, the respective SO(3) matrices should be valid, i.e. its R * R^T = 1
      and det(R) = 1.
    rot_type: A Rotation config type.
      the pose. Can be one of:
        quaternion: Expects four pose elements with the x, y, z, and w
          quaternion elements, respectively.
        axis: Expects three pose elements with that when unit-normalized allows
          to extract an axis x, y, z, and an angle of rotation with respect to
          this axis.
  Returns
    An n x [6-7] tensor with the corresponding pose of each SE(3) matrix. The
      pose is represented as a translation vector followed by a rotation.
  Raises:
    InvalidArgumentError: If debug mode is on and the rotation matrix of the
      SE(3) transform isn't normalized, i.e. R * R^T != I and det(R) != 1.
  """
  so3 = se3[..., :3, :3]
  trans = se3[..., :3, 3]
  if rot_type == Rotation.quat:
    rot = tfg.quaternion.from_rotation_matrix(so3)
  elif rot_type == Rotation.axis:
    axis, angle = tfg.axis_angle.from_rotation_matrix(so3)
    rot = axis * angle
  else:
    raise ValueError(f"Unsupported rotation type {rot_type}.")
  return tf.concat([trans, rot], axis=-1)


@tf.function(input_signature=[tf.TensorSpec([None, 4, 4], dtype=tf.float32)])
def se3_compose(se3_matrices):
  """Compose a batch of SE(3) matrices into a single transformation.

  The composition is performed from first to last. This means we're assuming
  that the last SE(3) matrix corresponds to the last pose transformation in
  a sequence. If we have a sequence of consecutive poses in time T_1, T_2,
  and T_3, the returned composed transform is T_1->3 = T_3 * (T_2 * T_1).

  Args:
      se3_matrices: An n x 4 x 4 tensor with a series of SE(3) matrices.

  Returns:
      A 1 x 4 x 4 tensor with a single SE(3) matrix consisting of the
      composition between the series of SE(3) matrices.
  """
  composed_se3 = tf.foldl(
      fn=lambda se3_comp, se3: tf.matmul(se3, se3_comp), elems=se3_matrices)
  return tf.expand_dims(composed_se3, axis=0)


@tf.function(experimental_relax_shapes=True)
def normalize_pose(pose, rot_type):
  """Normalize the rotation component of a pose.

  Args:
    pose: An n x [6-7] tensor with the poses we wish to normalize. The first
      three positions of the tensor are the translations in x, y, and z,
      respectively.  The last four pose elements represent the rotation.
    rot_type: A Rotation config type.
        quat: Expects four pose elements with the x, y, z, and w
          quaternion elements, respectively.
        axis: Expects three pose elements with that when unit-normalized allows
          to extract an axis x, y, z, and an angle of rotation with respect to
          this axis.
  Returns:
    An n x [6-7] tensor of poses with unit-normalized rotations.
  """
  if rot_type == Rotation.quat:
    rot_norm = tfg.quaternion.normalize(pose[..., 3:])
    return tf.concat([pose[..., :3], rot_norm], axis=-1)
  if rot_type == Rotation.axis:
    return tf.identity(pose)
  raise ValueError(f"Unrecognized rotation representation {rot_type}")


@tf.function(input_signature=[tf.TensorSpec([None, 4, 4], dtype=tf.float32)])
def inverse_se3(se3):
  """Compute the inverse se3 matrix.

  Args:
    se3: An n x 4 x 4 tensor with valid SE(3) matrices.

  Returns:
    An n x 4 x 4 tensor with inverse SE(3) matrices.
  """
  so3, trans = se3[:, :3, :3], se3[:, :3, 3:]
  so3_inv = tf.transpose(so3, perm=[0, 2, 1])
  trans_inv = -so3_inv @ trans
  return se3_matrix(so3_inv, tf.squeeze(trans_inv, -1))


@tf.function(input_signature=[
    tf.TensorSpec([None, 4, 4], dtype=tf.float32),
    tf.TensorSpec([None, 4, 4], dtype=tf.float32)
])
def relative_se3(se3_matrix1, se3_matrix2):
  """Compute the relative SE(3) pose between two SE(3) pose matrices.

  This functions expects se3_matrix2 to be a pose in a time step after
  se3_matrix1 and computes the relative pose as R1 = M1^-1 * M2. This way it's
  possible to convert relative poses to an absolute pose by always placing the
  lower time step poses in the left hand side of the matrix multiplication,
  such that M3 = M1 * (R1 * R2) = M1 * (M1^-1 * M2 * M2^-1 * M3) = M3.

  Args:
    se3_matrix1: An n x 4 x 4 tensor with SE(3) pose matrices.
    se3_matrix2: Another n x 4 x 4 tensor with SE(3) pose matrices.

  Returns:
    An n x 4 x 4 tensor with the relative poses between the SE(3) matrix in
      the respective index of the input tensors.
  """
  return inverse_se3(se3_matrix1) @ se3_matrix2


@tf.function(input_signature=[tf.TensorSpec([None, 4, 4], dtype=tf.float32)])
def abs_to_rel_se3(abs_se3):
  """Convert absolute SE(3) poses to relative SE(3) poses.

  Args:
    abs_se3: An n x 4 x 4 tensor with absolute SE(3) poses. The batch dimension
      must be >= 2.

  Returns:
    An (n - 1) x 4 x 4 tensor with the relative SE(3) poses between the
      successive absolute poses.
  """
  return tf.map_fn(
      fn=lambda abs_se3s: tf.squeeze(
          relative_se3(
              se3_matrix1=abs_se3s[0][None, ...],
              se3_matrix2=abs_se3s[1][None, ...])),
      elems=(abs_se3[:-1, ...], abs_se3[1:, ...]),
      dtype=tf.float32)


@tf.function(experimental_relax_shapes=True)
def abs_to_rel_pose(abs_pose, rot_type):
  """Convert a tensor of absolute poses of `rot_type` to relative poses.

  Args:
    abs_pose: An n x [6-7] array with absolute poses.
    rot_type: The Rotation config type use to represent a rotation in the pose.

  Returns:
    An array of (n - 1) x [6-7] absolute poses.
  """
  abs_se3 = pose_to_se3(abs_pose, rot_type)
  rel_se3 = abs_to_rel_se3(abs_se3)
  rel_pose = se3_to_pose(rel_se3, rot_type)
  return rel_pose


@tf.function(input_signature=[tf.TensorSpec([None, 4, 4], dtype=tf.float32)])
def rel_to_abs_se3(rel_se3_matrices):
  """Convert array of relative poses to absolute poses w.r.t the first.

  Args:
    rel_se3_matrices: An n x 4 x 4 tensor of relative SE(3) pose matrices.

  Returns:
    An (n + 1) x 4 x 4 tensor with the absolute poses computed by applying
      the relative poses successively starting from an initial identity pose.
  """
  batch_size = tf.shape(rel_se3_matrices)[0]
  abs_se3 = tf.TensorArray(
      tf.float32, size=batch_size + 1, clear_after_read=False)
  abs_se3 = abs_se3.write(0, NULL_SE3[0])
  for i in tf.range(1, batch_size + 1):
    abs_se3 = abs_se3.write(i, abs_se3.read(i - 1) @ rel_se3_matrices[i - 1])
  return abs_se3.stack()


@tf.function(experimental_relax_shapes=True)
def rel_to_abs_pose(rel_pose, rot_type):
  """Convert a tensor of relative poses of `rot_type` to absolute poses.

  Args:
    rel_pose: An n x [6-7] array with relative poses.
    rot_type: The Rotation config type use to represent a rotation in the pose.

  Returns:
    An array of (n + 1) x [6-7] absolute poses.
  """
  rel_se3 = pose_to_se3(rel_pose, rot_type)
  abs_se3 = rel_to_abs_se3(rel_se3)
  abs_pose = se3_to_pose(abs_se3, rot_type)
  return abs_pose


def null_pose(rot_type):
  """Return a null pose for the given rot_type."""
  return se3_to_pose(NULL_SE3, rot_type)

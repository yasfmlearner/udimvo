"""This module defines all of the loss functions used in the models."""
import logging

import tensorflow as tf

import src.functions.lie as lie


@tf.function
def ctc_loss(comp_poses, direct_pose, rot_type):
  """Compute the CTC loss term.

  Args:
    comp_poses: An n x 7 tensor with sequential poses that should
      be composed.
    direct_pose: A 7-dimensional tensor with a single pose for the
      direct transformation estimated by the network.
    rot_type: A Rotation config type.
        quaternion: Expects four pose elements with the x, y, z, and w
          quaternion elements, respectively.
        axis: Expects three pose elements with that when unit-normalized allows
          to extract an axis x, y, z, and an angle of rotation with respect to
          this axis.

  Returns:
      A scalar value with the L2-norm of the difference between the
      composed and direct pose.
  """
  se3_matrices = lie.pose_to_se3(comp_poses, rot_type)
  composed_se3 = lie.se3_compose(se3_matrices)
  composed_pose = lie.se3_to_pose(composed_se3, rot_type)
  return tf.nn.l2_loss(direct_pose - composed_pose)


@tf.function
def infomax_loss(logits, neg_samples, name):
  """Compute the infomax loss term.

  Args:
      logits: The output activations of the discriminator.
      neg_samples: The number of negative samples for every positive samples.
      name: The name of the infomax formulation that should be used to
          compute the term. One of {"dv", "jsd", "infonce"}.

  Returns:
      A tf.Tensor with the infomax loss value.
  """
  batch_size = logits.shape[0]
  neg_split_index = batch_size // (neg_samples + 1)
  logits_pos, logits_neg = logits[:neg_split_index], logits[neg_split_index:]
  if name == "dv":
    return -donsker_varadhan(logits_pos, logits_neg)
  if name == "jsd":
    return -jensen_shannon(logits_pos, logits_neg)
  if name == "infonce":
    return -infonce(logits_pos, logits_neg)
  logging.error("Infomax loss with name %s hasn't been implemented.", name)
  return None


@tf.function
def donsker_varadhan(pos_values, neg_values):
  """Compute the Donsker-Varadhan representation of the KL-divergence.

  A note about the computation is that we need to use reduce_logsumexp in
  order to avoid instabilities in the exp and log functions. The exp function
  can't have large values, since it starts overflowing from 89 for float32.
  The log function can't have negative values, so the exp guarantees that
  the array passed to it is > 0.
  Args:
      pos_values: A batch of positive values output by the discriminator.
      neg_values: A batch of negative values output by the discriminator.

  Returns:
      A tf.Tensor with the computed value.
  """
  E_pos = tf.reduce_mean(pos_values)
  nr_neg_values = tf.cast(neg_values.shape[0], tf.float32)
  E_neg = tf.math.reduce_logsumexp(neg_values) - tf.math.log(nr_neg_values)
  return E_pos - E_neg


@tf.function
def jensen_shannon(pos_values, neg_values):
  """Compute the Jensen-Shannon MI estimator.

  Args:
      pos_values: A batch of positive values output by the discriminator.
      neg_values: A batch of negative values output by the discriminator.

  Returns:
      A tf.Tensor with the computed value.
  """
  E_neg = tf.reduce_mean(tf.math.softplus(neg_values))
  E_pos = tf.reduce_mean(-tf.math.softplus(-pos_values))
  return E_pos - E_neg


@tf.function
def infonce(pos_values, neg_values):
  """Compute the Noise Contrastive Estimation bound on MI.

  Args:
      pos_values: A batch of positive values output by the discriminator.
      neg_values: A batch of negative values output by the discriminator.

  Returns:
      A tf.Tensor with the computed value.
  """
  E_neg = tf.math.reduce_mean(tf.math.reduce_logsumexp(neg_values))
  E_pos = tf.reduce_mean(pos_values - E_neg)
  return E_pos

"""This module defines all of the metrics used in the models."""
import tensorflow as tf
import tensorflow.keras as keras


class MeanArrayList():
  """Keeps track of the running mean for a list of tensors."""

  def __init__(self, nr_arrs, name="grads_mean"):
    """Track a list of `nr_arrs` arrays' mean values.

    Args:
        nr_arrs: The number of tensors we wish to track.
        name: String with the base name assigned to the list of metrics.
    """
    self._arrs = [
        keras.metrics.MeanTensor(name=f"{name}_{i}") for i in range(nr_arrs)
    ]

  @tf.function
  def update_state(self, new_arrs):
    """Update the mean values for the arrays by appending `new_arrs`.

    Args:
        new_arrs: A list of tensors of length `nr_arrs` used to update
            the mean values of each tensor.
    """
    for new_arr, arr in zip(new_arrs, self._arrs):
      arr.update_state(new_arr)

  @tf.function
  def reset_states(self):
    """Reset the mean values of the arrays."""
    for arr in self._arrs:
      arr.reset_states()

  def __call__(self, new_arrs):
    self.update_state(new_arrs)

  @tf.function
  def result(self):
    """Return a list of arrays with the mean values over all updates."""
    arr_tensors = [arr.result() for arr in self._arrs]
    return arr_tensors

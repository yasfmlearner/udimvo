"""This module defines all of the regularization functions used."""
import logging

import tensorflow as tf


@tf.function
def gradient_regularization(input_grads, typ="db"):
  """Compute the gradient regularization term.

  By default, computes the doubleback loss (i.e. DataGrad), which penalizes
  the squared L2-norm of the loss' gradient with respect to the inputs.

  Args:
      input_grads: The gradients with respect to the model inputs.
      typ: The type of gradient regularization to apply. One of {'db', 'sr'}.

  Returns:
      A tensor with the computed regularization term.
  """
  grad_reg = None
  if typ == "db":
    grad_reg = tf.add_n(
        [tf.reduce_sum(tf.square(grad)) for grad in input_grads])
  else:
    logging.error(
        "Gradient regularization type %s is undefined. Use "
        "either 'db' or 'sr'.", typ)
  return grad_reg


@tf.function
def weight_decay(weights, typ="l2"):
  """Compute the weight decay regularization term.

  By default, computes the L2-norm of the weights without square root.
  Args:
      weights: List of model weights.
      type: A string with the type of weight decay to compute. One of
          {'l1', 'l2'}.

  Returns:
      A tf.Tensor with the computed value.
  """
  decay_fn = None
  if typ == "l1":
    decay_fn = l1
  elif typ == "l2":
    decay_fn = tf.nn.l2_loss
  else:
    logging.error("Weight decay type %s is undefined. Use either 'l1' or 'l2'.",
                  typ)
  return tf.add_n([decay_fn(weight) for weight in weights])


@tf.function
def l1(x):
  """Compute the L1-norm of the given tensor."""
  return tf.reduce_sum(tf.abs(x))

"""Tests for the dataset module.

Verify that train and test sequences are properly generated.
"""
import pytest

from tests.markers import valid_dataset


@valid_dataset
def test_ctc_contract_success(train_sequence):
  """Assert that the CTC contract property is generated correctly."""
  train_seq = train_sequence(nr_seqs=2, max_seq_length=5, min_ctc_length=4)
  assert train_seq.ctc_contract == [
      (0, 0, 3),
      (1, 1, 4),
      (2, 0, 4),
      (3, 4, 7),
      (4, 5, 8),
      (5, 4, 8),
  ]

  train_seq = train_sequence(nr_seqs=1, max_seq_length=5, min_ctc_length=3)
  assert train_seq.ctc_contract == [
      (0, 0, 2),
      (1, 1, 3),
      (2, 2, 4),
      (3, 0, 3),
      (4, 1, 4),
      (5, 0, 4),
  ]


@valid_dataset
def test_ctc_contract_unspecified(train_sequence):
  """Assert that the CTC contract is an empty list when unspecified."""
  train_seq = train_sequence(max_seq_length=5)
  assert train_seq.ctc_contract == []


@valid_dataset
def test_ctc_batch_consistency(train_sequence):
  """Assert that the generated batch follows the CTC contract."""
  train_seq = train_sequence(
      nr_seqs=3, neg_samples=0, max_seq_length=5, min_ctc_length=4)
  batch = train_seq[0][0]
  pos_batch, pos_batch_td = batch
  pos_batch_td = pos_batch_td.reshape((-1,) + pos_batch_td.shape[-3:])
  for direct_idx, start_idx, stop_idx in train_seq.ctc_contract:
    direct_src = pos_batch[direct_idx, ..., :3]
    direct_dst = pos_batch[direct_idx, ..., 3:]
    consec_batch = pos_batch_td[start_idx:stop_idx]
    comp_src = consec_batch[0, ..., :3]
    comp_dst = consec_batch[-1, ..., 3:]
    assert (direct_src == comp_src).all()
    assert (direct_dst == comp_dst).all()


@valid_dataset
def test_train_infomax_batch_shape(train_sequence):
  """Assert that a batch with the correct shape is generated from config."""
  train_seq = train_sequence(
      nr_seqs=3,
      target_shape=(120, 120),
      max_seq_length=5,
      neg_samples=1,
      pos_warp_samples=1)
  pos_batch, pos_batch_td, neg_batch, neg_batch_td = train_seq[0][0]
  assert pos_batch_td.shape == (3, 4, 120, 120, 6)
  assert pos_batch.shape == (48, 120, 120, 6)
  assert pos_batch_td.shape == neg_batch_td.shape
  assert pos_batch.shape == neg_batch.shape

  train_seq = train_sequence(
      nr_seqs=2,
      target_shape=(120, 120),
      max_seq_length=6,
      neg_samples=2,
      pos_warp_samples=0)
  pos_batch, pos_batch_td, neg_batch, neg_batch_td = train_seq[0][0]
  assert pos_batch_td.shape == (2, 5, 120, 120, 6)
  assert pos_batch.shape == (20, 120, 120, 6)
  assert neg_batch_td.shape == (4, 5, 120, 120, 6)
  assert neg_batch.shape == (40, 120, 120, 6)


@pytest.mark.skip(reason="no way of currently testing this")
def test_train_seq_batch_shape(train_sequence):
  assert False


@pytest.mark.skip(reason="no way of currently testing this")
def test_eval_seq_batch_shape(eval_sequence):
  assert False


@pytest.mark.skip(reason="no way of currently testing this")
def test_train_seq_on_epoch_end(train_sequence):
  assert False


@pytest.mark.skip(reason="no way of currently testing this")
def test_eval_seq_on_epoch_end(eval_sequence):
  assert False


@pytest.mark.skip(reason="no way of currently testing this")
def test_eval_batch_consecutive_frames(eval_sequence):
  """Assert that all frame pairs are consecutive in an eval sequence."""
  assert False

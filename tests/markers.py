"""Defines all of the pytest markers used to skip tests.

The markers are a neat way to ensure that some tests only run if a
condition is met. We mainly use this to ensure that the datasets are
available to run a test that depends on it.
"""
import os

import pytest

from tests.conftest import FIXTURES_PATH, VALID_EUROC, VALID_KITTI, VALID_MSFT7

valid_kitti = pytest.mark.skipif(
    not VALID_KITTI, reason="No valid KITTI dataset available.")
valid_euroc = pytest.mark.skipif(
    not VALID_EUROC, reason="No valid EUROC dataset available.")
valid_msft7 = pytest.mark.skipif(
    not VALID_MSFT7, reason="No valid MSFT7 dataset available.")
valid_dataset = pytest.mark.skipif(
    not (VALID_KITTI or VALID_EUROC or VALID_MSFT7),
    reason="No valid dataset available.")

# Datafile fixtures
kitti_poses = pytest.mark.datafiles(
    os.path.join(FIXTURES_PATH, "kitti-poses.txt"))

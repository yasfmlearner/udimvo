import os

import pytest

from src.data.dataset import TrainSequence
from src.data.euroc import EUROC
from src.data.kitti import KITTI
from src.data.msft7 import MSFT7

DATASETS_PATH = os.path.join("data", "raw")
FIXTURES_PATH = os.path.join("tests", "fixtures")
KITTI_PATH = os.path.join(DATASETS_PATH, "kitti_odom", "raw")
EUROC_PATH = os.path.join(DATASETS_PATH, "euroc")
MSFT7_PATH = os.path.join(DATASETS_PATH, "msft7")

VALID_KITTI = KITTI.isvalid(KITTI_PATH)
VALID_EUROC = EUROC.isvalid(EUROC_PATH)
VALID_MSFT7 = MSFT7.isvalid(MSFT7_PATH)


@pytest.fixture
def kitti_dataset():
  """Return KITTI dataset instance from default path."""
  if VALID_KITTI:
    return KITTI(KITTI_PATH)
  return None


@pytest.fixture
def euroc_dataset():
  """Return EUROC dataset instance from default path."""
  if VALID_EUROC:
    return EUROC(EUROC_PATH)
  return None


@pytest.fixture
def msft7_dataset():
  """Return MSFT7 dataset instance from default path."""
  if VALID_MSFT7:
    return MSFT7(MSFT7_PATH)
  return None


@pytest.fixture
def existing_dataset(kitti_dataset, euroc_dataset, msft7_dataset):
  """Return any valid dataset."""
  if kitti_dataset is not None:
    return kitti_dataset
  if euroc_dataset is not None:
    return euroc_dataset
  if msft7_dataset is not None:
    return msft7_dataset
  raise ValueError("Couldn't find a valid dataset underneath 'data/raw'. "
                   "Make sure that you've set up the symlinks with "
                   "'make data'.")


@pytest.fixture
def train_sequence(existing_dataset):
  """Return a function that generates a default train sequence."""

  def _train_sequence(nr_seqs=3,
                      neg_samples=1,
                      pos_warp_samples=0,
                      max_seq_length=5,
                      min_ctc_length=-1,
                      target_shape=(120, 120),
                      random_crop=False,
                      preproc=False,
                      shuffle=False):
    return TrainSequence(
        dataset=existing_dataset,
        traj_ids=existing_dataset.get_train_traj_ids(),
        train_nr_seqs=nr_seqs,
        neg_samples=neg_samples,
        pos_warp_samples=pos_warp_samples,
        max_seq_length=max_seq_length,
        min_ctc_length=min_ctc_length,
        image_shape=target_shape,
        random_crop=random_crop,
        preproc=preproc,
        shuffle=shuffle,
    )

  return _train_sequence

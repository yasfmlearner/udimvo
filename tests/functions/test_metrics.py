"""Test the behavior of the metric class implementations."""
import pytest
import numpy as np

import src.functions.metrics as metrics


@pytest.fixture(name="arr1")
def fixture_arr1():
  """Return a list of values for arr1 at different timesteps."""
  arr1_t1 = [1, 2, 3, 4]
  arr1_t2 = [3, 4, 5, 6]
  return [arr1_t1, arr1_t2]


@pytest.fixture(name="arr2")
def fixture_arr2():
  """Return a list of values for arr2 at different timesteps."""
  arr2_t1 = [[-2.5, -3, 5], [-1, -2, 10]]
  arr2_t2 = [[2.5, 3, -5], [1, 2, -10]]
  return [arr2_t1, arr2_t2]


def test_mean_array_list_one_sample(arr1, arr2):
  """Assert that the mean value of each array is tracked for one update."""
  mean_list = metrics.MeanArrayList(nr_arrs=2, name="test")
  mean_list.update_state([arr1[0], arr2[0]])
  mean_values = mean_list.result()
  assert np.all(mean_values[0].numpy() == arr1[0])
  assert np.all(mean_values[1].numpy() == arr2[0])


def test_mean_array_list_multiple_samples(arr1, arr2):
  """Assert that the mean value of each array through two updates."""
  mean_list = metrics.MeanArrayList(nr_arrs=2, name="test")
  mean_list.update_state([arr1[0], arr2[0]])
  mean_list.update_state([arr1[1], arr2[1]])
  mean_values = mean_list.result()
  arr1_mean = np.mean(arr1, axis=0)
  arr2_mean = np.mean(arr2, axis=0)
  assert np.all(mean_values[0].numpy() == arr1_mean)
  assert np.all(mean_values[1].numpy() == arr2_mean)

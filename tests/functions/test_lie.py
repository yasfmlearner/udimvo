"""Test the behavior of the tensorflow implementation of lie functions."""
import os

import evo.core.lie_algebra as evo_lie
import numpy as np
import pytest
import scipy.spatial.transform as sst
import tensorflow as tf
import tensorflow_graphics.geometry.transformation as tfg

import src.functions.lie as lie
from src.config.types import Rotation
from tests.markers import kitti_poses


@pytest.fixture(name="coords", scope="module")
def fixture_coords():
  """Return a batch of 3-D homogeneous coordinates."""
  # yapf: disable
  return np.expand_dims(np.array([
      [ 1.0, 0.0,  0.0, 1.0],
      [ 2.0, 1.0, -1.0, 1.0],
      [-2.0, 0.0,  4.0, 1.0],
      [-0.5, 0.5,  1.0, 1.0],
  ], dtype=np.float32), axis=-1)
  # yapf: enable


@pytest.fixture(name="trans", scope="module")
def fixture_trans():
  """Return a batch of translation vectors."""
  # yapf: disable
  return np.array([
      [ 1.0, 0.0, 1.0],
      [ 0.0, 1.0, 0.0],
      [ 2.0, 1.0, 0.0],
      [-1.0, 0.5, 2.0],
  ], dtype=np.float32)
  # yapf: enable


@pytest.fixture(name="axis", scope="module")
def fixture_axis():
  """Return a batch of axis angles."""
  # yapf: disable
  return np.array([
      [np.pi / 2, 0, 0],
      [np.pi / 4, np.pi / 2, np.pi / 4],
      [0, 0, np.pi / 8],
      [np.pi / 3, -np.pi / 3, 0],
  ], dtype=np.float32)
  # yapf: enable


@pytest.fixture(name="quat", scope="module")
def fixture_quat(axis):
  """Return a batch of quaternions."""
  return sst.Rotation.from_rotvec(axis).as_quat().astype(np.float32)


@pytest.fixture(name="pose_quat", scope="module")
def fixture_pose_quat(trans, quat):
  """Return a batch of quaternion poses."""
  return np.concatenate([trans, quat], axis=1)


@pytest.fixture(name="pose_axis", scope="module")
def fixture_pose_axis(trans, axis):
  """Return a batch of axis-angle poses."""
  return np.concatenate([trans, axis], axis=1)


@pytest.fixture(name="so3", scope="module")
def fixture_so3(quat):
  """Return an array of SO(3) rotation matrices."""
  return sst.Rotation.from_quat(quat).as_matrix().astype(np.float32)


@pytest.fixture(name="se3", scope="module")
def fixture_se3(so3, trans):
  """Return an array of SE(3) pose matrices."""
  se3 = np.empty((trans.shape[0], 4, 4), dtype=np.float32)
  se3[:, :3, :3] = so3
  se3[:, :3, 3] = trans
  se3[:, 3, :] = [0, 0, 0, 1]
  return se3


def assert_valid_se3(se3, atol=1e-3):
  """Checks if an array of se3 matrices is normalized.

  Args:
    se3: An n x 4 x 4 array of se3 matrices.

  Returns: True if all so3 matrices are normalized (i.e. se3 * se3.T = I and
    det(se3) = 1) and the last row is [0, 0, 0, 1].
  """
  so3 = se3[:, :3, :3]
  filler = se3[:, 3, :]
  so3_T = np.transpose(so3, axes=(0, 2, 1))
  assert np.allclose(np.linalg.det(so3), 1, atol=atol)
  assert np.allclose(filler, [0, 0, 0, 1], atol=atol)
  assert np.allclose(np.matmul(so3, so3_T), np.identity(3), atol=atol)


def test_normalize_pose_quat(pose_quat, trans, quat):
  """Test that the quaternion pose is correctly normalized."""
  norm_pose_tf = lie.normalize_pose(pose_quat, Rotation.quat)
  norm_quat = sst.Rotation.from_quat(quat).as_quat()
  norm_pose_np = np.concatenate([trans, norm_quat], axis=1)
  assert np.allclose(norm_pose_tf, norm_pose_np, atol=1e-7)


def test_normalize_pose_axis(pose_axis, trans, axis):
  """Test that the axis-angle pose is correctly normalized."""
  norm_pose_tf = lie.normalize_pose(pose_axis, Rotation.axis)
  norm_axis = sst.Rotation.from_rotvec(axis).as_rotvec()
  norm_pose_np = np.concatenate([trans, norm_axis], axis=1)
  assert np.allclose(norm_pose_tf, norm_pose_np, atol=1e-7)


def test_pose_quat_to_se3(pose_quat, se3):
  """Assert that the quaternion pose is converted to an SE(3) matrix."""
  norm_pose = lie.normalize_pose(pose_quat, Rotation.quat)
  se3_tf = lie.pose_to_se3(norm_pose, Rotation.quat)
  assert np.allclose(se3_tf, se3, atol=1e-6)


def test_pose_axis_to_se3(pose_axis, se3):
  """Assert that the axis-angle pose is converted to an SE(3) matrix."""
  norm_pose = lie.normalize_pose(pose_axis, Rotation.axis)
  se3_tf = lie.pose_to_se3(norm_pose, Rotation.axis)
  assert np.allclose(se3_tf, se3, atol=1e-6)


def test_se3_to_pose_quat(se3, pose_quat):
  """Assert that the SE(3) matrix is converted to a quaternion pose vector."""
  pose_tf = lie.se3_to_pose(se3, Rotation.quat)
  assert np.allclose(pose_tf, pose_quat)


def test_se3_to_and_from_pose_quat(se3):
  """Assert that we can convert to and from a quat pose starting from SE(3)."""
  pose_quat_tf = lie.se3_to_pose(se3, Rotation.quat)
  se3_tf = lie.pose_to_se3(pose_quat_tf, Rotation.quat)
  assert np.allclose(se3_tf, se3, atol=1e-7)


def test_se3_to_and_from_pose_axis(se3):
  """Assert that we can convert to and from a axis pose starting from SE(3)."""
  pose_axis_tf = lie.se3_to_pose(se3, Rotation.axis)
  se3_tf = lie.pose_to_se3(pose_axis_tf, Rotation.axis)
  assert np.allclose(se3_tf, se3, atol=1e-6)


def test_pose_quat_to_and_from_se3(pose_quat):
  """Assert that we can convert to and from a SE(3) starting from quat pose."""
  se3_tf = lie.pose_to_se3(pose_quat, Rotation.quat)
  pose_quat_tf = lie.se3_to_pose(se3_tf, Rotation.quat)
  assert np.allclose(pose_quat_tf, pose_quat, atol=1e-7)


def test_pose_axis_to_and_from_se3(pose_axis):
  """Assert that we can convert to and from a SE(3) starting from axis pose."""
  se3_tf = lie.pose_to_se3(pose_axis, Rotation.axis)
  pose_axis_tf = lie.se3_to_pose(se3_tf, Rotation.axis)
  assert np.allclose(pose_axis_tf, pose_axis, atol=1e-6)


def test_se3_to_pose_axis(se3, pose_axis):
  """Assert that the SE(3) matrix is converted to an axis-angle pose."""
  pose_tf = lie.se3_to_pose(se3, Rotation.axis)
  assert np.allclose(pose_tf, pose_axis, atol=1e-6)


def test_se3_compose_matrix(se3):
  """Verify that all composable matrices can be formed individual transforms."""
  batch_size = se3.shape[0]
  for dist in range(2, batch_size):
    for i in range(batch_size - dist):
      se3_compose_tf = lie.se3_compose(se3[i:i + dist + 1])
      se3_compose_np = se3[i]
      for se3_matrix in se3[i + 1:i + dist + 1]:
        se3_compose_np = np.matmul(se3_matrix, se3_compose_np)
      assert np.allclose(se3_compose_tf, se3_compose_np)


def test_se3_compose_transform_coords(coords, se3):
  """Test the composition of SE(3) matrices correctly transforms coordinates."""
  se3_compose_tf = lie.se3_compose(se3)
  final_coords_tf = np.matmul(se3_compose_tf, coords)
  final_coords = np.copy(coords)
  for se3_matrix in se3:
    final_coords = np.matmul(se3_matrix, final_coords)
  assert np.allclose(final_coords_tf, final_coords)


def parse_kitti_pose_file(filename):
  """Parse a kitti pose file and return an array of SE(3) poses."""
  se3_matrices = []
  with open(filename, "r") as kitti_f:
    for pose in kitti_f:
      pose_floats = list(map(float, pose.split())) + [0, 0, 0, 1]
      se3 = np.array(pose_floats, dtype=np.float32).reshape(4, 4)
      se3_matrices.append(se3)
  return np.array(se3_matrices)


@kitti_poses
def test_kitti_poses_normalized(datafiles):
  """Load sample KITTI poses and check that poses are normalized."""
  pose_file = os.path.join(datafiles, "kitti-poses.txt")
  abs_se3 = parse_kitti_pose_file(pose_file)
  assert_valid_se3(abs_se3)


@kitti_poses
def test_kitti_valid_se3(datafiles):
  """Check that KITTI poses are valid SE(3) matrices."""
  pose_file = os.path.join(datafiles, "kitti-poses.txt")
  abs_se3 = parse_kitti_pose_file(pose_file)
  assert_valid_se3(abs_se3)


def test_se3_matrix(so3, trans, se3):
  """Test that an SE(3) pose can be created from an SO(3) rot and a trans."""
  se3_tf = lie.se3_matrix(so3, trans)
  assert np.allclose(se3_tf, se3)


def test_inverse_se3(se3):
  """Verify the computation of the inverse SE(3) transformation."""
  se3_inv_tf = lie.inverse_se3(se3)
  se3_inv = np.array(list(map(evo_lie.se3_inverse, se3)))
  assert np.allclose(se3_inv_tf, se3_inv)


@kitti_poses
def test_relative_se3(datafiles, se3):
  """Verify the relative SE(3) between two pose transformations."""
  pose_file = os.path.join(datafiles, "kitti-poses.txt")
  abs_se3_kitti = parse_kitti_pose_file(pose_file)
  rel_se3_tf = lie.relative_se3(abs_se3_kitti[:-1], abs_se3_kitti[1:])
  rel_se3 = np.array(
      list(map(evo_lie.relative_se3, abs_se3_kitti[:-1], abs_se3_kitti[1:])))
  assert np.allclose(rel_se3_tf, rel_se3, atol=1e-4)

  rel_se3_tf = lie.relative_se3(se3[:-1], se3[1:])
  rel_se3 = np.array(list(map(evo_lie.relative_se3, se3[:-1], se3[1:])))
  assert np.allclose(rel_se3_tf, rel_se3)


@kitti_poses
def test_kitti_abs_to_rel_se3_conversion(datafiles, se3):
  """Test that we can convert to and from relative and absolute poses.

  We only use a subset of the poses from the KITTI file since the error from
  the conversions accumulates really quickly in the translation vector and we
  wanted to keep the tolerance for np.allclose relatively low. Note how for 200
  poses we already had to increase the absolute tolerance to 5e-4, which is
  much higher than the one used for other tests.
  """
  pose_file = os.path.join(datafiles, "kitti-poses.txt")
  abs_se3_kitti = parse_kitti_pose_file(pose_file)
  rel_se3_tf = lie.abs_to_rel_se3(abs_se3_kitti)
  abs_se3_tf = lie.rel_to_abs_se3(rel_se3_tf)
  assert np.allclose(abs_se3_tf, abs_se3_kitti, atol=1.1e-1)
  assert_valid_se3(abs_se3_tf)

  abs_se3_tf = lie.rel_to_abs_se3(se3)
  rel_se3_tf = lie.abs_to_rel_se3(abs_se3_tf)
  assert np.allclose(rel_se3_tf, se3, atol=1e-6)


def test_rel_to_abs_pose_quat(pose_quat):
  """Test the conversion from relative to absolute quaternion poses."""
  abs_pose_quat_tf = lie.rel_to_abs_pose(pose_quat, Rotation.quat)
  rel_pose_quat_tf = lie.abs_to_rel_pose(abs_pose_quat_tf, Rotation.quat)
  assert np.allclose(rel_pose_quat_tf, pose_quat, atol=1e-6)


def test_rel_to_abs_pose_axis(pose_axis):
  """Test the conversion from relative to absolute axis-angle poses."""
  abs_pose_axis_tf = lie.rel_to_abs_pose(pose_axis, Rotation.axis)
  rel_pose_axis_tf = lie.abs_to_rel_pose(abs_pose_axis_tf, Rotation.axis)
  assert np.allclose(rel_pose_axis_tf, pose_axis, atol=2e-6)


def test_abs_to_rel_pose_quat(pose_quat):
  """Test the conversion from absolute to relative quaternion poses."""
  abs_pose_quat = np.empty(
      shape=(pose_quat.shape[0] + 1, pose_quat.shape[1]), dtype=np.float32)
  abs_pose_quat[0, :] = [0, 0, 0, 0, 0, 0, 1]
  abs_pose_quat[1:, :] = pose_quat
  rel_pose_quat_tf = lie.abs_to_rel_pose(abs_pose_quat, Rotation.quat)
  abs_pose_quat_tf = lie.rel_to_abs_pose(rel_pose_quat_tf, Rotation.quat)
  assert np.allclose(abs_pose_quat_tf, abs_pose_quat, atol=1e-6)


def test_abs_to_rel_pose_axis(pose_axis):
  """Test the conversion from absolute to relative axis-angle poses."""
  abs_pose_axis = np.empty(
      shape=(pose_axis.shape[0] + 1, pose_axis.shape[1]), dtype=np.float32)
  abs_pose_axis[0, :] = [0, 0, 0, 0, 0, 0]
  abs_pose_axis[1:, :] = pose_axis
  rel_pose_axis_tf = lie.abs_to_rel_pose(abs_pose_axis, Rotation.axis)
  abs_pose_axis_tf = lie.rel_to_abs_pose(rel_pose_axis_tf, Rotation.axis)
  assert np.allclose(abs_pose_axis_tf, abs_pose_axis, atol=1e-5)


def test_so3_from_quaternion_neg_trace():
  """Assert that we can compute the gradient when the trace is -1.

  This test is meant to verify a bug found in tensorflow_graphics that
  propagates NaN values to the inputs when computing the gradient of the output
  function of `quaternion.from_rotation_matrix` with respect to its inputs when
  the rotation matrix has a trace of -1.
  """
  # yapf: disable
  so3_neg_trace = tf.Variable([[[0.873557,  0.428941, -0.230017],
                                [0.428868, -0.901813, -0.052973],
                                [-0.23015, -0.052372, -0.971744]]],
                              dtype=tf.float32)
  # yapf: enable
  with tf.GradientTape() as tape:
    quat = tfg.quaternion.from_rotation_matrix(so3_neg_trace)
  grad_quat_so3 = tape.gradient(quat, so3_neg_trace)
  assert np.all(np.invert(np.isnan(grad_quat_so3)))

\section{Experiments}\label{sec:experiments}

Throughout all the experiments, we used a sequence length $N = 5$, minimum
temporal distance $t_{\min} = 3$, $L=0$ positive augmentations, $K=1$ negative
samples, Adam optimizer with $\alpha = \num{1e-4}$, $\beta_1 = 0.9$, $\beta_2 =
0.99$, pre-trained VGG-16 with batch norm as $C_\psi$, and MLP with one hidden
layer of size 1024 as $E_\psi$ and $T_\omega$. Every epoch we cycled through
one trajectory from the training split (\ie 00-08) and used it for validation.
The input images were resized to $120 \times 120$ and a batch size of 3
sequences of length $N$ was used. The models were trained for 30 epochs and we
always used the latest checkpoint for evaluation on trajectory 00.

The initial experiments were with the mutual information objective.
After exhaustive tuning, we concluded that it was not possible to learn a
meaningful pose, given that there's no way to guarantee that the encoded
representation is a pose without a stronger self-supervisory signal. Since
these experiments were made with outdated logs, we omit the visualizations in
this report.

The next experiments were with the compositional transformation constraints
loss $\mathcal{L}_\mathrm{CTC}$. Figure~\ref{fig:ctc-hist} shows the histograms
for the ground-truth and predicted features of the pose. Note how most
predicted features are normally distributed around 0 with a higher $\sigma$
than the ground truth poses. The scale of the distribution is less important,
since we still align the trajectories and apply a scale correction before
comparing them, given that we cannot recover an absolute scale from monocular
frames. Figure~\ref{fig:ctc-pose} displays each of the features plotted over time with
respect to the ground-truth trajectory. The model does an extremely poor job of learning
the trajectory, predicting only one abrupt orientation shift while there are
multiple throughout it. Figure~\ref{fig:ctc-traj} shows the
predicted trajectory positions in 3-D, where we can see how the model is not
able to estimate a pose by only minimizing $\mathcal{L}_\mathrm{CTC}$. One of
the problems with this loss term is that it allows the model to learn a trivial
solution where all predicted poses are null, since this satisfies the
consistency constraint. Iyer \etal~\cite{Iyer2018GeometricConsistency} address
this issue by adding a regularization term with the error between the predicted
pose and a noisy estimate from ORB-SLAM. Our next experiment explores this
solution by adding a ground-truth regularization term.

\begin{figure}
    \begin{center}
    \begin{subfigure}{0.49\textwidth}
        \resizebox{\textwidth}{!}{%
            \input{plots/ctc-baseline/traj-00-pred.pgf}
        }
        \caption{Prediction}
        \label{fig:ctc-hist-pred}
      \end{subfigure}
      \begin{subfigure}{0.49\textwidth}
        \resizebox{\textwidth}{!}{%
            \input{plots/ctc-baseline/traj-00-true.pgf}
        }
        \caption{Ground truth}
        \label{fig:ctc-hist-true}
    \end{subfigure}
    \end{center}
    \caption{Histograms for each of the features in the 6-DoF
    ground-truth~\subref{fig:ctc-hist-true} and
    predicted~\subref{fig:ctc-hist-pred} poses for trajectory 00 of the KITTI
    dataset obtained from the model trained to minimize
    $\mathcal{L}_\mathrm{CTC}$.}
    \label{fig:ctc-hist}
\end{figure}

\begin{figure}
    \begin{center}
    \begin{subfigure}{0.49\textwidth}
      \includegraphics[width=\textwidth]{plots/ctc-baseline/tensorboard/pos.png}
      \caption{Position}
      \label{fig:ctc-pos}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
      \includegraphics[width=\textwidth]{plots/ctc-baseline/tensorboard/ori.png}
      \caption{Orientation}
      \label{fig:ctc-ori}
    \end{subfigure}
    \end{center}
    \caption{Position~\subref{fig:ctc-pos} and orientation~\subref{fig:ctc-ori}
    over time for trajectory 00 from the KITTI dataset inferred with the model
    trained on $\mathcal{L}_\mathrm{CTC}$. In blue we have the predicted values
    and in black the ground-truth ones.}
    \label{fig:ctc-pose}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/ctc-baseline/tensorboard/traj.png}
    \caption{Trajectory positions in 3-D for the KITTI sequence 00 predicted
    with the model trained on $\mathcal{L}_\mathrm{CTC}$. The blue line is the
    predicted trajectory and the black one is the ground-truth.}
    \label{fig:ctc-traj}
  \end{center}
\end{figure}

In the next experiment, we wanted to validate if the model was able to learn
anything meaningful to verify that there was no bug in the network. To do so,
we defined the supervised objective given below

\begin{equation}
  \mathcal{L}_\mathrm{GT} = \sum_{t=t_{\min}}^{N-1} \sum_{j=1}^{N-t} \|p_{j
  \rightarrow j + t} - \tilde{p}_{j \rightarrow j + t} \|_2,
\end{equation}

where $\tilde{p}$ is the ground-truth pose.

Figure~\ref{fig:gt-hist} shows a similar distribution to
Figure~\ref{fig:ctc-hist}.  However, Figure~\ref{fig:gt-pose} shows a much more
promising trajectory estimate, where the model is able to predict several of
the sharp orientation shifts and the general direction of the trajectory
positions. Figure~\ref{fig:gt-traj} shows that despite the model not learning a
good approximation of the ground-truth trajectory, it is not learning a trivial
solution.

\begin{figure}
    \begin{center}
    \begin{subfigure}{0.49\textwidth}
        \resizebox{\textwidth}{!}{%
            \input{plots/ground-truth/traj-00-pred.pgf}
        }
        \caption{Prediction}
        \label{fig:gt-hist-pred}
      \end{subfigure}
      \begin{subfigure}{0.49\textwidth}
        \resizebox{\textwidth}{!}{%
            \input{plots/ground-truth/traj-00-true.pgf}
        }
        \caption{Ground truth}
        \label{fig:gt-hist-true}
    \end{subfigure}
    \end{center}
    \caption{Histograms for each of the features in the 6-DoF
    ground-truth~\subref{fig:gt-hist-true} and
    predicted~\subref{fig:gt-hist-pred} poses for trajectory 00 of the KITTI
    dataset obtained from the model trained to minimize
    $\mathcal{L}_\mathrm{GT}$.}
    \label{fig:gt-hist}
\end{figure}

\begin{figure}
    \begin{center}
    \begin{subfigure}{0.49\textwidth}
      \includegraphics[width=\textwidth]{plots/ground-truth/tensorboard/pos.png}
      \caption{Position}
      \label{fig:gt-pos}
    \end{subfigure}
    \begin{subfigure}{0.49\textwidth}
      \includegraphics[width=\textwidth]{plots/ground-truth/tensorboard/ori.png}
      \caption{Orientation}
      \label{fig:gt-ori}
    \end{subfigure}
    \end{center}
    \caption{Position~\subref{fig:ctc-pos} and orientation~\subref{fig:ctc-ori}
    over time for trajectory 00 from the KITTI dataset inferred with the model
    trained on $\mathcal{L}_\mathrm{GT}$. In blue we have the predicted values
    and in black the ground-truth ones.}
    \label{fig:gt-pose}
\end{figure}

\begin{figure}
  \begin{center}
    \includegraphics[width=0.6\textwidth]{plots/ground-truth/tensorboard/traj.png}
    \caption{Trajectory positions in 3-D for the KITTI sequence 00 predicted
    with the model trained on $\mathcal{L}_\mathrm{GT}$. The blue line is the
    predicted trajectory and the black one is the ground-truth.}
    \label{fig:gt-traj}
  \end{center}
\end{figure}

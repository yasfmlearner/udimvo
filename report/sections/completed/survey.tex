\section{Bibliographic Survey}\label{sec:survey}

The survey consisted of evaluating the state of the art methods for unsupervised visual odometry and self-supervised representation learning.

\subsection{Unsupervised Visual Odometry}\label{sec:uvo}

The initial mutual information framework we proposed was
largely based on the work of Iyer \etal~\cite{Iyer2018GeometricConsistency}. The idea was to use geometric consistency as a self-supervisory signal to learn
meaningful representations for pose estimation with mutual information.
After reviewing the literature, we found that there are several other ways to add supervision to estimating VO\@.

One of the main patterns we observed in the literature was depth and
egomotion as symbiotic self-supervisory signals. Instead of only estimating the egomotion of an agent, the networks also estimate the depth map of each monocular frame. This information is used alongside the pose to build photometric reconstruction losses. Zhou \etal~\cite{Zhou2017UVO} were the first to propose a framework that uses monocular frames to estimate depth and egomotion through view synthesis supervision. Given a training image sequence $\{I_i\;|\; 1 \le i \le N\}$ with one frame $I_t$ being the target view and the remaining ones source views $\{I_s\;|\;1 \le s \le N, s \ne t\}$, the temporal view synthesis
objective can be formulated as

\begin{equation}
  \mathcal{L}_{t_{vs}} = \sum_s \sum_p |I_t(p) - \hat{I}_s(p)|,
\end{equation}

where $p$ indexes over pixel coordinates and $\hat{I}_s$ is the source view $I_s$ warped to the target image coordinates. The reconstructed source view $\hat{I}_s$ is obtained by sampling pixels from $I_s$ based on the predicted depth map $\hat{D_t}$ and the relative pose $\hat{T}_{t \rightarrow s}$. Let $p_t$ be the homogeneous coordinates of a pixel in the target view, and $K$ the
camera's intrinsic matrix, we can obtain the $p_t$'s projected coordinates
onto source view $p_s$ by

\begin{equation}\label{eq:temp-rl}
p_s \sim K \hat{T}_{t \rightarrow s} \hat{D}_t (p_t) K^{-1} p_t.
\end{equation}

Following this pattern, Li \etal proposed UnDeepVO~\cite{Li2018UnDeepVOMV},
which also takes two consecutive monocular frames and computes a depth map through a depth estimator network and a pose through a pose estimator network.
The main difference is that the SFMLearner proposed by Zhou \etal cannot recover scale from monocular images, while UnDeepVO uses stereo images during training to generate poses and depth maps with absolute scales and monocular frames during inference.

Despite these claims, we still question the validity of the pipeline,
given that the model was only evaluated in the KITTI dataset, which has similar absolute scales throughout the trajectories. To evaluate if the monocular VO framework is, in fact, able to recover absolute scales, it would be necessary to evaluate it on a different dataset with different scales.

Li \etal still propose interesting self-supervisory signals, leveraging the
stereo images available during training. They propose using spatial
losses for the stereo image pair and temporal losses for the consecutive
monocular images. Given two image sequences $\{I_{L_i}\;|\;1 \le i \le N\}$ and
$\{I_{R_i}\;|\;1 \le i \le N\}$ for the left and right cameras of the stereo pair,
respectively, we can formulate the spatial view synthesis loss as

\begin{equation}
  \mathcal{L}_{s_{vs}} =  \sum_i \sum_p |I_{L_i} - \hat{I}_{R_i}(p)|,
\end{equation}

where $\hat{I}_{R_i}$ is the $i$th right view warped to the $i$th left view, and
the transformation $T_{R \rightarrow L}$ must be known from the dataset.
In both view synthesis losses, they synthesize one image from the other using spatial transformers~\cite{Jaderberg2015Transformer}. One of the assumptions of the image reconstruction losses is that the scene is Lambertian, so that the brightness is constant regardless of the observer's viewing angle.

Zhan \etal~\cite{zhan2018depthvo} propose DepthVOFeat, which also uses stereo images during training to minimize the temporal and spatial view synthesis losses, but propose a new loss component that addresses the Lambertian assumption. They propose a term for the reconstruction error between dense features in the network, experimenting with the features from a pre-trained ResNet50~\cite{he2015resnet} on ImageNet and a pre-trained image descriptor network for dense monocular reconstruction~\cite{weerasekera2018learning}.
Their experiments show that minimizing the reconstruction loss between the dense features contributes to the network's depth and egomotion estimates. Zhan \etal also propose a depth continuity term that penalizes depth maps with discontinuities if image continuity is shown in the same region, given by

\begin{equation}
  \mathcal{L}_{dc} = \sum_i \sum_p |\partial_x D_i(p)| e^{-|\partial_x I_i(p)|} + |\partial_y D_i(p)| e^{-|\partial_y I_i(p)|},
\end{equation}

where $\partial_x$ and $\partial_y$ are the gradients in horizontal and
vertical directions, respectively.

Yin \etal~\cite{yin2018geonet} propose GeoNet, a pipeline to estimate depth and
egomotion from monocular frames. GeoNet uses an initial pipeline similar to
other works that assumes a rigid non-occluded environment to estimate a pose
and depth map and penalize the temporal reconstruction loss from
Equation~\ref{eq:temp-rl}. However, GeoNet also accounts for the non-rigid body
motion in the scene, by cascading the output of the rigid body motion pipeline
to their residual flow network, which predicts the per-pixel residual pose
between the reconstructed image and the target image. With the residual flow,
they compute the full flow between the images, which they use to reconstruct
the original one through the temporal view synthesis objective.

Almalioglu \etal~\cite{almalioglu2019ganvo} also propose GANVO, which combines
the image reconstruction loss with a minimax objective that trains a
discriminator to distinguish between the reconstructed image and the original.
Almalioglu \etal~\cite{almalioglu2020selfvio} then extends GANVO to propose
SelfVIO, which uses an adaptive sensor fusion module to combine monocular
frames and inertial measurement unit (IMU) readings and estimate the depth of
the scene and the egomotion of the agent.

Table~\ref{tab:unsupervised-vo} shows the performance of some of the
state of the art models on sequences 09 and 10 of the KITTI dataset. Some of
the networks analyzed in this section aren't in the table since they adopt a
different evaluation criteria that isn't comparable with the majority.

% Maybe add supervision column
\begin{table}
\begin{center}
 \begin{tabular}{|c|c|cc|c|}
 \hline
   %\multicolumn{1}{|c|}{Method} & Seq. 09 & Seq. 10 & Mean \\
   Method & Metric & Seq. 09 & Seq. 10 & Mean \\
   \hline
   \multirow{2}{*}{SelfVIO~\cite{almalioglu2020selfvio}} & $t_{\mathrm{rel}}(\si{\percent})$ & 1.95 & 1.81 & 1.88 \\
                            & $r_{\mathrm{rel}}(\si{\degree})$  & 1.15 & 1.30 & 1.23 \\
   \hline
   \multirow{2}{*}{DepthVOFeat~\cite{zhan2018depthvo}} & $t_{\mathrm{rel}}(\si{\percent})$ & 11.93 & 12.45 & 12.19 \\
                                & $r_{\mathrm{rel}}(\si{\degree})$  & 3.91  & 3.46  & 3.65 \\
   \hline
   \multirow{2}{*}{SfMLearner~\cite{Zhou2017UVO}} & $t_{\mathrm{rel}}(\si{\percent})$ & 21.63 & 20.54 & 21.09 \\
                               & $r_{\mathrm{rel}}(\si{\degree})$  & 3.57 & 10.93 & 7.25 \\
   \hline
   \multirow{2}{*}{ORB-SLAM~\cite{Mur-Artal2015ORBSLAM}} & $t_{\mathrm{rel}}(\si{\percent})$ & 24.41 & 3.16 & 13.79 \\
                             & $r_{\mathrm{rel}}(\si{\degree})$  & 2.08 & 2.15 & 2.12 \\
   \hline
\end{tabular}
  \caption{Comparison of monocular VO and monocular VIO approaches on KITTI
  sequences 09 and 10 using the standard evaluation criteria defined in the
  benchmark~\cite{KITTIBenchmark}. Metric $t_{\mathrm{rel}}(\si{\percent})$
  is the average relative translational error percentage on all trajectories of
  length \num{100}-\SI{800}{\meter}. Metric $r_{\mathrm{rel}}(\si{\degree})$ is
  the relative rotational error in $(\si{\degree}/100\si{\meter})$ on all
  trajectories of length \num{100}-\SI{800}{\meter}.}
  \label{tab:unsupervised-vo}
\end{center}
\end{table}


\subsection{Representation Learning}\label{sec:rep-learn}

One of the main contributions of this work is applying representation learning
techniques to verify if its possible to improve unsupervised visual odometry
pipelines. The initial proposal set out to use mutual information maximization
as a self-supervisory signal similar to how Hjelm \etal~\cite{Hjelm2019DIM}
were able to learn better features for the downstream task of image
classification with Deep InfoMax (DIM). However, mutual information-based
methods have shown to be strongly dependent on the choice of feature extractor
architecture~\cite{Tschannen2020OnMI}.

Contrastive learning techniques have recently obtained the state of the art in
self-supervised learning. Chen \etal~\cite{chen2020simple} recently proposed
SimCLR, which maximizes the agreement between distinctly augmented views of
the same data sample through a constrastive loss in the latent space. In their
framework, each sample $x$ from the training set is augmented twice to obtain
$\tilde{x}_i$ and $\tilde{x}_j$, which are encoded through $f(\cdot)$ to obtain
$h_i$ and $h_j$, respectively. The encoded features $h_i$ and $h_j$ are the
representations we wish to learn. The representations are then projected through
a projection head $g(\cdot)$ to obtain $z_i$ and $z_j$, which they found to be
more beneficial to define the contrastive loss on. Given a minibatch of $N$
training samples, they derive $2N$ augmented samples, and for every pair of
augmented samples, they treat all other $2(N-1)$ samples as negative examples.
Let $\mathrm{sim}(\mathbf{u}, \mathbf{v}) = \mathbf{u}^T \mathbf{v} /
\|\mathbf{u}\|_2 \|\mathbf{v}\|_2$, then the loss function for a positive
pair of examples $(i, j)$ is defined as

\begin{equation}
  \ell_{i,j} = -\log \frac{\exp(\mathrm{sim}(\mathbf{z}_i, \mathbf{z}_j)/\tau)}
  {\sum_{k=1}^{2N} \mathbbm{1}_{[k \ne i]} \exp(\mathrm{sim}(\mathbf{z}_i,
  \mathbf{z}_k)/\tau)},
\end{equation}

where $\mathbbm{1}_{[k \ne i]} \in \{0, 1\}$ is an indicator function
evaluating to 1 if $k \ne i$ and $\tau$ denotes a temperature parameter. The
final loss is computed across all positive pairs $(i, j)$ and $(j, i)$ in a
mini-batch.

Patacchiola \etal~\cite{patacchiola2020self} propose their own self-supervised
framework based on relational reasoning, that does not use a similarity metric
like the ones in constrastive learning or a distance metric like metric
learning. Instead, the framework uses a probabilistic approach, where a
relation module outputs a relation score $y$ that represents the probabilistic
estimate of representation membership. In their framework, $K$ augmentations
are applied to each sample $\mathbf{x}_n$ to form $\mathbf{x}_n^{(i)}$. A
forward pass through the network generates a vector $f(\mathbf{x}_n^{(i)}) =
\mathbf{z}_n^{(i)}$. Let $r(\cdot)$ define a relation module which takes as
input a pair of representations aggregated by $a(\cdot, \cdot)$ and returns a
relation score $y$. The loss function for an input sample $\mathbf{x}_n$ can be
defined as

\begin{equation}
  \ell_{n} = \sum_{k=1}^N \sum_{i=1}^{K} \sum_{j=1}^K
  \mathcal{L}(r(a(\mathbf{z}_n^{(i)}, \mathbf{z}_k^{(j)})), \mathbbm{1}_{[k = n]}),
\end{equation}

where $\mathcal{L}(\cdot, \cdot)$ is the binary cross-entropy loss.

The results of a linear evaluation of the learned representations on the
CIFAR-100 and tiny-ImgNet datasets for the self-supervised representation learning
frameworks discusses is given by Table~\ref{tab:repr-learning}. While the
constrastive learning and relational reasoning methods severely outperform
the mutual information method, the relational reasoning framework outperforms
SimCLR by only $\SI{3}{\percent}$ on average.

\begin{table}
\begin{center}
 \begin{tabular}{|c|c|c|}
 \hline
   Method & CIFAR-100 & tiny-ImgNet \\
   \hline
   Supervised (upper bound) & $65.32 \pm 0.22$ & $50.09 \pm 0.32$ \\
   Random weights (lower bound) & $7.65 \pm 0.44$ & $3.24 \pm 0.43$ \\
   \hline
   Deep InfoMax~\cite{Hjelm2019DIM} & $24.07 \pm 0.05$ & $17.51 \pm 0.15$ \\
   SimCLR~\cite{chen2020simple} & $42.13 \pm 0.35$ & $25.79 \pm 0.35$ \\
   Relational Reasoning~\cite{patacchiola2020self} & $46.17 \pm 0.17$ & $30.54 \pm 0.42$ \\
   \hline
\end{tabular}
  \caption{Comparison of self-supervised representation learning methods
  mean accuracy and standard deviation over three runs on ResNet-32 using a
  linear evaluation method (\ie training on unlabeled data and linear evaluation
  on labeled data).}
  \label{tab:repr-learning}
\end{center}
\end{table}

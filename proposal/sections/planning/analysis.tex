\section{Analysis}\label{sec:analysis}

This project sets out to develop a monocular VO estimation network with unsupervised learning techniques.
To validate the network's results, we use three datasets with different collection patterns and two evaluation metrics.
Section~\ref{sec:datasets} describes the datasets used and some of their characteristics and limitations.
Section~\ref{sec:metrics} defines the metrics that will be used to compare different networks tasked with estimating VO\@.

\subsection{Datasets}\label{sec:datasets}

Visual odometry datasets can be characterized by the environment in which the image sequences where captured (\eg, indoor or outdoor), the camera properties (\eg, monocular or stereo), and the vehicle used to capture the images (\eg, self-driving cars or micro aerial vehicles).

\textbf{KITTI Dataset}~\cite{Geiger2012Kitti}

The KITTI vision benchmark suite~\cite{KITTIBenchmark} is one of the most used benchmarks for evaluating VO algorithms.
The dataset consists of \num{22} stereo image sequences with a total distance of \SI{39.2}{\kilo\metre} recorded from a moving car.
Since our goal is to work with monocular images, only the left images from the dataset will be used.

One of the drawbacks of the KITTI dataset is the limited motion patterns of the agent inherent from driving.
The vehicle only moves in a forward motion and pose changes are smooth due to the car's kinematic model.
The dataset was also collected in an outdoor environment, which helps contrast both of the other datasets.

\textbf{EuRoC MAV Dataset}~\cite{Burri2016EuRoC}

The EuRoC MAV Dataset was created for the European Robotics Challenge (EuRoC) on Micro Aerial Vehicles.
The dataset consists of 11 stereo image sequences, ranging from slow flights with smoother pose transitions and better image quality to dynamic flights with motion blur and poor illumination.
Unlike KITTI, this dataset was collected indoors and offers scenes with 6 degrees of freedom (DoF).
The camera used also had a global shutter.

\textbf{Microsoft's 7-Scenes Dataset}~\cite{Glocker20157Scenes}

The 7-Scenes dataset is a collection of tracked RGB-D camera frames, collected indoors using a handheld Kinect RGB-D camera with a global shutter.
In this dataset, multiple sequences were recorded by different users for each of the seven scenes.
Due to the camera being handheld by different users, the dataset has great variability in paths and offers scenes with 6 DoF\@.

\subsection{Metrics}\label{sec:metrics}

To evaluate the accuracy of the pose estimations, one of the metrics that will be used is the Absolute Trajectory Error (ATE).
This metric computes the Root Mean Squared Error (RMSE) of the difference between each frame's estimated and actual pose and averages this error for the entire sequence.
The metric is given by
\begin{equation}\label{eq:ate}
  \text{ATE} = \frac{1}{N} \sum_{i=1}^N \| \mathbf{\hat{p}}_i - \mathbf{p}_i \|_2,
\end{equation}
where $N$ is the number of frames in the sequence, $\mathbf{\hat{p}}_i$ is the estimated pose for frame $i$ and $\mathbf{p}_i$ is the ground-truth pose for the same frame.

One of the problems with ATE is that it is not symmetric concerning the time the robot made a translational error.
If the robot were to make a translational error $e$ in the first motion and perfect estimates for the remaining motions, that error would be propagated across all of the other poses.
However, if the error were to occur only at the last motion, then the computed $ATE$ would be $k$ times less than if $e$ occurred in the first motion, where $k$ is the number of motions in the sequence.

The Relative Positional Error (RTE) is based on the relative displacement between robot poses.
Instead of comparing two poses in the global reference frame, it compares the ground truth and estimated motions.
The composition of two poses defines motion.
To compute the metric, we have to define which motions should be considered.
Therefore, we have to define the length and velocity of the trajectory to evaluate the metric.
For instance, in the KITTI benchmark~\cite{KITTIBenchmark}, they compute the metric for all possible sub-sequences of length \num{100} to \SI{800}{\meter}, in increments of \num{100}.
The metric is also separated into translational and rotational components, which are evaluated individually.
We define the relative translation and rotation error as
\begin{align}
\label{eq:rpetrans}
\text{RPE}_{trans} &= \frac{1}{|\mathcal{F}|} \sum_{(i,j) \in \mathcal{F}} \| (\mathbf{\hat{p}}_j \oplus \mathbf{\hat{p}}_i) \ominus (\mathbf{p}_j \oplus \mathbf{p}_i)\|_2,\\
\label{eq:rperot}
\text{RPE}_{pos} &= \frac{1}{|\mathcal{F}|} \sum_{(i,j) \in \mathcal{F}} \angle [ (\mathbf{\hat{p}}_j \oplus \mathbf{\hat{p}}_i) \ominus (\mathbf{p}_j \oplus \mathbf{p}_i) ],
\end{align}
respectively, where the $\oplus$ operator refers to the standard motion compositional operator and $\ominus$ refers to the inverse of $\oplus$~\cite{Kummerle2009RPE}.

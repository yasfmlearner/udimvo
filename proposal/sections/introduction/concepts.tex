\section{Concepts}\label{sec:concepts}
In this section, we detail some of the core concepts required to understand the project.
Section~\ref{sec:mi} defines what is mutual information and presents some of the recent work in estimating mutual information with neural networks.
Section~\ref{sec:vo} discusses what is visual odometry and some of the recent work that applies unsupervised learning methods to estimate the egomotion of an agent.

\subsection{Mutual Information}\label{sec:mi}
Mutual Information~(MI) is a fundamental quantity for measuring the relationship between two random variables $X$ and $Y$.
The correlation coefficient is another quantity used for the same end. However, it is limited since it can only capture the linear dependence between variables.
In contrast, MI captures the non-linear statistical dependencies between variables and can act as a measure of true dependence~\cite{Murphy2012ML}.
For continuous random variables, this is done by measuring how similar the joint probability density function $p(X, Y)$ is to the product of the marginal probability density functions $p(X)p(Y)$:
\begin{equation}\label{eq:mi}
 \mathcal{I}(X, Y) = \int_x \int_y p(x, y) \log \frac{p(x,y)}{p(x)p(y)} \dif{x} \dif{y}.
\end{equation}
MI is notoriously difficult to compute in continuous and high-dimensional settings.
Therefore, recent approaches use neural networks to estimate mutual information between high-dimensional continuous random variables (\eg MINE~\cite{Belghazi2018MINE}).

MI can also be defined in terms of the Kullback-Leibler (KL-) divergence between the joint distribution and the product of marginals, given by
\begin{align}
  \label{eq:mikl}
  \mathcal{I}(X, Y) &= \kl{P_{XY}}{P_X \otimes P_Y},\\
  \label{eq:kl}
  \kl{P}{Q} &= \int_x p(x) \log{\frac{p(x)}{q(x)}} \dif{x}.
\end{align}

Equation \ref{eq:mikl} shows the larger the divergence between the joint and the product of marginals, the larger the mutual information or dependence between $X$ and $Y$.
If $X$ and $Y$ are independent variables, the divergence is zero, and the mutual information reaches its minimum value.
MINE uses dual representations of the KL-divergence to estimate MI through a single learnable function that can be modeled by a neural network.
They primarily work with the Donsker-Varadhan representation~\cite{Donsker1976KLDual}, which allows the derivation of the following relationship
\begin{equation}\label{eq:midk}
  \mathcal{I}_{\texttt{DV}}(X, Y) = \kl{P_{XY}}{P_X \otimes P_Y} \ge \E_{P_{XY}}[T_{\omega}(x, y)] - \log{\E_{P_X \otimes P_Y} [e^{T_{\omega} (x, y)}]},
\end{equation}
where $T_{\omega} : \mathcal{X} \times \mathcal{Y} \to \mathbb{R}$ is a discriminator function modeled by a neural network with parameters $\omega$, and $\mathcal{X}$ and $\mathcal{Y}$ are the input and output spaces for the encoder network, respectively.
The encoder network maps the input samples to output samples through a continuous differentiable parametric function, $E_{\psi} : \mathcal{X} \to \mathcal{Y}$ with parameters $\psi$.
At a high level, we are optimizing $E_\psi$, our encoder network, by simultaneously estimating and maximizing the InfoMax objective~\cite{Hjelm2019DIM},
\begin{equation}\label{eq:infomax}
  \max\limits_{\psi,\omega}\mathcal{I}(X, E_\psi (X)).
\end{equation}

An equivalent formulation of the InfoMax objective that is a lower bound of the objective in Equation~\ref{eq:infomax} is,
\begin{equation}\label{eq:infomax-gen}
  \max\limits_{g_1 \in \mathcal{G}_1, g_2 \in \mathcal{G}_2} \mathcal{I}_{\texttt{EST}} (g_1(X^{(1)}); g_2(X^{(2)})),
\end{equation}
where $\mathcal{I}_{\texttt{EST}}(X; Y)$ is a sample-based estimator of the true MI $\mathcal{I}(X; Y)$, such as the Donsker-Varadhan representation given by Equation~\ref{eq:midk}, $X^{(1)}$ and $X^{(2)}$ are different, possibly overlapping views of an input $X$, and $g_1$ and $g_2$ are encoders~\cite{Tschannen2020OnMI}.
This formulation has a few advantages with respect to the one in Equation~\ref{eq:infomax}.
First, it estimates the MI between two learned representations of the two views, which typically lie on a much lower-dimensional space than the original input $X$.
Second, it allows more modeling flexibility with respect to which two views are chosen, allowing us to capture different aspects of the data.

The objective given by Equation~\ref{eq:infomax-gen} is the one that is used by Hjelm \etal in their work with Deep InfoMax (DIM).
In the local component of their loss, $g_1$ extracts global features of the entire input $X^{(1)}$ and $g_2$ extracts local features from image patches $X^{(2)}$, where $g_1$ corresponds to the activations of the final convolutional layer in the encoder and $g_2$ are the activations of the final fully connected layer in the same encoder.
Tian \etal in their work with Contrastive Multiview Coding (CMC) further generalize the objective in Equation~\ref{eq:infomax-gen} to consider multiple views $X^{(i)}$, where each $X^{(i)}$ corresponds to a different image modality, such as different color channels or the depth information of an image~\cite{Tian2019ContrastiveMC}.

Despite impressive results, it is well-known that maximizing MI does not necessarily lead to useful representations.
Tschannen \etal argue that the success of these methods is only loosely connected to MI and attribute it to the inductive bias in the encoder and MI estimator (\ie discriminator) networks~\cite{Tschannen2020OnMI}.
In their work, they show several key results to support their hypothesis.
First, a large MI estimate is not predictive of downstream performance.
They show this by demonstrating that the MI estimate saturates at the very early stages of training while the accuracy on the downstream task keeps improving.
Second, they show that higher capacity critics can lead to lower downstream performance.
By comparing different discriminators ranging from bilinear to multilayer perceptron (MLP) networks, they demonstrate that despite acheiving a tighter bound on MI, the higher capacity critics have a worse performance on the downstream task.
Third, they verify that the encoder architecture plays a more imoprtant role than the MI estimator.
By training different encoder architectures until they acheive the same MI lower bound, they showed that the CNN encoder clearly outperformed the MLP in downstream tasks despite both architectures acheiving equivalent results when trained in an end-to-end supervised manner.
They conclude their work by stating that it is unclear whether the connection to MI is a sufficient or necessary component to designing powerful unsupervised representation learning algorithms~\cite{Tschannen2020OnMI}.

Given these limitations, it is important to address a particular downstream task in a holistic manner, carefully choosing the encoder, critic and downstream evaluation protocol that complement each another.
The choice of negative examples also plays a crucial role, since it has been shown that when drawn in a dependent fashion the connection to MI might vanish completely~\cite{Tschannen2020OnMI}.
Finally, it is also important to look into other information estimates that don't completely ignore the geometry of the latent space necessary for good performance on the downstream task.
One such estimate is $\mathcal{V}$-information proposed by Xu \etal~\cite{Xu2020ATO}, which removes certain undesirable properties of MI like the Data Processing Inequality ($\mathcal{I}(t(X); Y) \le \mathcal{I}(X; Y)$ for any function $t: \mathcal{X} \to \mathcal{X}$), making it more appropriate for machine learning settings.

\subsection{Visual Odometry}\label{sec:vo}
In VO we estimate the egomotion of an agent using the input of a single or multiple cameras attached to it~\cite{Scaramuzza2011VO}.
Similarly to wheel odometry, where an agent's motion is estimated by integrating the number of turns of its wheels over time, VO estimates the pose of an agent-based on the incremental changes that motion induces in the images captured by its camera.
VO's advantage to wheels odometry is that it is less susceptible to uneven terrain or other adverse conditions that can cause changes in the agent's underlying kinematic model.
However, VO is highly dependent on image quality, requiring a static scene with enough texture and sufficient illumination for motion to be extracted.
The sampling rate also plays a critical role, since consecutive frames need to have sufficient scene overlap for motion extraction~\cite{Scaramuzza2011VO}.

With the success of deep learning-based approaches in tackling challenging problems in computer vision~\cite{Goodfellow2006DL}, a wide range of deep architectures have been proposed for VO estimation.
While geometric VO methods rely on feature matching and tracking to estimate the agent's motion, which makes it incredibly challenging to design features that have good invariance properties to adverse scene conditions, the deep VO methods are capable of learning these feature representations~\cite{Iyer2018GeometricConsistency}.
The deep VO methods can be further classified as supervised or unsupervised.
The supervised approaches assume the availability of ground-truth egomotion data, such as the per-frame camera pose in a global coordinate system, gathered using IMU or GPS sensors~\cite{Kendall2015PoseNet}.
The unsupervised approaches do not require ground-truth data and leverage other visual information to assist in the learning process, such as stereo images~\cite{Godard2016UDeepVOStereo}.

Iyer \etal proposed a recent take on learning VO estimation with an unsupervised approach~\cite{Iyer2018GeometricConsistency}.
They apply a noisy odometry estimate from a traditional geometric VO pipeline (\eg, ORB-SLAM~\cite{Mur-Artal2015ORBSLAM}) to train a recurrent neural network that outputs the camera pose transformation between two frames.
The approach also uses geometry as a self-supervisory signal by defining a set of Composite Transformation Constraints (CTCs) across a series of image frames.
These constraints ensure that estimated transforms over short timescales, when compounded, equal their computed counterparts over longer timescales.
For a sequence of three consecutive frames, we must satisfy
\begin{align}\label{eq:geom-cons}
T_t^{t+1} \cdot T_{t+1}^{t+2} \cdot T_{t+2}^{t+3} &= T_t^{t+3}, \\
T_t^{t+1} \cdot T_{t+1}^{t+2} &= T_t^{t+2}, \\
T_{t+1}^{t+2} \cdot T_{t+2}^{t+3} &= T_{t+1}^{t+3},
\end{align}
where $T_t^{t'}$ is the transform from the coordinates in image $I_t$ to the coordinate space in $I_{t'}$.

In Iyer \etal's work, they use both a CTC and a regularization loss to train their network.
The CTC enforces geometric consistency between the predictions and is defined by the Mean Squared Error~(MSE) between the direct transformation $T_d$ predicted between non-consecutive frames and the composite transformation $T_c$ composed as a product of smaller sequential transformations from successive frames.
The regularization loss enforces a prior on the transformations to prevent the collapse to a trivial solution where the predicted poses are all zero.
The term is defined by the MSE between every transform $T_*$ computed by the network and its respective prior $T'_*$ from a conventional VO estimator~\cite{Iyer2018GeometricConsistency}.

The idea of geometric consistency sheds some light on the possibilities of applying our mutual information learning framework.
We can exploit it by using geometric inconsistency as a way of feeding negative samples to our discriminator.
We can also draw ideas from Hjelm \etal~\cite{Hjelm2019DIM}, by using both local and global optimizations to enhance the representations learned through consistency between shorter and longer frame sequences, respectively.

Most of the other state-of-the-art approaches to unsupervised VO use image reconstruction as a supervisory signal~\cite{Garg2016UnsupervisedCF, Zhou2017UVO, Li2018UnDeepVOMV}.
Zhou \etal learn both depth and pose from monocular frames by minimizing a view synthesis objective formulated as
\begin{equation}\label{eq:view-synth}
  \mathcal{L}_{vs} = \sum_s \sum_p | I_t(p) - \hat{I}_s(p) |,
\end{equation}
where $I_t$ is the target frame, $\hat{I}_s$ are the different source views $I_s$ in the image sequence warped to the target coordinate frame, and $p$ are the indexes over pixel coordinates.
By estimating the depth of the target frame $I_t$ and the relative pose $T_{t \to s}$, they are able to reconstruct the target frame from each of the source frames.
Garg \etal estimate depth using the same idea, except they leverage the known value of $T_{t \to s}$ from left-right stereo image pairs to estimate the depth.

Despite showing promising results, it is more complex to apply MI to the view synthesis objective, since one of the downstream tasks is to estimate depth.
The depth estimate is a per-pixel depth map of the same size as the input.
This makes it so the critic is forced to consider structural information in the input, narrowing our options to only high-capacity critics and possibly penalizing the performance on the downstream task~\cite{Tschannen2020OnMI}.

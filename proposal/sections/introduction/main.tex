% !TeX encoding = UTF-8
\chapter{Introduction}\label{chp:intro}

Unsupervised learning methods are extremely important in tasks where it is costly to obtain ground-truth data.
Obtaining ground-truth per-frame pose information is costly to do with accuracy, requiring specialized equipment like GPS, laser scanners, and motion capture cameras~\cite{Geiger2012Kitti}.
In this context, it is increasingly important to develop unsupervised learning techniques capable of achieving comparable results to supervised approaches without the collection of ground-truth data.

While Visual Odometry~(VO) is the process of estimating the egomotion of an agent using cameras, Simultaneous Localization and Mapping~(SLAM) is a process in which an agent is required to localize itself in an unknown environment and build a map of this environment without any prior information.
The main difference between the two is that the former focuses on local consistency and aims to incrementally estimate the pose of the agent, while the latter aims to obtain global consistency by estimating the trajectory of the agent in a map and enforcing constraints such as loop closures, where the agent returns to a previously visited area, to reduce the estimation's drift~\cite{Scaramuzza2011VO}.

Due to their close relationship, improvements in VO lead to improvements in SLAM\@.
The latter is one of the core components of several of the upcoming technologies being developed for the next decade, such as autonomous vehicles, augmented reality, and virtual reality.
For autonomous vehicles, SLAM is a core problem that is currently solved through expensive sensors, like LIDAR, which measures distance through reflected light pulses.
Solving these problems with a monocular camera, for instance, could make these devices more financially accessible.
Similarly, augmented and virtual reality wearables need to be able to accurately estimate the agent's position and create a map to place virtual objects on top of real objects and better define the physical boundaries of the real world for navigation.

This project aims to establish a new unsupervised learning framework for visual odometry based on mutual information maximization.
Currently, most VO estimation methods consist of a simple encoder that takes a sequence of frames as input and outputs an estimated pose.
The proposed framework will consist of an encoder and a discriminator, where the encoder is responsible for estimating the pose, and the discriminator is used to maximize the mutual information between the input frames and the estimated pose.
The discriminator is fed positive and negative samples, which correspond to real and fake input-output pairs. The discriminator's output is then used to estimate the mutual information used as a component in the network's loss.

To the best of our knowledge, leveraging mutual information in VO tasks has only been done for dense direct estimation methods~\cite{Shankar2016VOMI}, which estimate the pose transformation between a given pair of frames by minimizing the photometric error between these frames.
Direct estimation and feature-based methods are classified as geometry-based methods that do not use any learning techniques to estimate VO, relying on camera geometry for determining motion.

Following the steps used to apply mutual information to image classification tasks~\cite{Hjelm2019DIM}, we can use existing state of the art VO encoders to estimate the pose for a given pair of frames.
However, what is still undefined and is one of the key contributions of this work is the choice of negative samples that should be generated to train the discriminator network.
The negative samples consist of any form of fake input that is fed to the discriminator.
In VO, this could be any pair of frames that is independent of the estimated pose.
Whether to use frames from an entirely different video or the same sequence but further or closer apart in time or even the same frames but with an applied perspective transformation is still undefined.

\input{sections/introduction/concepts}

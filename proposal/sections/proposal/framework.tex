\section{Framework}\label{sec:framework}
A high-level overview of the proposed learning framework is given by Figure~\ref{fig:hlframework}.
First, positive~($x$) and negative~($\hat{x}$) samples are drawn from the input space $\mathcal{X}$.
The encoder network $E_\psi$ is the network we wish to learn and maps our input samples to our desired representation ($E_\psi: \mathcal{X} \to \mathcal{Y}$).
The encoded positive samples ($y \sim E_\psi(x)$) are then paired with both the positive and negative input samples to form sample pairs drawn from the joint and marginal distributions, respectively.
Since the negative samples form the marginal distribution pairs, it is crucial for them to be independent of the output.
Otherwise, it can be shown that the MI estimator is no longer a lower nor upper bound on the true MI~\cite{Tschannen2020OnMI}.
The key to applying this learning framework is in determining what and how many negative samples should be used.
The discriminator or critic, is also modeled by a network and is responsible for outputting a real number from the input-output pairs ($T_\omega: \mathcal{X} \times \mathcal{Y} \to \mathbb{R}$).
The critic function is used as an input to a sample-based estimator of MI, such as the the Donsker-Varadhan estimator $\mathcal{I}_{\texttt{DV}}$ given by Equation~\ref{eq:midk}.
The MI estimator is then used as the InfoMax objective, which is trained to simulatenously estimate and maximize the MI between the input and output pairs.
For the particular case of $\mathcal{I}_{\texttt{DV}}$, we have that the discriminator $T_\omega$ will learn to maximize its value for the positive sample pairs $(x, y)$ and minimize its value for the negative sample pairs $(\hat{x}, y)$.

\input{sections/proposal/hl-fwk}

This project aims to apply this framework to the downstream task of VO\@.
This means learning the encoder $E_\psi$ that can be used to estimate an agent's pose from a sequence of monocular frames.
The performance of the model will be evaluated by comparing the learned encoder to other supervised and unsupervised VO estimation networks, which will be done by computing the metrics in Section~\ref{sec:metrics} on the datasets of Section~\ref{sec:datasets}.

A sample instance of the framework for this task is given by Figure~\ref{fig:sample-framework}.
The positive input samples are a pair of successive frames drawn from a sequence of the KITTI~\cite{Geiger2012Kitti} dataset, while the negative samples are successive frames drawn from a different sequence of the dataset.
Both samples are fed to a convolutional encoder $C_\psi$ that aims to reduce the dimensionality of the input before feeding the critic.
The encoded positive sample $C_\psi(x)$ is then fed to a fully-connected encoder $E_\psi$ that further lowers the dimensionality of the representation to the desired pose estimate.
The convolutional encoder's output for both positive and negative samples is then paired to the final pose estimate and fed to the discriminator $T_\omega$ that outputs real numbers for each pair.
Finally, the MI estimate is computed and used as our learning objective.
Here it is worth noting that we used the more general form of the InfoMax objective given by Equation~\ref{eq:infomax-gen}, where $g_1 = C_\psi$ and $g_2 = E_\psi$, and there is an overlap in the parameters of each function.

\input{sections/proposal/sample-fwk}

The sample framework shown in Figure~\ref{fig:sample-framework} is a simplified version of the one we wish to implement.
In practice, geometric consistency is going to be used as a self-supervisory signal.
This means that in order to update the network's parameters, we need to compute the pose estimate for multiple pairs of frames and guarantee that the composition of different poses is consistent with the direct estimate, as seen on Equation~\ref{eq:geom-cons}.
The convolutional and fully-connected encoders will be drawn from Iyer \etal's~\cite{Iyer2018GeometricConsistency} proposed networks which are available on GitHub\footnote{\url{https://github.com/krrish94/CTCNet-release}}
The negative sampling strategy along with the discriminator network will have to be chosen empirically, since there is no previous work applying MI to VO in a learning context.

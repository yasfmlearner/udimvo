#!/bin/bash

RAW_DATA_DIR="data/raw"

cat << EOM
Let's setup your data directory before you can start training or testing.
There are three datasets to setup: kitti, euroc, and msft7.
This script sets up symlinks within the data/raw directory for each of the datasets.
For each of the datasets, you must specify its parent directory.
Make sure that the each of the dataset directories has the name "kitti", "euroc", and "msft7", respectively.

EOM

function prompt_create_symlink {
    read -p "Specify a directory for $1 dataset [ENTER]: "
    REPLY=$(eval echo $REPLY)
    if [ -z $REPLY ]; then
        echo "Parent directory can't be empty."
        exit 1
    fi
    if ! [ -d $REPLY ]; then
        echo "Parent directory $REPLY doesn't exist."
        exit 1
    fi
    if ! [ -d $REPLY/$1 ]; then
        echo "Dataset directory $1 doesn't exist within $REPLY"
        exit 1
    fi
    if ! [ -d $RAW_DATA_DIR ]; then
        mkdir -p $RAW_DATA_DIR
    fi
    ln -v -s $REPLY/$1 $RAW_DATA_DIR/$1
}

for dataset in "kitti" "euroc" "msft7"; do
    echo Settup up $dataset dataset...
    if [ -h $RAW_DATA_DIR/$dataset ]; then
        read -n 1 -r -p "A symlink to $dataset already exists. Do you wish to override it? [Y/N]: "
        echo
            if [[ $REPLY =~ ^[Yy]$ ]]; then
                prompt_create_symlink $dataset
            else
                echo Skipping $dataset setup.
			fi
    else
        prompt_create_symlink $dataset
    fi
    echo
done

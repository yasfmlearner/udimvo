OUT_DIR = bin
PROP_NAME = proposal
REPORT_NAME = report
TCC_NAME = tcc
CV_NAME = cv

.PHONY: build build-report build-cv write-report write-cv create_dirs data clean

build: build-report build-cv

build-tcc: $(TCC_NAME)/$(TCC_NAME).tex | create_dirs_tcc
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR) \
		-jobname=$(TCC_NAME)

write-tcc: $(TCC_NAME)/$(TCC_NAME).tex | create_dirs_report
	latexmk --extra-mem-bot=10000000 -shell-escape -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) \
		-output-directory=$(OUT_DIR) -jobname=$(TCC_NAME)

build-proposal: $(PROP_NAME)/$(PROP_NAME).tex | create_dirs_proposal
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR) \
		-jobname=$(PROP_NAME)

write-proposal: $(PROP_NAME)/$(PROP_NAME).tex | create_dirs_proposal
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) \
		-output-directory=$(OUT_DIR) -jobname=$(PROP_NAME)

build-cv: $(CV_NAME)/$(CV_NAME).tex | create_dirs
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR) \
		-jobname=$(CV_NAME)

write-cv: $(CV_NAME)/$(CV_NAME).tex
	latexmk -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) \
		-output-directory=$(OUT_DIR) -jobname=$(CV_NAME)

build-report: $(REPORT_NAME)/$(REPORT_NAME).tex | create_dirs_report
	latexmk -pdf -cd $< -aux-directory=$(OUT_DIR) -output-directory=$(OUT_DIR) \
		-jobname=$(REPORT_NAME) -shell-escape

write-report: $(REPORT_NAME)/$(REPORT_NAME).tex | create_dirs_report
	latexmk --extra-mem-bot=10000000 -shell-escape -pdf -pvc -cd $< -aux-directory=$(OUT_DIR) \
		-output-directory=$(OUT_DIR) -jobname=$(REPORT_NAME)

create_dirs_tcc:
	@for OUTPUT in $(shell cd $(TCC_NAME); find . -type d); do \
	  mkdir -p $(OUT_DIR)/$$OUTPUT; \
	done

create_dirs_proposal:
	@for OUTPUT in $(shell cd $(PROP_NAME); find . -type d); do \
	  mkdir -p $(OUT_DIR)/$$OUTPUT; \
	done

create_dirs_report:
	@for OUTPUT in $(shell cd $(REPORT_NAME); find . -type d); do \
	  mkdir -p $(OUT_DIR)/$$OUTPUT; \
	done

data:
	./scripts/setup_data.sh

clean:
	@rm -rf $(OUT_DIR)/*
